const order = require('../../models/order.model');
const product = require('../../models/product.model');
const dispatch = require('../../models/dispatch.model');
var async = require("async");
const orderTimeLine = require('../../models/order-timeline.model');
const email = require('../../routes/email-functions');
const cryptoRandomString = require('crypto-random-string');
const user = require('../../models/user.model');
const acknowledge = require('../../models/acknowledge.model');
const returnOrder = require('../../models/return.model');
const transferOrder = require('../../models/transfer.model');
const notificationTimeLine = require('../../models/notification-timeline.model');
const costLevel = require('../../models/costlevel.model');
const dateFomate = require('dateformat');


// getDataOrderWise
exports.getDataOrderWise = function(req, res, next) {
    let filterData = req.body;
    var UserID = null;
    const condition = {
        'Active': 1
    }

    if (filterData.UserID && filterData.UserID != '0') {
        UserID = filterData.UserID;
    }

    if (filterData.ProductID && filterData.ProductID != '0') {
        condition['Products.ProductID'] = filterData.ProductID;
    }

    if (filterData.CustomerName && filterData.CustomerName != '') {
        condition['CustomerDetails.CustomerName'] = { $regex: new RegExp(filterData.CustomerName, "i") };
    }

    if (filterData.CustomerMobile && filterData.CustomerMobile != '') {
        condition['CustomerDetails.Mobile'] = { $regex: '.*' + filterData.CustomerMobile + '.*' };
    }

    if (filterData.RequestID && filterData.RequestID != '') {
        condition.RequestID = { $regex: '.*' + filterData.RequestID + '.*' };
    }

    if (filterData.ServiceType && filterData.ServiceType != '0') {
        condition.ServiceType = filterData.ServiceType;
    }

    if (filterData.ProductCode && filterData.ProductCode != '') {
        condition['Products.ProductCode'] = { $regex: new RegExp(filterData.ProductCode, "i") };
    }

    if (filterData.FromDate && filterData.FromDate != '') {
        condition.CreatedDate = {
            $gte: dateFomate(new Date(filterData.FromDate), 'yyyy-mm-dd 00:00:00')
        };
    }

    if (filterData.ToDate && filterData.ToDate != '') {
        condition.CreatedDate = {
            $lte: dateFomate(new Date(filterData.ToDate), 'yyyy-mm-dd 23:59:59')
        };
    }

    if (filterData.FromDate && filterData.FromDate != '' && filterData.ToDate && filterData.ToDate != '') {
        condition.CreatedDate = {
            $gte: dateFomate(new Date(filterData.FromDate), 'yyyy-mm-dd 00:00:00'),
            $lte: dateFomate(new Date(filterData.ToDate), 'yyyy-mm-dd 23:59:59')
        };
    }

    user.findOne({ _id: UserID })
        .populate({ path: 'RoleID' })
        .exec().then(resultUser => {
            var RoleName = resultUser && resultUser.RoleID ? resultUser.RoleID.Description : '';
            if (RoleName == 'OM Supervisory') {
                condition.Status = {
                    $ne: 1
                }
            } else if (RoleName == 'Approver') {
                condition.Status = {
                    $in: [1, 2, 3]
                }
            } else if (RoleName == 'Dealer') {
                condition.SalesPersonID = UserID;
            }

            order.find(condition)
                .sort({ _id: 'desc' })
                .populate({ path: 'UserID', select: ['FirstName', 'LastName', 'Phone'] })
                .populate({ path: 'Products.ProductID' })
                .populate({ path: 'SalesPersonID', select: ['FirstName', 'LastName', 'Phone', 'Email', 'Address'] })
                .then(resultOrder => {
                    user.findById(UserID).then(resultUser => {
                        finalResult = [];
                        if (RoleName == 'Approver') {
                            var minVal = resultUser.MinCostLevel,
                                maxVal = resultUser.MaxCostLevel;
                            resultOrder.map(item => {
                                if (item.Status == 1 && item.TotalAmount > minVal && item.TotalAmount <= maxVal) {
                                    finalResult.push(item);
                                } else if (item.ApproverID == UserID) {
                                    finalResult.push(item);
                                }
                            })
                        } else {
                            finalResult = resultOrder;
                        }
                        if (filterData.Status && filterData.Status != '0') {
                            finalResult = finalResult.filter(x => x.Status == filterData.Status);
                        }
                        finalResult.map(item => {
                            item.Products = item.Products.filter(x => x.Active == 1)
                        })
                        res.status(200).json({
                            'status': true,
                            'message': 'Success',
                            'result': finalResult,
                            'allOrder': resultOrder
                        })
                    }).catch(err => {
                        next(err);
                    })
                }).catch(err => {
                    next(err);
                })
        }).catch(err => {
            next(err);
        })
}


// getDataProductWise
exports.getDataProductWise = function(req, res, next) {
    let filterData = req.body;
    var UserID = null;
    const condition = {
        'Active': 1
    }

    if (filterData.UserID && filterData.UserID != '0') {
        UserID = filterData.UserID;
    }

    if (filterData.ProductID && filterData.ProductID != '0') {
        condition['Products.ProductID'] = filterData.ProductID;
    }

    if (filterData.CustomerName && filterData.CustomerName != '') {
        condition['CustomerDetails.CustomerName'] = { $regex: new RegExp(filterData.CustomerName, "i") };
    }

    if (filterData.CustomerMobile && filterData.CustomerMobile != '') {
        condition['CustomerDetails.Mobile'] = { $regex: '.*' + filterData.CustomerMobile + '.*' };
    }

    if (filterData.RequestID && filterData.RequestID != '') {
        condition.RequestID = { $regex: '.*' + filterData.RequestID + '.*' };
    }

    if (filterData.ServiceType && filterData.ServiceType != '0') {
        condition.ServiceType = filterData.ServiceType;
    }

    if (filterData.ProductCode && filterData.ProductCode != '') {
        condition['Products.ProductCode'] = { $regex: new RegExp(filterData.ProductCode, "i") };
    }

    if (filterData.FromDate && filterData.FromDate != '') {
        condition.CreatedDate = {
            $gte: dateFomate(new Date(filterData.FromDate), 'yyyy-mm-dd 00:00:00')
        };
    }

    if (filterData.ToDate && filterData.ToDate != '') {
        condition.CreatedDate = {
            $lte: dateFomate(new Date(filterData.ToDate), 'yyyy-mm-dd 23:59:59')
        };
    }

    if (filterData.FromDate && filterData.FromDate != '' && filterData.ToDate && filterData.ToDate != '') {
        condition.CreatedDate = {
            $gte: dateFomate(new Date(filterData.FromDate), 'yyyy-mm-dd 00:00:00'),
            $lte: dateFomate(new Date(filterData.ToDate), 'yyyy-mm-dd 23:59:59')
        };
    }

    user.findOne({ _id: UserID })
        .populate({ path: 'RoleID' })
        .exec().then(resultUser => {
            var RoleName = resultUser && resultUser.RoleID ? resultUser.RoleID.Description : '';
            if (RoleName == 'OM Supervisory') {
                condition.Status = {
                    $ne: 1
                }
            } else if (RoleName == 'Approver') {
                condition.Status = {
                    $in: [1, 2, 3]
                }
            } else if (RoleName == 'Dealer') {
                condition.SalesPersonID = UserID;
            }

            order.find(condition)
                .sort({ _id: 'desc' })
                .populate({ path: 'UserID', select: ['FirstName', 'LastName', 'Phone'] })
                .populate({ path: 'Products.ProductID' })
                .populate({ path: 'SalesPersonID', select: ['FirstName', 'LastName', 'Phone', 'Email', 'Address'] })
                .then(resultOrder => {
                    user.findById(UserID).then(resultUser => {
                        finalResult = [];
                        if (RoleName == 'Approver') {
                            var minVal = resultUser.MinCostLevel,
                                maxVal = resultUser.MaxCostLevel;
                            resultOrder.map(item => {
                                if (item.Status == 1 && item.TotalAmount > minVal && item.TotalAmount <= maxVal) {
                                    finalResult.push(item);
                                } else if (item.ApproverID == UserID) {
                                    finalResult.push(item);
                                }
                            })
                        } else {
                            finalResult = resultOrder;
                        }
                        if (filterData.Status && filterData.Status != '0') {
                            finalResult = finalResult.filter(x => x.Status == filterData.Status);
                        }

                        finalResult.map(item => {
                            item.Products = item.Products.filter(x => x.Active == 1)
                        })

                        var ProductArr = [];
                        finalResult.map(item => {
                            item.Products.map(p => {
                                var ind = ProductArr.findIndex(x => x._id == p.ProductID._id);
                                if (ind == -1) {
                                    ProductArr.push({
                                        _id: p.ProductID._id,
                                        ProductCode: p.ProductID.ProductCode,
                                        Name: p.ProductID.Name,
                                        Type: p.ProductID.Type,
                                        Quantity: parseInt(p.Quantity),
                                        MisFranchise: p.ProductID.MisFranchise ? p.ProductID.MisFranchise : 'NA',
                                        MisBuHead: p.ProductID.MisBUGroup ? p.ProductID.MisBUGroup : 'NA'
                                    })
                                } else {
                                    ProductArr[ind].Quantity += parseInt(p.Quantity);
                                }
                            })
                        })
                        if (filterData.ProductID && filterData.ProductID != '0') {
                            ProductArr = ProductArr.filter(x => x._id == filterData.ProductID);
                        }
                        if (filterData.ProductCode && filterData.ProductCode != '') {
                            ProductArr = ProductArr.filter(x => x.ProductCode.toLowerCase().indexOf(filterData.ProductCode.toLowerCase()) > -1);
                        }

                        if (filterData.MisFranchise && filterData.MisFranchise != '0') {
                            ProductArr = ProductArr.filter(x => x.MisFranchise.toLowerCase().indexOf(filterData.MisFranchise.toLowerCase()) > -1);
                        }
                        if (filterData.MisBuHead && filterData.MisBuHead != '0') {
                            ProductArr = ProductArr.filter(x => x.MisBuHead.toLowerCase().indexOf(filterData.MisBuHead.toLowerCase()) > -1);
                        }
                        res.status(200).json({
                            'status': true,
                            'message': 'Success',
                            'result': ProductArr
                        })
                    }).catch(err => {
                        next(err);
                    })
                }).catch(err => {
                    next(err);
                })
        }).catch(err => {
            next(err);
        })
}


// getAllBudgetReport
exports.getAllBudgetReport = function(req, res, next) {
    var startDay = new Date("1/1/" + (new Date()).getFullYear());
    var endDay = new Date("12/31/" + (new Date()).getFullYear());
    var condition = {
        CreatedDate: { $gte: startDay, $lte: endDay },
        // Status: { $in: [5, 6] },
        Active: 1
    };
    order.find(condition)
        .sort({ _id: 'desc' })
        .populate({ path: 'UserID', select: ['FirstName', 'LastName', 'Phone'] })
        .populate({ path: 'Products.ProductID' })
        .populate({ path: 'SalesPersonID', select: ['FirstName', 'LastName', 'Phone', 'Email', 'Address'] })
        .then(resultOrder => {
            finalResult = resultOrder;

            finalResult.map(item => {
                item.Products = item.Products.filter(x => x.Active == 1)
            })

            var ProductArr = [];
            var ProductUniqueArr = [];
            finalResult.map(item => {
                item.Products.map(p => {
                    if (!p.ProductID.MisFranchise) {
                        p.ProductID.MisFranchise = 'NA';
                    }

                    var month = item.CreatedDate.getMonth() + 1;
                    var ind = ProductArr.findIndex(x => x.MisFranchise == p.ProductID.MisFranchise && x.Month == month);
                    var indUnique = ProductArr.findIndex(x => x.MisFranchise == p.ProductID.MisFranchise);

                    if (indUnique == -1) {
                        ProductUniqueArr.push({
                            _id: p.ProductID._id,
                            ProductCode: p.ProductID.ProductCode,
                            MisFranchise: p.ProductID.MisFranchise,
                            Name: p.ProductID.Name
                        })
                    }

                    if (ind == -1) {
                        ProductArr.push({
                            _id: p.ProductID._id,
                            ProductCode: p.ProductID.ProductCode,
                            MisFranchise: p.ProductID.MisFranchise,
                            Name: p.ProductID.Name,
                            Type: p.ProductID.Type,
                            Quantity: parseInt(p.Quantity),
                            Price: parseInt(p.Quantity) * parseFloat(p.Price),
                            Month: month

                        })
                    } else {
                        ProductArr[ind].Quantity += parseInt(p.Quantity);
                        ProductArr[ind].Price += (parseInt(p.Quantity) * parseFloat(p.Price));
                    }
                })
            })
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': ProductArr,
                'Products': ProductUniqueArr,
                'currentYear': new Date().getFullYear(),
                'currentShortYear': new Date().getFullYear().toString().substr(-2)
            })
        }).catch(err => {
            next(err);
        })
}


// getAllByTrendService
exports.getAllByTrendService = function(req, res, next) {
    let filterData = req.body;

    const condition = {
        'Active': 1
    }

    if (filterData.Type == "Y") {
        var startDay = dateFomate(new Date("1/1/" + (new Date()).getFullYear()), 'yyyy-mm-dd 00:00:00');
        var endDay = dateFomate(new Date("12/31/" + (new Date()).getFullYear()), 'yyyy-mm-dd 23:59:59');
        condition.CreatedDate = {
            $gte: startDay,
            $lte: endDay
        };
    } else if (filterData.Type == "M") {
        var date = new Date();
        var startDay = dateFomate(new Date(date.getFullYear(), date.getMonth(), 1), 'yyyy-mm-dd 00:00:00');
        var endDay = dateFomate(new Date(date.getFullYear(), date.getMonth() + 1, 0), 'yyyy-mm-dd 23:59:59');
        condition.CreatedDate = {
            $gte: startDay,
            $lte: endDay
        };
    } else if (filterData.Type == "D") {
        var startDay = dateFomate(new Date(), 'yyyy-mm-dd 00:00:00');
        var endDay = dateFomate(new Date(), 'yyyy-mm-dd 23:59:59');
        condition.CreatedDate = {
            $gte: startDay,
            $lte: endDay
        };
    }

    if (filterData.Status == "4") {
        condition.Status = 4;
    } else if (filterData.Status == "9") {
        condition.Status = 9;
    } else if (filterData.Status == "10") {
        condition.OldSalesPersonID = { '$ne': null };
    }

    order.find(condition)
        .sort({ _id: 'desc' })
        .populate({ path: 'Products.ProductID' })
        .populate({ path: 'SalesPersonID', select: ['FirstName', 'LastName', 'Phone', 'Email', 'Address'] })
        .populate({ path: 'OldSalesPersonID', select: ['FirstName', 'LastName', 'Phone', 'Email', 'Address'] })
        .then(resultOrder => {
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': resultOrder
            })
        }).catch(err => {
            console.log(err)
            next(err);
        })
}

// getAllOrdersBySortReport
exports.getAllOrdersBySortReport = function(req, res, next) {
    order.find({ Active: 1, Status: { $in: [4, 5, 6, 9] } })
        .populate({ path: 'Products.ProductID' })
        .populate({ path: 'SalesPersonID' })
        .then(resultOrder => {
            resultOrder.filter(x => x.Status == 4 || x.Status == 9 || x.OldSalesPersonID);
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': resultOrder
            })
        }).catch(err => {
            next(err);
        })
}

// getDataFranchiseBuHeadWise
exports.getDataFranchiseBuHeadWise = function(req, res, next) {
    let filterData = req.body;
    const condition = {
        'Active': 1
    }

    order.find(condition)
        .sort({ _id: 'desc' })
        .populate({ path: 'UserID', select: ['FirstName', 'LastName', 'Phone'] })
        .populate({ path: 'Products.ProductID' })
        .populate({ path: 'SalesPersonID', select: ['FirstName', 'LastName', 'Phone', 'Email', 'Address'] })
        .then(resultOrder => {

            if (filterData.MisFranchise && filterData.MisFranchise != '0') {
                resultOrder = resultOrder.filter(item =>
                    item.Products.findIndex(x => x.ProductID.MisFranchise == filterData.MisFranchise) > -1
                )
            }
            if (filterData.MisBuHead && filterData.MisBuHead != '0') {
                resultOrder = resultOrder.filter(item =>
                    item.Products.findIndex(x => x.ProductID.MisBUGroup == filterData.MisBuHead) > -1
                )
            }

            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': resultOrder
            })
        }).catch(err => {
            console.log(err);
            next(err);
        })
}

function datediff(second, first) {
    var day = Math.round((second - first) / (1000 * 60 * 60 * 24));
    if (day <= 1) {
        return day + ' day';
    } else {
        return day + ' days';
    }
}


// getDataForTatReport
exports.getDataForTatReport = function(req, res, next) {
    try {
        let filterData = req.body;
        var UserID = null;
        const condition = {
            'Active': 1
        }

        if (filterData.UserID && filterData.UserID != '0') {
            UserID = filterData.UserID;
        }

        if (filterData.ProductID && filterData.ProductID != '0') {
            condition['Products.ProductID'] = filterData.ProductID;
        }

        if (filterData.CustomerName && filterData.CustomerName != '') {
            condition['CustomerDetails.CustomerName'] = { $regex: new RegExp(filterData.CustomerName, "i") };
        }

        if (filterData.CustomerMobile && filterData.CustomerMobile != '') {
            condition['CustomerDetails.Mobile'] = { $regex: '.*' + filterData.CustomerMobile + '.*' };
        }

        if (filterData.RequestID && filterData.RequestID != '') {
            condition.RequestID = { $regex: '.*' + filterData.RequestID + '.*' };
        }

        if (filterData.ServiceType && filterData.ServiceType != '0') {
            condition.ServiceType = filterData.ServiceType;
        }

        if (filterData.ProductCode && filterData.ProductCode != '') {
            condition['Products.ProductCode'] = { $regex: new RegExp(filterData.ProductCode, "i") };
        }

        if (filterData.FromDate && filterData.FromDate != '') {
            condition.CreatedDate = {
                $gte: dateFomate(new Date(filterData.FromDate), 'yyyy-mm-dd 00:00:00')
            };
        }

        if (filterData.ToDate && filterData.ToDate != '') {
            condition.CreatedDate = {
                $lte: dateFomate(new Date(filterData.ToDate), 'yyyy-mm-dd 23:59:59')
            };
        }

        if (filterData.FromDate && filterData.FromDate != '' && filterData.ToDate && filterData.ToDate != '') {
            condition.CreatedDate = {
                $gte: dateFomate(new Date(filterData.FromDate), 'yyyy-mm-dd 00:00:00'),
                $lte: dateFomate(new Date(filterData.ToDate), 'yyyy-mm-dd 23:59:59')
            };
        }

        user.findOne({ _id: UserID })
            .populate({ path: 'RoleID' })
            .exec().then(resultUser => {
                var RoleName = resultUser && resultUser.RoleID ? resultUser.RoleID.Description : '';
                if (RoleName == 'OM Supervisory') {
                    condition.Status = {
                        $ne: 1
                    }
                } else if (RoleName == 'Approver') {
                    condition.Status = {
                        $in: [1, 2, 3]
                    }
                } else if (RoleName == 'Dealer') {
                    condition.SalesPersonID = UserID;
                }

                order.find(condition)
                    .sort({ _id: 'desc' })
                    .populate({ path: 'UserID', select: ['FirstName', 'LastName', 'Phone'] })
                    .populate({ path: 'Products.ProductID' })
                    .populate({ path: 'SalesPersonID', select: ['FirstName', 'LastName', 'Phone', 'Email', 'Address'] })
                    .then(resultOrder => {
                        user.findById(UserID).then(async resultUser => {
                            finalResult = [];
                            if (RoleName == 'Approver') {
                                var minVal = resultUser.MinCostLevel,
                                    maxVal = resultUser.MaxCostLevel;
                                resultOrder.map(item => {
                                    if (item.Status == 1 && item.TotalAmount > minVal && item.TotalAmount <= maxVal) {
                                        finalResult.push(item);
                                    } else if (item.ApproverID == UserID) {
                                        finalResult.push(item);
                                    }
                                })
                            } else {
                                finalResult = resultOrder;
                            }
                            if (filterData.Status && filterData.Status != '0') {
                                finalResult = finalResult.filter(x => x.Status == filterData.Status);
                            }
                            tatReportData = [];
                            await Promise.all(finalResult.map((item) => {
                                return new Promise(async(resolve, reject) => {
                                    await orderTimeLine.find({ Active: 1, OrderID: item._id }).then(resultTimeLine => {
                                        if (item.OldOrderID) {
                                            console.log(item)
                                            orderTimeLine.find({ Active: 1, OrderID: item.OldOrderID }).then(resultOldTimeLine => {
                                                resultOldTimeLine.map(x => {
                                                    resultTimeLine.push(x);
                                                });
                                                var approveInd = resultTimeLine.findIndex(x => x.Status == 2);
                                                var dispatchInd = resultTimeLine.findIndex(x => x.Status == 4);
                                                var deliveryInd = resultTimeLine.findIndex(x => x.Status == 5);
                                                var acknowledgeInd = resultTimeLine.findIndex(x => x.Status == 6);
                                                tatReportData.push({
                                                    RequestID: item.RequestID,
                                                    Products: item.Products.filter(x => x.Active == 1),
                                                    TimeLine: resultTimeLine,
                                                    CreatedDate: item.CreatedDate,
                                                    ApproveDays: approveInd != -1 ? datediff(resultTimeLine[approveInd].ModificationDate, item.CreatedDate) : '-',
                                                    DispatchDays: dispatchInd != -1 && approveInd != -1 ? datediff(resultTimeLine[dispatchInd].ModificationDate, resultTimeLine[approveInd].ModificationDate) : '-',
                                                    DeliveryDays: deliveryInd != -1 && dispatchInd != -1 ? datediff(resultTimeLine[deliveryInd].ModificationDate, resultTimeLine[dispatchInd].ModificationDate) : '-',
                                                    AcknowledgeDays: acknowledgeInd != -1 && deliveryInd != -1 ? datediff(resultTimeLine[acknowledgeInd].ModificationDate, resultTimeLine[deliveryInd].ModificationDate) : '-',
                                                });
                                                resolve();
                                            })
                                        } else {
                                            var approveInd = resultTimeLine.findIndex(x => x.Status == 2);
                                            var dispatchInd = resultTimeLine.findIndex(x => x.Status == 4);
                                            var deliveryInd = resultTimeLine.findIndex(x => x.Status == 5);
                                            var acknowledgeInd = resultTimeLine.findIndex(x => x.Status == 6);
                                            tatReportData.push({
                                                RequestID: item.RequestID,
                                                Products: item.Products.filter(x => x.Active == 1),
                                                TimeLine: resultTimeLine,
                                                CreatedDate: item.CreatedDate,
                                                ApproveDays: approveInd != -1 ? datediff(resultTimeLine[approveInd].ModificationDate, item.CreatedDate) : '-',
                                                DispatchDays: dispatchInd != -1 && approveInd != -1 ? datediff(resultTimeLine[dispatchInd].ModificationDate, resultTimeLine[approveInd].ModificationDate) : '-',
                                                DeliveryDays: deliveryInd != -1 && dispatchInd != -1 ? datediff(resultTimeLine[deliveryInd].ModificationDate, resultTimeLine[dispatchInd].ModificationDate) : '-',
                                                AcknowledgeDays: acknowledgeInd != -1 && deliveryInd != -1 ? datediff(resultTimeLine[acknowledgeInd].ModificationDate, resultTimeLine[deliveryInd].ModificationDate) : '-',
                                            });
                                            resolve();
                                        }
                                    }).catch(err => {
                                        console.log(err)
                                        reject();
                                    })
                                })
                            }))
                            res.status(200).json({
                                'status': true,
                                'message': 'Success',
                                'result': tatReportData,
                                'allOrder': resultOrder
                            })
                        }).catch(err => {
                            next(err);
                        })
                    }).catch(err => {
                        next(err);
                    })
            }).catch(err => {
                next(err);
            })
    } catch (e) {
        console.log(e)
    }
}