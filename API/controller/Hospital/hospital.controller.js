const hospital = require('../../models/hospital.model');

// create
exports.create = function (req, res, next) {
    let data = JSON.parse(req.body.data);
    data.ModificationDate = new Date();
    data.Active = 1;
    let UID = 0;

    const hospitals = new hospital(data);
    if (req.file) {
        let path = req.file.path;
        path = path.replace('public\\', '');
        path = path.replace('public/', ''); 
        hospitals.Avatar = path;
    }
    hospital.findOne().sort({_id: 'desc'}).then(result => {
        if (result) {
            UID = Number(result['UID_Temp']) + 1;
        }
        else {
            UID = 1;
        }
        hospitals['UID_Temp'] = UID;
        hospitals['UID'] = 'IVTOOL' + UID;
        
        hospitals.save().then(result =>{
            console.log(hospitals)
            res.status(200).json({
                'status': true,
                'message': 'Successfully created hospital',
                'result': hospitals
            })
        }).catch(err => {
            console.log(err)
            next(err);
        })
    }).catch(err => {
        next(err);
    });
}

// update
exports.update = function (req, res, next) {
    let data = JSON.parse(req.body.data);
    let id = req.params.id;
    data.ModificationDate = new Date();
    data.Active = 1;
    if (req.file) {
        let path = req.file.path;
        path = path.replace('public\\', '');
        path = path.replace('public/', ''); 
        data.Avatar = path;
    } else {
        delete data.Avatar;
    }
    hospital.findByIdAndUpdate(id, data, {new: true}).then(result => { 
        res.status(200).json({
            'status': true,
            'message': 'Successfully updated record',
            'result': result
        })
    }).catch(err => {
        console.log(err);
        next(err);
    })
}

// getOne
exports.getOne = function (req, res, next) {
    let id = req.params._id;
    hospital.findById(id)
    .select(['Name', 'Avatar', 'Address', 'OwnerName', 'City', 'State', 'Country', 'PIN'])
    .then(result => {
        res.status(200).json({
            'status': true,
            'message': 'Success',
            'result': result
        })
    }).catch(err => {
        next(err);
    })
}

// getAll
exports.getAll = function (req, res, next) {
    const condition = {
        'Active': 1
    };
    hospital.find(condition)
    .sort({_id: 'desc'})
    .select(['Name', 'Avatar', 'Address', 'OwnerName', 'City', 'State', 'Country', 'PIN'])
    .then(result => {
        res.status(200).json({
            'status': true,
            'message': 'Success',
            'result': result
        })
    }).catch(err => {
        next(err);
    })
}

// delete
exports.delete = function (req, res, next) {
    let id = req.params._id;
    hospital.findByIdAndUpdate(id, {Active: 0}).then(result => {
        res.status(200).json({
            'status': true,
            'message': 'Successfully deleted record',
            'result': result
        })
    }).catch(err => {
        next(err);
    })
}
