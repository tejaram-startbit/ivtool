const inventory = require('../../models/product.model');
const readXlsxFile = require('read-excel-file/node');
const history = require('../../models/product-history.model');
const Fs = require('fs')

function writeToFile() {
    inventory.find({ Active: 1 }).exec().then(data => {
        var path = './public/Files/Backup/inventory-' + Date.now() + '.json';
        const json = JSON.stringify(data, null, 2)
        Fs.openSync(path, 'w');
        Fs.writeFile(path, json, (err) => {
            if (err) {
                console.error(err)
                throw err
            }
            console.log('Saved data to file.');
            return true;
        })
    })
}

function SaveInventoryHistory(data) {
    data.ModificationDate = new Date();
    data.Active = 1;
    data = new history(data);
    data.save();
}


// create
exports.create = function(req, res, next) {
    let data = JSON.parse(req.body.data);
    data.AvailableQuantity = data.TotalQuantity;
    data.ModificationDate = new Date();
    data.ProductType = 'Inventory';
    data.Active = 1;
    let UID = 0;

    const inventorys = new inventory(data);
    if (req.file) {
        let path = req.file.path;
        path = path.replace('public\\', '');
        path = path.replace('public/', '');
        inventorys.Image = path;
    }
    inventory.findOne().sort({ _id: 'desc' }).then(result => {
        if (result) {
            UID = Number(result['UID_Temp']) + 1;
        } else {
            UID = 1;
        }
        inventorys['UID_Temp'] = UID;
        inventorys['UID'] = 'IVTOOL' + UID;

        inventorys.save().then(result => {
            res.status(200).json({
                'status': true,
                'message': 'Successfully created inventory',
                'result': inventorys
            })
        }).catch(err => {
            console.log(err)
            next(err);
        })
    }).catch(err => {
        next(err);
    });
}

// update
exports.update = function(req, res, next) {
    let data = JSON.parse(req.body.data);
    data.AvailableQuantity = data.TotalQuantity;
    delete data.TotalQuantity;
    let id = req.params.id;
    data.ModificationDate = new Date();
    data.Active = 1;
    if (req.file) {
        let path = req.file.path;
        path = path.replace('public\\', '');
        path = path.replace('public/', '');
        data.Image = path;
    } else {
        delete data.Image;
    }
    inventory.findById(id).then(resultInventory => {
        if (resultInventory.AvailableQuantity < data.AvailableQuantity) {
            var qty = parseInt(data.AvailableQuantity) - parseInt(resultInventory.AvailableQuantity);
            data.TotalQuantity = resultInventory.TotalQuantity + qty;
        }
        inventory.findByIdAndUpdate(id, data, { new: true }).then(result => {
            res.status(200).json({
                'status': true,
                'message': 'Successfully updated record',
                'result': result
            })
        }).catch(err => {
            console.log(err);
            next(err);
        })
    }).catch(err => {
        next(err);
    })
}

// getOne
exports.getOne = function(req, res, next) {
    let id = req.params._id;
    inventory.findById(id)
        .select(['Name', 'ProductCode', 'Image', 'Type', 'Description', 'TotalQuantity', 'AvailableQuantity', 'Price', 'ProductConsumable', 'Inventory', 'BranchCode', 'MisFranchise', 'MisBUGroup', 'Location'])
        .then(result => {
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': result
            })
        }).catch(err => {
            next(err);
        })
}

// getAll
exports.getAll = function(req, res, next) {
    const condition = {
        'Active': 1,
        'ProductType': 'Inventory'
    };
    inventory.find(condition)
        .sort({ _id: 'desc' })
        .select(['Name', 'ProductCode', 'Image', 'Type', 'Description', 'TotalQuantity', 'AvailableQuantity', 'Price', 'ProductConsumable', 'Inventory', 'BranchCode', 'MisFranchise', 'MisBUGroup', 'Location'])
        .then(result => {
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': result
            })
        }).catch(err => {
            next(err);
        })
}

// getAllInventorysByType
exports.getAllInventorysByType = function(req, res, next) {
    const condition = {
        'Active': 1,
        'Type': req.params.ServiceType,
        'ProductType': 'Inventory'
    };
    inventory.find(condition)
        .sort({ _id: 'desc' })
        .select(['Name', 'ProductCode', 'Image', 'Type', 'Description', 'TotalQuantity', 'AvailableQuantity', 'Price', 'ProductConsumable', 'Inventory', 'BranchCode', 'MisFranchise', 'MisBUGroup', 'Location'])
        .then(result => {
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': result
            })
        }).catch(err => {
            next(err);
        })
}

// delete
exports.delete = function(req, res, next) {
    let id = req.params._id;
    inventory.findByIdAndUpdate(id, { Active: 0 }).then(result => {
        res.status(200).json({
            'status': true,
            'message': 'Successfully deleted record',
            'result': result
        })
    }).catch(err => {
        next(err);
    })
}

// uploadExcel
exports.uploadExcel = function(req, res, next) {
    var index = -1;
    if (req.file) {
        writeToFile();
        readXlsxFile(req.file.path).then(async excelData => {
            await Promise.all(excelData.map((row) => {
                index++;
                return new Promise(async(resolvenew, rejectnew) => {
                    if (index > 0) {
                        await inventory.findOne({ Name: row[0], ProductCode: row[1] }).exec().then(async resultInventory => {
                            if (resultInventory) {
                                var inventoryObj = {
                                    Name: row[0],
                                    ProductCode: row[1],
                                    Type: row[2],
                                    Description: row[3],
                                    Price: row[4],
                                    TotalQuantity: parseInt(row[5]) + parseInt(resultInventory.TotalQuantity),
                                    AvailableQuantity: parseInt(row[5]) + parseInt(resultInventory.AvailableQuantity),
                                    ProductConsumable: row[6],
                                    Inventory: row[7],
                                    BranchCode: row[8],
                                    MisFranchise: row[9],
                                    MisBUGroup: row[10],
                                    ModificationDate: new Date(),
                                    Active: 1,
                                    ProductType: 'Inventory'
                                }
                                await inventory.findByIdAndUpdate(resultInventory._id, inventoryObj).then(result => {
                                    var historyData = {
                                        InventoryID: resultInventory._id,
                                        Price: row[4],
                                        Quantity: parseInt(row[5])
                                    };
                                    SaveInventoryHistory(historyData);
                                    resolvenew();
                                }).catch(err => {
                                    console.log(err);
                                    rejectnew();
                                });
                            } else {
                                var inventoryObj = {
                                    Name: row[0],
                                    ProductCode: row[1],
                                    Type: row[2],
                                    Description: row[3],
                                    Price: row[4],
                                    TotalQuantity: row[5],
                                    AvailableQuantity: row[5],
                                    ProductConsumable: row[6],
                                    Inventory: row[7],
                                    BranchCode: row[8],
                                    MisFranchise: row[9],
                                    MisBUGroup: row[10],
                                    ModificationDate: new Date(),
                                    Active: 1,
                                    ProductType: 'Inventory'
                                }
                                inventoryObj = new inventory(inventoryObj);
                                await inventoryObj.save().then(result => {
                                    var historyData = {
                                        InventoryID: inventoryObj._id,
                                        Price: row[4],
                                        Quantity: parseInt(row[5])
                                    };
                                    SaveInventoryHistory(historyData);
                                    resolvenew();
                                }).catch(err => {
                                    console.log(err);
                                    rejectnew();
                                })
                            }
                        })
                    } else {
                        resolvenew();
                    }
                })
            }))
            res.status(200).json({
                'status': true,
                'message': 'Successfully updated record',
                'result': excelData
            })
        }).catch(err => {
            next(err);
        })
    } else {
        res.status(200).json({
            'status': false,
            'message': 'Something went wrong',
            'result': null
        })
    }
}