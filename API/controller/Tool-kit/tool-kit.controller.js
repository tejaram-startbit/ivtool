const toolkit = require('../../models/tool-kit.model');

// create
exports.create = function(req, res, next) {
    try {
        let data = JSON.parse(req.body.data);
        data.ModificationDate = new Date();
        data.Active = 1;

        const toolkits = new toolkit(data);

        toolkit.findOne({ Name: data.Name }).then(result => {
            if (result) {
                res.status(200).json({
                    'status': false,
                    'message': 'Tool kit name has been already exist , please try with diffrent name',
                    'result': {}
                })
            } else {

                toolkits.save().then(result => {
                    console.log(toolkits)
                    res.status(200).json({
                        'status': true,
                        'message': 'Successfully created toolkit',
                        'result': toolkits
                    })
                }).catch(err => {
                    console.log(err)
                    next(err);
                })

            }
        }).catch(err => {
            next(err);
        });
    } catch (e) {
        next(e)
    }

}

// update
exports.update = function(req, res, next) {
    let data = JSON.parse(req.body.data);

    let id = req.params.id;
    data.ModificationDate = new Date();
    data.Active = 1;
    console.log(data)

    toolkit.findByIdAndUpdate(id, data, { new: true }).then(result => {
        res.status(200).json({
            'status': true,
            'message': 'Successfully updated record',
            'result': result
        })
    }).catch(err => {
        console.log(err);
        next(err);
    })

}

// getOne
exports.getOne = function(req, res, next) {
    let id = req.params._id;
    toolkit.findById(id)
        // .select(['Name', 'ProductCode', 'Image', 'Type', 'Description', 'TotalQuantity', 'AvailableQuantity', 'Price', 'ProductConsumable', 'Inventory', 'BranchCode'])
        .then(result => {
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': result
            })
        }).catch(err => {
            next(err);
        })
}

// getAll
exports.getAll = function(req, res, next) {
    const condition = {
        'Active': 1
    };
    toolkit.find(condition)
        .sort({ _id: 'desc' })
        .populate({ path: 'Inventory.Inventory_ID' })
        .then(result => {
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': result
            })
        }).catch(err => {
            next(err);
        })
}


// delete
exports.delete = function(req, res, next) {
    let id = req.params._id;
    toolkit.findByIdAndUpdate(id, { Active: 0 }).then(result => {
        res.status(200).json({
            'status': true,
            'message': 'Successfully deleted record',
            'result': result
        })
    }).catch(err => {
        next(err);
    })
}