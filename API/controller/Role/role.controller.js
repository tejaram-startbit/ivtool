const roles = require('../../models/role.model');

// create
exports.create = function (req, res, next) {
    let data = req.body;
    data.ModificationDate = new Date();
    data.Active = 1;

    const role = new roles(data);
    role.save().then(result => {
        res.status(200).json({
            'status': true,
            'message': 'Successfully created record',
            'result': role
        })
    }).catch(err => {
        next(err);
    })
}

// update
exports.update = function (req, res, next) {
    let id = req.params._id;
    let data = req.body;
    data.ModificationDate = new Date();

    roles.findByIdAndUpdate(id, data, {new: true}).then(result => {
        res.status(200).json({
            'status': true,
            'message': 'Successfully updated record',
            'result': result
        })
    }).catch(err => {
        next(err);
    })
}

// getOne
exports.getOne = function (req, res, next) {
    let id = req.params._id;
    roles.findById(id).select(['Name', 'Description']).then(result => {
        res.status(200).json({
            'status': true,
            'message': 'Success',
            'result': result
        })
    }).catch(err => {
        next(err);
    })
}

// getAll
exports.getAll = function (req, res, next) {
    const condition = {
        'Active': 1
    };
    roles.find(condition).select(['Name', 'Description']).sort({_id: -1}).then(result => {
        res.status(200).json({
            'status': true,
            'message': 'Success',
            'result': result
        })
    }).catch(err => {
        next(err);
    })
}

// delete
exports.delete = function (req, res, next) {
    let id = req.params._id;
    roles.findByIdAndUpdate(id, {Active: 0}).then(result => {
        res.status(200).json({
            'status': true,
            'message': 'Successfully deleted record',
            'result': result
        })
    }).catch(err => {
        next(err);
    })
}
