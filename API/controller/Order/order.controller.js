const order = require('../../models/order.model');
const product = require('../../models/product.model');
const dispatch = require('../../models/dispatch.model');
var async = require("async");
const orderTimeLine = require('../../models/order-timeline.model');
const email = require('../../routes/email-functions');
const cryptoRandomString = require('crypto-random-string');
const user = require('../../models/user.model');
const acknowledge = require('../../models/acknowledge.model');
const returnOrder = require('../../models/return.model');
const transferOrder = require('../../models/transfer.model');
const notificationTimeLine = require('../../models/notification-timeline.model');
const costLevel = require('../../models/costlevel.model');
const transfer_history = require('../../models/transfer-history.model');
var dateFormat = require('dateformat');
const roles = require('../../models/role.model');
const common_config = require('../../common.config');
const transferRequestModel = require('../../models/transfer-request.model');
const { ObjectId } = require('mongodb');

function createOrderTimeLine(data) {
    data.ModificationDate = new Date();
    data.Active = 1;
    data = new orderTimeLine(data);
    data.save().then(result => {
        return true;
    }).catch(err => {
        next(err);
    })
}

function createNotificationTimeLine(data) {
    data.ModificationDate = new Date();
    data.Active = 1;
    data = new notificationTimeLine(data);
    data.save().then(result => {
        return true;
    })
}

function createTransferHistory(data) {
    data.ModificationDate = new Date();
    data.Active = 1;
    data = new transfer_history(data);
    data.save().then(result => {
        return true;
    })
}

async function createCustomer(userObj) {
    let UID = 0;
    await roles.findOne({ Description: 'Customer' }).then(resultRole => {
        user.findOne({ FirstName: userObj.FirstName, Email: userObj.Email, Phone: userObj.Phone, Active: 1 }).then(resultUser => {
            if (resultUser) {
                return true;
            } else {
                user.findOne().sort({ _id: 'desc' }).then(result => {
                    if (result) {
                        UID = Number(result['UID_Temp']) + 1;
                    } else {
                        UID = 1;
                    }

                    userObj.UID_Temp = UID;
                    userObj.UID = 'IVTOOL' + UID
                    userObj.RoleID = resultRole._id;
                    userObj.ModificationDate = new Date();
                    userObj.Active = 1;
                    userObj.save().then((result, err) => {
                        if (err) {
                            console.log(err)
                        }
                        return true;
                    })
                })
            }
        }).catch(err => {
            next(err);
        })
    })
}

// create
exports.create = function(req, res, next) {
    try {
        let data = JSON.parse(req.body.data);
        data.CreatedDate = new Date();
        data.ModificationDate = new Date();
        data.Active = 1;
        data.Status = 1;

        var totalPrice = 0;
        var i = 0;
        var htmlContent = '';
        const sales = '';

        for (let p of data.Products) {
            let condition = {
                //'Active': 1,
                'ProductCode': p.ProductCode
            };
            product.findOne(condition)
                .select(['Price', 'Name'])
                .then(async result => {
                    totalPrice = totalPrice + (p.Price * p.Quantity);
                    i = i + 1;
                    htmlContent += '<meta http-equiv="Content-Type" content="text/html;charset=utf-8"> <tr>';
                    htmlContent += '<td style="width:65%;color: #fff;border-bottom: 1px dotted;">' + p.Name + '</td><td style="width:20%;color: #fff;border-bottom: 1px dotted;">' + p.Quantity + '</td><td style="text-align:right;width:15%;color: #fff;border-bottom: 1px dotted;"> ₹' + p.Price + '</td>';
                    htmlContent += '</tr>';
                    if (data.Products.length == i) {
                        data.TotalAmount = totalPrice;
                        const orders = new order(data);
                        if (req.file) {
                            let path = req.file.path;
                            path = path.replace('public\\', '');
                            path = path.replace('public/', '');
                            orders.Attachment = path;
                        }
                        var OrderTimeLine = {
                            OrderID: orders._id,
                            Message: 'Order Created successfully',
                            ModifiedBy: orders.SalesPersonID,
                            Status: 1
                        }
                        createOrderTimeLine(OrderTimeLine);
                        createNotificationTimeLine(OrderTimeLine);

                        if (orders.CustomerDetails.CustomerID == 1) {
                            var userObj = {
                                FirstName: orders.CustomerDetails.CustomerName,
                                Email: orders.CustomerDetails.Email,
                                Phone: orders.CustomerDetails.Mobile,
                                Address: orders.CustomerDetails.Address
                            }
                            userObj = new user(userObj);
                            orders.CustomerDetails.CustomerID = userObj._id;
                            createCustomer(userObj);
                        }

                        order.findOne({ Active: 1 })
                            .select(['RequestID'])
                            .sort({ _id: 'desc' })
                            .then(resultRequestID => {
                                var rqID = 400000;
                                if (resultRequestID) {
                                    var r = resultRequestID.RequestID;
                                    var string = r.split("-");
                                    rqID = parseInt(string[1]) + 1;
                                }

                                if (orders.ServiceType == 'Demo') {
                                    orders.RequestID = 'DR-' + rqID;
                                } else {
                                    orders.RequestID = 'IR-' + rqID;
                                }

                                orders.save().then(result1 => {
                                    // user.find({Active: 1, MinCostLevel: {$lte: orders.TotalAmount.value}, MaxCostLevel: {$gte: orders.TotalAmount.value}})

                                    const map = new Map();

                                    const resultBuGroup = [];
                                    for (const item of data.Products) {
                                        if (!map.has(item.MisBUGroup)) {
                                            map.set(item.MisBUGroup, true); // set any value to Map
                                            if (item.MisBUGroup)
                                                resultBuGroup.push(item.MisBUGroup);
                                        }
                                    }

                                    const resultFranchise = [];
                                    for (const item of data.Products) {
                                        if (!map.has(item.MisFranchise)) {
                                            map.set(item.MisFranchise, true); // set any value to Map
                                            if (item.MisFranchise)
                                                resultFranchise.push(item.MisFranchise);
                                        }
                                    }
                                    user.find({
                                            Active: 1,
                                            $or: [{
                                                    MisBUGroup: { $in: resultBuGroup }
                                                },
                                                {
                                                    MisFranchise: { $in: resultFranchise }
                                                }
                                            ]
                                        })
                                        .then(result2 => {
                                            result2 = result2.filter(x => x.MisBUGroup || x.MisFranchise);
                                            const server = common_config.server_domain;
                                            for (let item of result2) {
                                                var replacementsValue = {
                                                    Email: item.Email,
                                                    ServiceType: data.ServiceType,
                                                    ScheduleReturnData: data.ScheduleReturnDate ? dateFormat(data.ScheduleReturnDate, 'dddd, mmmm dS, yyyy') : null,
                                                    DeliveryLocation: data.DeliveryLocation,
                                                    TotalAmount: '₹' + orders.TotalAmount,
                                                    InnerHtmlContent: htmlContent,
                                                    OrderLink: server + '/#/order-request/' + item._id + '/' + orders._id + '/' + orders.RequestID
                                                };
                                                email(item.Email, 'Order Verification - ' + data.RequestID, 'Order-Approval', replacementsValue);
                                            }
                                            if (result2.length > 0) {
                                                res.status(200).json({
                                                    'status': true,
                                                    'message': 'Successfully created record',
                                                    'result': result1
                                                });
                                            } else {
                                                res.status(200).json({
                                                    'status': true,
                                                    'message': 'Successfully created order but cost approval not found',
                                                    'result': result1
                                                });
                                            }
                                        }).catch(err => {
                                            next(err);
                                        })
                                }).catch(err => {
                                    console.log(err)
                                    next(err);
                                });
                            })
                    }

                }).catch(err => {
                    next(err);
                });
        }
    } catch (e) {
        console.log(e);
    }
}

exports.acknoledgeCreate = function(req, res, next) {
    try {
        let data = JSON.parse(req.body.data);
        let orderid = req.body.orderid;
        let acknowledges = new acknowledge(data);
        //acknowledge
        acknowledges.OrderID = orderid;
        if (req.file) {
            let path = req.file.path;
            path = path.replace('public\\', '');
            path = path.replace('public/', '');
            acknowledges.Picture = path;
        }

        acknowledges.save().then(result => {
            let orderdata = {};
            orderdata.Status = 6;

            order.findByIdAndUpdate(orderid, orderdata, { new: true })
                .populate({ path: 'UserID', select: ['FirstName', 'LastName', 'Phone'] })
                .populate({ path: 'Products.ProductID' })
                .populate({ path: 'SalesPersonID', select: ['Email'] })
                .populate({ path: 'OrderManagementID', select: ['Email'] })
                .then(resultOrder => {
                    var OrderTimeLine = {
                        OrderID: orderid,
                        Message: 'Order Acknowledged successfully',
                        ModifiedBy: resultOrder.SalesPersonID,
                        Status: 6
                    }
                    createOrderTimeLine(OrderTimeLine);
                    createNotificationTimeLine(OrderTimeLine);

                    var emailData = {
                        ServerLink: common_config.server_domain,
                        SalesPerson: {
                            Name: resultOrder.SalesPersonID ? resultOrder.SalesPersonID.FirstName : '',
                            Email: resultOrder.SalesPersonID ? resultOrder.SalesPersonID.Email : common_config.EmailID,
                            Phone: resultOrder.SalesPersonID ? resultOrder.SalesPersonID.Phone : ''
                        },
                        OrderDetail: {
                            ServiceType: resultOrder.ServiceType,
                            RequestID: resultOrder.RequestID,
                            Status: 6,
                            DeliveryLocation: resultOrder.DeliveryLocation,
                            ScheduleReturnDate: resultOrder.ScheduleReturnDate ? dateFormat(resultOrder.ScheduleReturnDate, 'dddd, mmmm dS, yyyy') : null,
                        },
                        Detail: acknowledges.Remaks,
                        Product: resultOrder.Products,
                        Attachment: [
                            // {
                            //     filename: 'ProductDetailAttachment.xlsx',
                            //     path: req.headers.origin + '/' + dispatchData.ProductDetailAttachment
                            // },
                            // {
                            //     filename: 'ProductInstructionPdf.pdf',
                            //     path: req.headers.origin + '/' + dispatchData.ProductInstructionPdf
                            // }
                        ]
                    }
                    email(resultOrder.OrderManagementID.Email, 'Order acknowledged successfully - ' + resultOrder.RequestID, 'order-acknowledge', emailData);
                    for (let p of resultOrder.Products) {
                        const qty = p.ProductID.AvailableQuantity - p.Quantity;
                        let data = {
                            'AvailableQuantity': qty
                        };
                        product.findByIdAndUpdate(p.ProductID._id, data, { new: true })
                            .then(result34 => {

                            }).catch(err => {
                                //next(err);
                            })
                    }

                }).catch(err => {
                    //next(err);
                })
            res.status(200).json({
                'status': true,
                'message': 'Successfully acknowledged',
                'result': result
            })
        }).catch(err => {
            next(err);
        })
    } catch (e) {
        console.log("entering catch block");
        console.log(e);
        console.log("leaving catch block");
    }

}

// update
exports.update = function(req, res, next) {
    try {
        let data = JSON.parse(req.body.data);
        //let data = req.body;
        let id = req.params._id;
        data.ModificationDate = new Date();
        data.Active = 1;
        data.Status = 1;

        //console.log(data);
        order.findByIdAndUpdate(id, data, { new: true })
            .populate({ path: 'Products.ProductID' })
            .populate({ path: 'SalesPersonID' })
            .populate({ path: 'ApproverID' })
            .then(result => {
                const server = common_config.server_domain;
                var replacementsValue = {
                    RequestID: result.RequestID,
                    SalesPerson: {
                        Name: result.SalesPersonID ? result.SalesPersonID.FirstName + ' ' + result.SalesPersonID.LastName : '',
                        Email: result.SalesPersonID ? result.SalesPersonID.Email : common_config.EmailID,
                        Phone: result.SalesPersonID ? result.SalesPersonID.Phone : ''
                    },
                    ServiceType: result.ServiceType,
                    ScheduleReturnData: result.ScheduleReturnDate ? dateFormat(result.ScheduleReturnDate, 'dddd, mmmm dS, yyyy') : null,
                    DeliveryLocation: result.DeliveryLocation,
                    TotalAmount: '?' + result.TotalAmount,
                    Product: result.Products,
                    OrderLink: server + '/#/order-request/' + result.ApproverID._id + '/' + result._id + '/' + result.RequestID
                };

                email(result.ApproverID.Email, 'Order Re-Verification - ' + result.RequestID, 'Order-ReApproval', replacementsValue);
                var OrderTimeLine = {
                    OrderID: id,
                    Message: 'Order Updated successfully',
                    Status: 1
                }
                createOrderTimeLine(OrderTimeLine);
                createNotificationTimeLine(OrderTimeLine);
                res.status(200).json({
                    'status': true,
                    'message': 'Successfully updated record',
                    'result': result
                })
            }).catch(err => {
                next(err);
            })
    } catch (e) {
        console.log(e);
    }
}

// getOne
exports.getOne = function(req, res, next) {
    let id = req.params._id;
    order.findById(id)
        .populate({ path: 'Products.ProductID', select: ['Name', 'ProductCode'] })
        .populate({ path: 'SalesPersonID', select: ['FirstName', 'LastName', 'Phone', 'Email', 'Address'] })
        .then(result => {
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': result
            })
        }).catch(err => {
            next(err);
        });
}

//getOneOrderByRequestID
exports.getOneOrderByRequestID = function(req, res, next) {
    let id = req.params._id;
    let salesid = req.params._salesid;
    const condition = {
        'RequestID': id,
        //'SalesPersonID': salesid
    }
    order.find(condition)
        .populate([{
            path: 'Products.ProductID',
            select: ['Name']
        }])
        .then(result => {
            if (result.length > 0) {
                result[0].Products = result[0].Products.filter(x => x.Active == 1)
            }
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': result
            })
        }).catch(err => {
            next(err);
        });
}

//getAllSP
exports.getAllSP = function(req, res, next) {
    let salesid = req.params._salesid;
    const RoleName = 'Dealer';

    roles.findOne({ 'Active': 1, 'Description': 'Dealer' })
        .then(resultrole => {
            const condition = {
                'Active': 1,
                'RoleID': resultrole._id,
                '_id': { $nin: [salesid] }
            };
            user.find(condition)
                .sort({ _id: 'desc' })
                .then(result => {
                    res.status(200).json({
                        'status': true,
                        'message': 'Success',
                        'result': result
                    })
                }).catch(err => {
                    next(err);
                })
        })
        .catch(err => {
            next(err);
        })
}

// getAllCustomerNSP
exports.getAllCustomerNSP = function(req, res, next) {
    let id = req.params.salesid;
    const RoleName = 'Dealer';
    const condition = {
        'Active': 1,
        // 'RoleID': '5e15a4f79f08b53f703453de'
    };
    user.find(condition)
        .populate({ path: 'RoleID', match: { Description: 'Dealer' } })
        .sort({ _id: 'desc' })
        .then(result => {
            result = result.filter(x => x.RoleID);
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': result
            })
        }).catch(err => {
            next(err);
        })
}

// getSalesAll
exports.getSalesAll = function(req, res, next) {
    let id = req.params.salesid;
    const condition = {
        'Active': 1,
        'SalesPersonID': id
    };
    order.find(condition)
        .sort({ _id: 'desc' })
        //.select(['CustomerDetails', 'DeliveryLocation', 'ServiceType', 'ReturnDate', 'Consumable'])
        .then(result => {
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': result
            })
        }).catch(err => {
            next(err);
        })
}

// getAll
exports.getAll = function(req, res, next) {
    const condition = {
        'Active': 1
    };
    order.find(condition)
        .sort({ ModificationDate: 'desc' })
        // .select(['ServiceType', 'RequestID', 'CustomerDetails', 'DeliveryLocation', 'ScheduleReturnDate', 'Status', 'TotalAmount', 'Attachment', 'ApproverID', 'CreatedDate', 'ReasonMessage', 'IsUpdated', 'ModificationDate'])
        .populate({ path: 'UserID', select: ['FirstName', 'LastName', 'Phone'] })
        .populate({ path: 'Products.ProductID' })
        .populate({ path: 'ReturnProducts.ProductID' })
        .populate({ path: 'SalesPersonID', select: ['FirstName', 'LastName', 'Phone', 'Email', 'Address'] })
        .then(result => {
            result.map(item => {
                item.Products = item.Products.filter(x => x.Active == 1)
            })
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': result
            })
        }).catch(err => {
            next(err);
        })
}

// getAllByLastOneYear
exports.getAllByLastOneYear = function(req, res, next) {
    let dateNow = new Date();
    dateNow.setFullYear(dateNow.getFullYear() - 1);
    const condition = {
        'Active': 1,
        'CreatedDate': { '$gte': dateNow }
    };
    order.find(condition)
        .sort({ ModificationDate: 'desc' })
        // .select(['ServiceType', 'RequestID', 'CustomerDetails', 'DeliveryLocation', 'ScheduleReturnDate', 'Status', 'TotalAmount', 'Attachment', 'ApproverID', 'CreatedDate', 'ReasonMessage', 'IsUpdated', 'ModificationDate'])
        .populate({ path: 'UserID', select: ['FirstName', 'LastName', 'Phone'] })
        .populate({ path: 'Products.ProductID' })
        .populate({ path: 'SalesPersonID', select: ['FirstName', 'LastName', 'Phone', 'Email', 'Address'] })
        .then(result => {
            result.map(item => {
                item.Products = item.Products.filter(x => x.Active == 1)
            })
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': result
            })
        }).catch(err => {
            next(err);
        })
}

// getAllLastMonth
exports.getAllLastMonth = function(req, res, next) {
    let date = new Date();
    var firstDay = new Date(date.getFullYear(), date.getMonth() - 1, 1);
    var lastDay = new Date(date.getFullYear(), date.getMonth(), 0);
    const condition = {
        'Active': 1,
        'CreatedDate': { '$gte': new Date(firstDay), '$lte': new Date(lastDay) }
    };
    order.find(condition)
        .sort({ ModificationDate: 'desc' })
        // .select(['ServiceType', 'RequestID', 'CustomerDetails', 'DeliveryLocation', 'ScheduleReturnDate', 'Status', 'TotalAmount', 'Attachment', 'ApproverID', 'CreatedDate', 'ReasonMessage', 'IsUpdated', 'ModificationDate'])
        .populate({ path: 'UserID', select: ['FirstName', 'LastName', 'Phone'] })
        .populate({ path: 'Products.ProductID' })
        .populate({ path: 'ReturnProducts.ProductID' })
        .populate({ path: 'SalesPersonID', select: ['FirstName', 'LastName', 'Phone', 'Email', 'Address'] })
        .then(result => {
            result.map(item => {
                item.Products = item.Products.filter(x => x.Active == 1)
            })
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': result
            })
        }).catch(err => {
            next(err);
        })
}

// getAllBySalesPersonID
exports.getAllBySalesPersonID = function(req, res, next) {
    let id = req.params.salesid;
    const condition = {
        'Active': 1,
        'SalesperosnID': id
    };
    order.find(condition)
        .sort({ ModificationDate: 'desc' })
        // .select(['ServiceType', 'RequestID', 'CustomerDetails', 'DeliveryLocation', 'ScheduleReturnDate', 'Status', 'TotalAmount', 'Attachment', 'ApproverID', 'CreatedDate', 'ReasonMessage', 'IsUpdated', 'ModificationDate'])
        .populate({ path: 'UserID', select: ['FirstName', 'LastName', 'Phone'] })
        .populate({ path: 'Products.ProductID' })
        .populate({ path: 'ReturnProducts.ProductID' })
        .populate({ path: 'SalesPersonID', select: ['FirstName', 'LastName', 'Phone', 'Email', 'Address'] })
        .then(result => {
            result.map(item => {
                item.Products = item.Products.filter(x => x.Active == 1)
            })
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': result
            })
        }).catch(err => {
            next(err);
        })
}

// delete
exports.delete = function(req, res, next) {
    let id = req.params._id;
    order.findByIdAndUpdate(id, { Active: 0 }).then(result => {
        res.status(200).json({
            'status': true,
            'message': 'Successfully deleted record',
            'result': result
        })
    }).catch(err => {
        next(err);
    })
}

// updateFinalProductDetail
exports.updateFinalProductDetail = function(req, res, next) {
    let data = JSON.parse(req.body.data);
    let OrderID = req.body.OrderID;
    let OrderManagementID = data.OrderManagementID;
    data.OrderID = OrderID;
    data.ModificationDate = new Date();
    data.Active = 1;
    const dispatchData = new dispatch(data);
    dispatchData.Documents = [];
    if (req.files) {
        for (let file of req.files) {
            let path = file.path;
            path = path.replace('public\\', '');
            path = path.replace('public/', '');
            dispatchData.Documents.push(path);
        }

    }
    dispatchData.save().then(result => {
        var OrderTimeLine = {
            OrderID: OrderID,
            Message: 'Order final detail updated successfully',
            ModifiedBy: data.OrderManagementID,
            Status: 2
        }
        createOrderTimeLine(OrderTimeLine);
        createNotificationTimeLine(OrderTimeLine);
        res.status(200).json({
            'status': true,
            'message': 'Order final detail updated successfully',
            'result': dispatchData
        })
    }).catch(err => {
        next(err);
    })
}

// dispatchedOrder
exports.dispatchedOrder = function(req, res, next) {
    let data = JSON.parse(req.body.data);
    let DispatchOrderID = req.body.OrderID;
    let OrderManagementID = data.OrderManagementID;
    data.ModificationDate = new Date();
    data.Active = 1;
    const dispatchData = data;
    if (req.files.excelFile[0]) {
        let file = req.files.excelFile[0];
        let path = file.path;
        path = path.replace('public\\', '');
        path = path.replace('public/', '');
        dispatchData.ProductDetailAttachment = path;
    }
    if (req.files.pdfFile[0]) {
        let file = req.files.pdfFile[0];
        let path = file.path;
        path = path.replace('public\\', '');
        path = path.replace('public/', '');
        dispatchData.ProductInstructionPdf = path;
    }
    dispatch.findByIdAndUpdate(DispatchOrderID, data, { new: true })
        .then(resultDispatch => {
            let OrderID = resultDispatch.OrderID;
            order.findByIdAndUpdate(OrderID, { Status: 4, ModificationDate: new Date(), OrderManagementID: OrderManagementID }, { new: true })
                .populate({ path: 'Products.ProductID' })
                .populate({ path: 'SalesPersonID' })
                .then(resultOrder => {
                    var emailData = {
                        ServerLink: common_config.server_domain,
                        SalesPerson: {
                            Name: resultOrder.SalesPersonID ? resultOrder.SalesPersonID.FirstName + ' ' + resultOrder.SalesPersonID.LastName : '',
                            Email: resultOrder.SalesPersonID ? resultOrder.SalesPersonID.Email : common_config.EmailID,
                            Phone: resultOrder.SalesPersonID ? resultOrder.SalesPersonID.Phone : ''
                        },
                        OrderDetail: {
                            ServiceType: resultOrder.ServiceType,
                            RequestID: resultOrder.RequestID,
                            Status: 4,
                            DeliveryLocation: resultOrder.DeliveryLocation,
                            ScheduleReturnDate: resultOrder.ScheduleReturnDate ? dateFormat(resultOrder.ScheduleReturnDate, 'dddd, mmmm dS, yyyy') : null
                        },
                        dispatchData: {
                            HAWB_No: dispatchData.HAWB_No,
                            HAWB_Date: dateFormat(dispatchData.HAWB_Date, 'dddd, mmmm dS, yyyy'),
                            ETA: dateFormat(dispatchData.ETA, 'dddd, mmmm dS, yyyy'),
                            JDE_Invoice_No: dispatchData.JDE_Invoice_No
                        },
                        Detail: data.ReasonMessage,
                        Product: resultOrder.Products,
                        Attachment: [
                            // {
                            //     filename: 'ProductDetailAttachment.xlsx',
                            //     path: req.headers.origin + '/' + dispatchData.ProductDetailAttachment
                            // },
                            // {
                            //     filename: 'ProductInstructionPdf.pdf',
                            //     path: req.headers.origin + '/' + dispatchData.ProductInstructionPdf
                            // }
                        ]
                    }
                    email(emailData.SalesPerson.Email, 'Order Dispatched successfully - ' + resultOrder.RequestID, 'dispatch-order', emailData);

                    var OrderTimeLine = {
                        OrderID: OrderID,
                        Message: 'Order Dispatched successfully',
                        ModifiedBy: data.OrderManagementID,
                        Status: 4
                    }
                    createOrderTimeLine(OrderTimeLine);
                    createNotificationTimeLine(OrderTimeLine);
                    res.status(200).json({
                        'status': true,
                        'message': 'Successfully dispatched order',
                        'result': dispatchData
                    })
                }).catch(err => {
                    next(err);
                })
        }).catch(err => {
            next(err);
        })
}

// orderDelivered
exports.orderDelivered = function(req, res, next) {
    let OrderID = req.params._id;
    let _currentUser = req.params._currentUser;
    order.findByIdAndUpdate(OrderID, { Status: 5, ModificationDate: new Date() }, { new: true })
        .populate({ path: 'Products.ProductID' })
        .populate({ path: 'SalesPersonID' })
        .then(resultOrder => {
            var OrderTimeLine = {
                OrderID: OrderID,
                Message: 'Order Delivered successfully',
                ModifiedBy: _currentUser,
                Status: 5
            }
            createOrderTimeLine(OrderTimeLine);
            createNotificationTimeLine(OrderTimeLine);
            var emailData = {
                ServerLink: common_config.server_domain,
                SalesPerson: {
                    Name: resultOrder.SalesPersonID ? resultOrder.SalesPersonID.FirstName + ' ' + resultOrder.SalesPersonID.LastName : '',
                    Email: resultOrder.SalesPersonID ? resultOrder.SalesPersonID.Email : common_config.EmailID,
                    Phone: resultOrder.SalesPersonID ? resultOrder.SalesPersonID.Phone : ''
                },
                OrderDetail: {
                    ServiceType: resultOrder.ServiceType,
                    RequestID: resultOrder.RequestID,
                    Status: 5,
                    DeliveryLocation: resultOrder.DeliveryLocation,
                    TotalAmount: resultOrder.TotalAmount,
                    ScheduleReturnDate: resultOrder.ScheduleReturnDate ? dateFormat(resultOrder.ScheduleReturnDate, 'dddd, mmmm dS, yyyy') : null,
                },
                Detail: 'Order Delivered successfully',
                Product: resultOrder.Products,
                Attachment: []
            }
            email(emailData.SalesPerson.Email, 'Order delivered successfully - ' + resultOrder.RequestID, 'delivered-order', emailData);
            if (resultOrder.CustomerDetails && resultOrder.CustomerDetails.Email) {
                user.findOne({ FirstName: resultOrder.CustomerDetails.CustomerName, Email: resultOrder.CustomerDetails.Email }).then(userResult => {
                    if (userResult) {
                        var server = common_config.server_domain;
                        emailData.OrderLink = server + '/#/order-acknowledge/' + userResult._id + '/' + OrderID + '/' + resultOrder.RequestID,
                            email(resultOrder.CustomerDetails.Email, 'Order acknowledge request - ' + resultOrder.RequestID, 'Order-acknowledge-request', emailData);
                    }
                })
            }
            res.status(200).json({
                'status': true,
                'message': 'Successfully delivered order',
                'result': resultOrder
            })
        }).catch(err => {
            next(err);
        })
}

// approveOrder
exports.approveOrder = function(req, res, next) {
    let OrderID = req.params._id;
    let data = req.body;
    user.findById(data.ApproverID)
        .then(resultUser => {
            if (resultUser) {
                order.findById(OrderID)
                    .populate({ path: 'Products.ProductID' })
                    .populate({ path: 'SalesPersonID' })
                    .then(resultOrder => {
                        var emailData = {
                            ServerLink: common_config.server_domain,
                            SalesPerson: {
                                Name: resultOrder.SalesPersonID ? resultOrder.SalesPersonID.FirstName + ' ' + resultOrder.SalesPersonID.LastName : '',
                                Email: resultOrder.SalesPersonID ? resultOrder.SalesPersonID.Email : common_config.EmailID,
                                Phone: resultOrder.SalesPersonID ? resultOrder.SalesPersonID.Phone : ''
                            },
                            CostApproval: {
                                Name: resultUser.FirstName + ' ' + resultUser.LastName,
                                Email: resultUser.Email ? resultUser.Email : common_config.EmailID,
                                Phone: resultUser.Phone
                            },
                            OrderDetail: {
                                ServiceType: resultOrder.ServiceType,
                                RequestID: resultOrder.RequestID,
                                Status: 2,
                                DeliveryLocation: resultOrder.DeliveryLocation,
                                ScheduleReturnDate: resultOrder.ScheduleReturnDate ? dateFormat(resultOrder.ScheduleReturnDate, 'dddd, mmmm dS, yyyy') : null,
                            },
                            Detail: 'Order request approved successfully',
                            Product: resultOrder.Products,
                            Attachment: []
                        }

                        order.findByIdAndUpdate(OrderID, { Status: 2, ApproverID: data.ApproverID, ModificationDate: new Date() }, { new: true }).then(result => {
                            var OrderTimeLine = {
                                OrderID: OrderID,
                                Message: 'Order Approved successfully',
                                ModifiedBy: data.ApproverID,
                                Status: 2
                            }
                            createOrderTimeLine(OrderTimeLine);
                            createNotificationTimeLine(OrderTimeLine);

                            user.find({ Active: 1 }).populate({ path: 'RoleID', match: { Description: 'OM Supervisory' } }).then(resultOm => {
                                resultOm = resultOm.filter(x => x.RoleID);
                                var OmEmailArr = [];
                                resultOm.map(item => {
                                    OmEmailArr.push(item.Email);
                                })
                                email(OmEmailArr, 'Order Request Approved - ' + resultOrder.RequestID, 'approve-order', emailData);
                            })

                            res.status(200).json({
                                'status': true,
                                'message': 'Successfully delivered order',
                                'result': result
                            })
                        }).catch(err => {
                            next(err);
                        })
                    }).catch(err => {
                        next(err);
                    })
            } else {
                res.status(200).json({
                    'status': false,
                    'message': 'Invalid Approver',
                    'result': null
                })
            }

        }).catch(err => {
            next(err);
        })
}

// disapproveOrder
exports.disapproveOrder = function(req, res, next) {
    let OrderID = req.params._id;
    let data = req.body;
    order.findById(OrderID)
        .populate({ path: 'UserID' })
        .populate({ path: 'Products.ProductID' })
        .populate({ path: 'SalesPersonID' })
        .then(resultOrder => {
            var emailData = {
                ServerLink: common_config.server_domain,
                SalesPerson: {
                    Name: resultOrder.SalesPersonID ? resultOrder.SalesPersonID.FirstName + ' ' + resultOrder.SalesPersonID.LastName : '',
                    Email: resultOrder.SalesPersonID ? resultOrder.SalesPersonID.Email : common_config.EmailID,
                    Phone: resultOrder.SalesPersonID ? resultOrder.SalesPersonID.Phone : ''
                },
                OrderDetail: {
                    ServiceType: resultOrder.ServiceType,
                    RequestID: resultOrder.RequestID,
                    Status: 3,
                    DeliveryLocation: resultOrder.DeliveryLocation,
                    ScheduleReturnDate: resultOrder.ScheduleReturnDate ? dateFormat(resultOrder.ScheduleReturnDate, 'dddd, mmmm dS, yyyy') : null,
                },
                Detail: data.ReasonMessage,
                Product: resultOrder.Products,
                Attachment: []
            }
            email(emailData.SalesPerson.Email, 'Order Request Disapproved - ' + resultOrder.RequestID, 'disapprove-order', emailData);
            order.findByIdAndUpdate(OrderID, { Status: 3, ReasonMessage: data.ReasonMessage, ApproverID: data.ApproverID, ModificationDate: new Date() }, { new: true }).then(result => {
                var OrderTimeLine = {
                    OrderID: OrderID,
                    Message: 'Order disapproved successfully',
                    ModifiedBy: data.ApproverID,
                    Status: 3
                }
                createOrderTimeLine(OrderTimeLine);
                createNotificationTimeLine(OrderTimeLine);
                res.status(200).json({
                    'status': true,
                    'message': 'Successfully disapproved order',
                    'result': result
                })
            }).catch(err => {
                next(err);
            })
        }).catch(err => {
            next(err);
        })
}

// declineOrder
exports.declineOrder = function(req, res, next) {
    let OrderID = req.params._id;
    let data = req.body;
    order.findById(OrderID)
        .populate({ path: 'UserID' })
        .populate({ path: 'Products.ProductID' })
        .populate({ path: 'SalesPersonID' })
        .then(resultOrder => {
            var emailData = {
                ServerLink: common_config.server_domain,
                SalesPerson: {
                    Name: resultOrder.SalesPersonID ? resultOrder.SalesPersonID.FirstName + ' ' + resultOrder.SalesPersonID.LastName : '',
                    Email: resultOrder.SalesPersonID ? resultOrder.SalesPersonID.Email : common_config.EmailID,
                    Phone: resultOrder.SalesPersonID ? resultOrder.SalesPersonID.Phone : ''
                },
                OrderDetail: {
                    ServiceType: resultOrder.ServiceType,
                    RequestID: resultOrder.RequestID,
                    Status: 10,
                    DeliveryLocation: resultOrder.DeliveryLocation,
                    ScheduleReturnDate: resultOrder.ScheduleReturnDate ? dateFormat(resultOrder.ScheduleReturnDate, 'dddd, mmmm dS, yyyy') : null,
                },
                Detail: data.ReasonMessage,
                Product: resultOrder.Products,
                Attachment: []
            }
            email(emailData.SalesPerson.Email, 'Order Declined - ' + resultOrder.RequestID, 'disapprove-order', emailData);
            order.findByIdAndUpdate(OrderID, { Status: 10, DeclineReasonMessage: data.ReasonMessage, ModificationDate: new Date() }, { new: true }).then(result => {
                var OrderTimeLine = {
                    OrderID: OrderID,
                    Message: 'Order declined successfully',
                    ModifiedBy: data.ApproverID,
                    Status: 10
                }
                createOrderTimeLine(OrderTimeLine);
                createNotificationTimeLine(OrderTimeLine);
                res.status(200).json({
                    'status': true,
                    'message': 'Successfully declined order',
                    'result': result
                })
            }).catch(err => {
                next(err);
            })
        }).catch(err => {
            next(err);
        })
}

// getDispatcheOrderDetail
exports.getDispatcheOrderDetail = function(req, res, next) {
    let OrderID = req.params._id;
    dispatch.findOne({ 'Active': 1, 'OrderID': OrderID }).sort({ _id: -1 }).then(result => {
        res.status(200).json({
            'status': true,
            'message': 'Successfully delivered order',
            'result': result
        })
    }).catch(err => {
        next(err);
    })
}

// getOrderTimeLine
exports.getOrderTimeLine = function(req, res, next) {
    let OrderID = req.params._id;
    orderTimeLine.find({ 'Active': 1, 'OrderID': OrderID })
        .sort({ _id: -1 })
        .populate({ path: 'ModifiedBy', select: ['FirstName', 'LastName', 'Phone', 'Email'] })
        .then(result => {
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': result
            })
        }).catch(err => {
            next(err);
        })
}

// returnOrder
exports.returnOrder = function(req, res, next) {
    let OrderID = req.params._id;
    let returnData = req.body.data;
    let ReturnProducts = req.body.ReturnProducts;
    returnData.OrderID = OrderID;
    returnData.Active = 1;
    returnData.ModificationDate = new Date();
    returnData.Products = ReturnProducts;
    returnData = new returnOrder(returnData);
    order.findByIdAndUpdate(OrderID, { Status: 7, ModificationDate: new Date() }, { new: true })
        .populate({ path: 'Products.ProductID' })
        .populate({ path: 'SalesPersonID' })
        .populate({ path: 'OrderManagementID' })
        .then(resultOrder => {
            var emailData = {
                ServerLink: common_config.server_domain,
                SalesPerson: {
                    Name: resultOrder.SalesPersonID ? resultOrder.SalesPersonID.FirstName + ' ' + resultOrder.SalesPersonID.LastName : '',
                    Email: resultOrder.SalesPersonID ? resultOrder.SalesPersonID.Email : common_config.EmailID,
                    Phone: resultOrder.SalesPersonID ? resultOrder.SalesPersonID.Phone : ''
                },
                OrderDetail: {
                    ServiceType: resultOrder.ServiceType,
                    RequestID: resultOrder.RequestID,
                    Status: 7,
                    DeliveryLocation: resultOrder.DeliveryLocation,
                    ScheduleReturnDate: resultOrder.ScheduleReturnDate ? dateFormat(resultOrder.ScheduleReturnDate, 'dddd, mmmm dS, yyyy') : null,
                },
                dispatchData: {
                    PickUpDate: returnData.PickUpDate,
                    PickUpLocation: returnData.PickUpLocation
                },
                Detail: 'Order return request send successfully',
                Product: resultOrder.Products,
                Attachment: []
            }

            returnData.save().then(result => {
                var OrderTimeLine = {
                    OrderID: OrderID,
                    Message: 'Order return request send successfully',
                    ModifiedBy: resultOrder.SalesPersonID,
                    Status: 7
                }
                createOrderTimeLine(OrderTimeLine);
                createNotificationTimeLine(OrderTimeLine);

                var OmEmail = resultOrder.OrderManagementID ? resultOrder.OrderManagementID.Email : 'tejaram@startbitsolutions.com';
                email(OmEmail, 'Order return request send successfully - ' + resultOrder.RequestID, 'return-order', emailData);

                res.status(200).json({
                    'status': true,
                    'message': 'Order return request send successfully',
                    'result': returnData
                })
            }).catch(err => {
                next(err);
            })
        }).catch(err => {
            next(err);
        })
}

// getTransferRequest
exports.getTransferRequest = function(req, res, next) {
    try {
        var OrderID = ObjectId(req.params._id);
        transferRequestModel.findOne({ OrderID: OrderID })
            .populate({ path: 'OldSalesPersonID', select: ['FirstName', 'LastName', 'Email', 'Phone'] })
            .populate({ path: 'NewSalesPersonID', select: ['FirstName', 'LastName', 'Email', 'Phone'] })
            .sort({ _id: -1 }).then(result => {
                res.status(200).json({
                    'status': true,
                    'message': 'Successfull',
                    'result': result
                })
            }).catch(err => {
                console.log(err)
                next(err);
            })
    } catch (e) {
        console.log(e)
    }
}

// transferRequestOrder
exports.transferRequestOrder = function(req, res, next) {
    try {
        let OrderID = req.params._id;
        let transferData = req.body;
        let data = {
            'SalesPersonID': ObjectId(transferData.CustomerID),
            'CustomerDetails': transferData.CustomerDetails,
            'DeliveryLocation': transferData.DeliveryLocation,
            'ModificationDate': new Date()
        }

        order.findById(OrderID)
            .populate({ path: 'SalesPersonID', select: ['Email', 'FirstName', 'LastName', 'Phone'] })
            .then(resultOrderOld => {
                var productsArr = [];
                var flag = 0;
                resultOrderOld.Products.map(item => {
                    if (item.ProductConsumable == 'Yes') {
                        item.Active = 0;
                    } else {
                        flag = 1;
                    }
                    productsArr.push(item);
                })
                if (flag == 0) {
                    res.status(200).json({
                        'status': false,
                        'message': 'Sorry, this order does not contain a product for transfer',
                        'result': transferData
                    })
                } else {
                    user.findOne(data.SalesPersonID).then(salesPersonResult => {
                        var orderTransferData = {
                            OldSalesPersonID: resultOrderOld.SalesPersonID,
                            NewSalesPersonID: data.SalesPersonID,
                            OrderID: OrderID,
                            TransferData: transferData,
                            Status: 1,
                            ModificationDate: new Date(),
                            ModifiedBy: transferData.currentUserID ? transferData.currentUserID : data.SalesPersonID,
                            Active: 1
                        }
                        orderTransferData = new transferRequestModel(orderTransferData);
                        orderTransferData.save().then(result => {
                            var OrderTimeLine = {
                                OrderID: OrderID,
                                Message: 'Order transfer request send ' + resultOrderOld.SalesPersonID.FirstName + ' ' + resultOrderOld.SalesPersonID.LastName + ' to ' + salesPersonResult.FirstName + ' ' + salesPersonResult.LastName + ' successfully',
                                ModifiedBy: transferData.currentUserID ? transferData.currentUserID : resultOrderOld.SalesPersonID,
                                Status: 8
                            }
                            createOrderTimeLine(OrderTimeLine);
                            createNotificationTimeLine(OrderTimeLine);
                            res.status(200).json({
                                'status': true,
                                'message': 'Successfully transfered order',
                                'result': orderTransferData
                            })
                        }).catch(err => {
                            console.log(err)
                            next(err);
                        })
                    }).catch(err => {
                        console.log(err)
                        next(err);
                    })
                }
            }).catch(err => {
                console.log(err)
                next(err);
            })
    } catch (e) {
        console.log(e);
    }
}

// transferOrder
exports.transferOrder = function(req, res, next) {
    try {
        let OrderID = req.params._id;
        let transferData = req.body;
        let isFullOrder = true;
        newProductsArr = transferData.Products;
        oldProductsArr = [];
        let data = {
            'SalesPersonID': transferData.CustomerID,
            'CustomerDetails': transferData.CustomerDetails,
            'DeliveryLocation': transferData.DeliveryLocation,
            'Products': transferData.Products,
            'ModificationDate': new Date()
        }
        order.findById(OrderID)
            .populate({ path: 'SalesPersonID', select: ['Email', 'FirstName', 'LastName', 'Phone'] })
            .then(resultOrderOld => {
                var productsArr = [];
                var flag = 0;
                resultOrderOld.Products.map(item => {
                    if (item.ProductConsumable == 'Yes') {
                        item.Active = 0;
                    } else {
                        flag = 1;
                    }
                    productsArr.push(item);
                })

                // check order is partial transfer or not
                productsArr.map(item => {
                    if (item.Active) {
                        var index = newProductsArr.findIndex(x => x.ProductCode == item.ProductCode && x.Quantity == item.Quantity && !x.value)
                        var newProductIndex = newProductsArr.findIndex(x => x.ProductCode == item.ProductCode);
                        if (index == -1) {
                            isFullOrder = false;
                            oldProductsArr.push({
                                Quantity: parseInt(item.Quantity) - parseInt(newProductsArr[newProductIndex].Quantity),
                                Active: 1,
                                _id: item._id,
                                ProductID: item.ProductID,
                                ProductCode: item.ProductCode,
                                ProductConsumable: item.ProductConsumable,
                                Price: item.Price,
                                Inventory: item.Inventory,
                                BranchCode: item.BranchCode
                            })
                        } else {
                            oldProductsArr.push(item);
                        }
                    } else {
                        oldProductsArr.push(item);
                    }
                })


                if (flag == 0) {
                    res.status(200).json({
                        'status': false,
                        'message': 'Sorry, this order does not contain a product for transfer',
                        'result': transferData
                    })
                }
                // full order transfer
                else if (isFullOrder) {
                    data.Products = productsArr;
                    data.Status = 5;
                    order.findByIdAndUpdate(OrderID, data, { new: true })
                        .populate({ path: 'Products.ProductID' })
                        .populate({ path: 'SalesPersonID', select: ['Email', 'FirstName', 'LastName', 'Phone'] })
                        .populate({ path: 'OrderManagementID', select: ['Email', 'FirstName', 'Phone'] })
                        .then(resultOrder => {
                            var emailData = {
                                ServerLink: common_config.server_domain,
                                SalesPerson: {
                                    Name: resultOrder.SalesPersonID ? resultOrder.SalesPersonID.FirstName : '',
                                    Email: resultOrder.SalesPersonID ? resultOrder.SalesPersonID.Email : 'bhoomikamehta@startbitsolutions.com',
                                    Phone: resultOrder.SalesPersonID ? resultOrder.SalesPersonID.Phone : ''
                                },
                                OrderDetail: {
                                    ServiceType: resultOrder.ServiceType,
                                    RequestID: resultOrder.RequestID,
                                    Status: 5,
                                    DeliveryLocation: resultOrder.DeliveryLocation,
                                    ScheduleReturnDate: resultOrder.ScheduleReturnDate ? dateFormat(resultOrder.ScheduleReturnDate, 'dddd, mmmm dS, yyyy') : null,
                                },
                                Product: resultOrder.Products,
                                Attachment: []
                            }
                            if (resultOrder.OrderManagementID) {
                                email(resultOrder.OrderManagementID.Email, 'Order transfered successfully - ' + resultOrder.RequestID, 'transfer-order', emailData);
                            } else {
                                user.find({ Active: 1 }).populate({ path: 'RoleID', match: { Description: 'OM Supervisory' } }).then(resultOm => {
                                    resultOm = resultOm.filter(x => x.RoleID);
                                    var OmEmailArr = [];
                                    resultOm.map(item => {
                                        OmEmailArr.push(item.Email);
                                    })
                                    email(OmEmailArr, 'Order transfered successfully - ' + resultOrder.RequestID, 'transfer-order', emailData);
                                })
                            }
                            var OrderTimeLine = {
                                OrderID: OrderID,
                                Message: 'Order transfered ' + resultOrderOld.SalesPersonID.FirstName + ' ' + resultOrderOld.SalesPersonID.LastName + ' to ' + resultOrder.SalesPersonID.FirstName + ' ' + resultOrder.SalesPersonID.LastName + ' successfully',
                                ModifiedBy: transferData.currentUserID ? transferData.currentUserID : resultOrder.SalesPersonID,
                                Status: 8
                            }
                            createOrderTimeLine(OrderTimeLine);
                            createNotificationTimeLine(OrderTimeLine);

                            var transferHistoryData = {
                                OrderID: OrderID,
                                OldOrderDetail: {
                                    SalesPersonID: resultOrderOld.SalesPersonID,
                                    CustomerDetails: resultOrderOld.CustomerDetails,
                                    DeliveryLocation: resultOrderOld.DeliveryLocation
                                },
                                NewOrderDetail: {
                                    SalesPersonID: transferData.CustomerID,
                                    CustomerDetails: transferData.CustomerDetails,
                                    DeliveryLocation: transferData.DeliveryLocation
                                },
                                ModifiedBy: transferData.currentUserID ? transferData.currentUserID : resultOrderOld.SalesPersonID
                            }
                            createTransferHistory(transferHistoryData);
                            transferRequestModel.findByIdAndUpdate(transferData.transferRequestID, { Status: 2, ModificationDate: new Date() }).exec();
                            res.status(200).json({
                                'status': true,
                                'message': 'Successfully transfered order',
                                'result': transferData
                            })
                        }).catch(err => {
                            next(err)
                        })
                }
                // partial transfer order 
                else {
                    resultOrderOld.Products = oldProductsArr.filter(x => x.Quantity > 0);
                    order.findByIdAndUpdate(OrderID, resultOrderOld, { new: true })
                        .populate({ path: 'Products.ProductID' })
                        .populate({ path: 'SalesPersonID', select: ['Email', 'FirstName', 'LastName', 'Phone'] })
                        .populate({ path: 'OrderManagementID', select: ['Email', 'FirstName', 'Phone'] })
                        .then(resultOrder => {
                            order.findOne({}).sort({ _id: -1 }).then(resultLastOrder => {
                                var newTotalAmount = 0;
                                newProductsArr.map(x => {
                                    if (x.value) {
                                        newTotalAmount += parseFloat(x.Price) * parseInt(x.Quantity);
                                    }
                                })

                                rqID = 400000;
                                if (resultLastOrder) {
                                    var r = resultLastOrder.RequestID;
                                    var string = r.split("-");
                                    rqID = parseInt(string[1]) + 1;
                                }
                                var transferRequestData = {
                                    OldOrderID: resultOrderOld._id,
                                    ServiceType: resultOrderOld.ServiceType,
                                    RequestID: 'DR-' + rqID,
                                    Products: newProductsArr.filter(x => x.value),
                                    UserID: resultOrderOld.UserID,
                                    CustomerDetails: data.CustomerDetails,
                                    DeliveryLocation: data.DeliveryLocation,
                                    ScheduleReturnDate: resultOrderOld.ScheduleReturnDate,
                                    Status: 5, //resultOrderOld.Status,
                                    TotalAmount: newTotalAmount,
                                    Attachment: resultOrderOld.Attachment,
                                    ApproverID: resultOrderOld.ApproverID,
                                    SalesPersonID: data.SalesPersonID,
                                    OldSalesPersonID: resultOrderOld.SalesPersonID._id,
                                    OrderManagementID: resultOrderOld.OrderManagementID,
                                    ReasonMessage: '',
                                    DeclineReasonMessage: '',
                                    IsUpdated: resultOrderOld.IsUpdated,
                                    ModificationDate: new Date(),
                                    ModifiedBy: transferData.currentUserID ? transferData.currentUserID : resultOrderOld.SalesPersonID,
                                    Active: 1
                                }

                                transferRequestData = new order(transferRequestData);
                                transferRequestData.save().then(resultTransferData => {

                                    var emailData = {
                                        ServerLink: common_config.server_domain,
                                        SalesPerson: {
                                            Name: resultOrder.SalesPersonID ? resultOrder.SalesPersonID.FirstName : '',
                                            Email: resultOrder.SalesPersonID ? resultOrder.SalesPersonID.Email : common_config.EmailID,
                                            Phone: resultOrder.SalesPersonID ? resultOrder.SalesPersonID.Phone : ''
                                        },
                                        OrderDetail: {
                                            ServiceType: resultOrder.ServiceType,
                                            RequestID: resultOrder.RequestID,
                                            Status: 5,
                                            DeliveryLocation: resultOrder.DeliveryLocation,
                                            ScheduleReturnDate: resultOrder.ScheduleReturnDate ? dateFormat(resultOrder.ScheduleReturnDate, 'dddd, mmmm dS, yyyy') : null,
                                        },
                                        Product: newProductsArr.filter(x => x.value),
                                        Attachment: []
                                    }
                                    if (resultOrder.OrderManagementID) {
                                        email(resultOrder.OrderManagementID.Email, 'Order transfered successfully - ' + resultOrder.RequestID, 'transfer-order', emailData);
                                    } else {
                                        user.find({ Active: 1 }).populate({ path: 'RoleID', match: { Description: 'OM Supervisory' } }).then(resultOm => {
                                            resultOm = resultOm.filter(x => x.RoleID);
                                            var OmEmailArr = [];
                                            resultOm.map(item => {
                                                OmEmailArr.push(item.Email);
                                            })
                                            email(OmEmailArr, 'Order transfered successfully - ' + resultOrder.RequestID, 'transfer-order', emailData);
                                        })
                                    }


                                    // assign order to new salesperson
                                    user.findById(transferRequestData.SalesPersonID).then(resultUserForSalesperson => {
                                        emailData.NewSalesPerson = {
                                            Name: resultUserForSalesperson ? resultUserForSalesperson.FirstName : '',
                                            Email: resultUserForSalesperson ? resultUserForSalesperson.Email : common_config.EmailID,
                                            Phone: resultUserForSalesperson ? resultUserForSalesperson.Phone : ''
                                        };
                                        if (resultUserForSalesperson.Email)
                                            email(resultUserForSalesperson.Email, 'Transfered Order assigned successfully - ' + transferRequestData.RequestID, 'transfer-order-assign', emailData);
                                    })


                                    var OrderTimeLine = {
                                        OrderID: OrderID,
                                        Message: 'Order transfered ' + resultOrderOld.SalesPersonID.FirstName + ' ' + resultOrderOld.SalesPersonID.LastName + ' to ' + resultOrder.SalesPersonID.FirstName + ' ' + resultOrder.SalesPersonID.LastName + ' successfully',
                                        ModifiedBy: transferData.currentUserID ? transferData.currentUserID : resultOrder.SalesPersonID,
                                        Status: 8
                                    }
                                    createOrderTimeLine(OrderTimeLine);
                                    createNotificationTimeLine(OrderTimeLine);


                                    var transferHistoryData = {
                                        OrderID: OrderID,
                                        OldOrderDetail: {
                                            SalesPersonID: resultOrderOld.SalesPersonID,
                                            CustomerDetails: resultOrderOld.CustomerDetails,
                                            DeliveryLocation: resultOrderOld.DeliveryLocation
                                        },
                                        NewOrderDetail: {
                                            SalesPersonID: transferData.CustomerID,
                                            CustomerDetails: transferData.CustomerDetails,
                                            DeliveryLocation: transferData.DeliveryLocation
                                        },
                                        ModifiedBy: transferData.currentUserID ? transferData.currentUserID : resultOrderOld.SalesPersonID
                                    }
                                    createTransferHistory(transferHistoryData);
                                    transferRequestModel.findByIdAndUpdate(transferData.transferRequestID, { Status: 2, ModificationDate: new Date() })
                                        .then().catch(err => {
                                            console.log(err)
                                        });
                                    res.status(200).json({
                                        'status': true,
                                        'message': 'Successfully transfered order',
                                        'result': resultTransferData
                                    })
                                })
                            }).catch(err => {
                                console.log(err)
                                next(err);
                            })
                        })
                }
            }).catch(err => {
                console.log(err)
                next(err);
            })
    } catch (e) {
        console.log(e);
    }
}

//getNotificationByUser
exports.getNotificationByUser = function(req, res, next) {
    var UserID = req.params._id;
    var condition = {
        Active: 1,
        ReadBy: { $ne: UserID }
    }
    user.findById(UserID)
        .populate(['RoleID'])
        .then(resultUser => {
            if (resultUser.RoleID.Description == 'Dealer' || resultUser.RoleID.Description == 'Dealer') {
                condition.Status = {
                    $in: [2, 3, 4, 5]
                }
            } else if (resultUser.RoleID.Description == 'OM Supervisory') {
                condition.Status = {
                    $in: [1, 6, 7, 9]
                }
            } else if (resultUser.RoleID.Description == 'OM Supervisor') {
                condition.Status = 2;
            }
            notificationTimeLine.find(condition)
                .populate({ path: 'OrderID' })
                .populate({ path: 'ModifiedBy', select: ['FirstName', 'LastName', 'Phone', 'Email'] })
                .sort({ '_id': -1 })
                .then(resultNotification => {
                    if (resultUser.RoleID.Description == 'Dealer') {
                        resultNotification = resultNotification.filter(x => x.OrderID.SalesPersonID == UserID);
                    } else if (resultUser.RoleID.Description == 'DealerChild') {
                        resultNotification = [];
                    }
                    res.status(200).json({
                        'status': true,
                        'message': 'Success',
                        'result': resultNotification
                    })
                }).catch(err => {
                    next(err);
                })
        }).catch(err => {
            next(err);
        })
}

//readNotificationByUser
exports.readNotificationByUser = function(req, res, next) {
    var NotificationID = req.body.NotificationID;
    var UserID = req.body.UserID;
    notificationTimeLine.findById(NotificationID).exec().then(result => {
        var arr = result.ReadBy ? result.ReadBy : [];
        arr.push(UserID);
        var data = {
            ReadBy: arr
        }
        notificationTimeLine.findByIdAndUpdate(NotificationID, data, { new: true }).then(resultNotification => {
            res.status(200).json({
                'status': true,
                'message': 'Successfully notification read',
                'result': null
            })
        }).catch(err => {
            next(err);
        })
    }).catch(err => {
        next(err);
    })
}

exports.getLastOrderRequestID = function(req, res, next) {

    const condition = {
        'Active': 1
    }
    order.find(condition)
        .select(['RequestID'])
        .sort({ _id: 'desc' })
        .limit(1)
        .then(result => {
            if (result.length > 0) {
                let r = result[0].RequestID;
                var string = r.split("-");

                const requestid = parseInt(string[1]) + 1;
                res.status(200).json({
                    'status': true,
                    'message': 'Success',
                    'result': requestid
                })
            } else {
                const requestid = 400000;
                res.status(200).json({
                    'status': true,
                    'message': 'Success',
                    'result': requestid
                })
            }


        }).catch(err => {
            next(err);
        })

}

// getAllOrdersByRole
exports.getAllOrdersByRole = function(req, res, next) {
    var RoleName = req.body.Role;
    var UserID = req.body.UserID;

    const condition = {
        'Active': 1
    };
    if (RoleName == 'OM Supervisory') {
        condition.Status = {
            $in: [2, 3, 4, 5, 6, 7, 9]
        }
    } else if (RoleName == 'OM Supervisor') {
        condition.Status = {
            $in: [1, 2, 10]
        }
    } else if (RoleName == 'Approver') {
        condition.Status = {
            $in: [1, 2, 3]
        }
    } else if (RoleName == 'Dealer') {
        condition.SalesPersonID = UserID;
    }
    order.find(condition)
        .sort({ ModificationDate: 'desc' })
        // .select(['ServiceType', 'RequestID', 'CustomerDetails', 'DeliveryLocation', 'ScheduleReturnDate', 'Status', 'TotalAmount', 'Attachment', 'ApproverID', 'CreatedDate', 'ReasonMessage', 'IsUpdated', 'ModificationDate'])
        .populate({ path: 'UserID', select: ['FirstName', 'LastName', 'Phone'] })
        .populate({ path: 'Products.ProductID' })
        .populate({ path: 'ReturnProducts.ProductID' })
        .populate({ path: 'SalesPersonID', select: ['FirstName', 'LastName', 'Phone', 'Email', 'Address'] })
        .then(resultOrder => {
            user.findById(UserID).then(resultUser => {
                finalResult = [];
                if (RoleName == 'Approver') {
                    // var minVal = resultUser.MinCostLevel, maxVal = resultUser.MaxCostLevel;
                    resultOrder.map(item => {
                        // if (item.Status == 1 && item.TotalAmount > minVal && item.TotalAmount <= maxVal) {
                        if (item.Status == 1) {
                            finalResult.push(item);
                        } else if (item.ApproverID == UserID) {
                            finalResult.push(item);
                        }
                    })
                } else if (RoleName == 'DealerChild') {
                    finalResult = [];
                } else {
                    finalResult = resultOrder;
                }
                finalResult.map(item => {
                    item.Products = item.Products.filter(x => x.Active == 1)
                })
                res.status(200).json({
                    'status': true,
                    'message': 'Success',
                    'result': finalResult,
                    'allOrder': resultOrder
                })
            }).catch(err => {
                next(err);
            })
        }).catch(err => {
            next(err);
        })
}

// orderReturnAccept
exports.orderReturnAccept = function(req, res, next) {
    try {
        let OrderID = req.params._id;
        let _currentUser = req.params._currentUser;
        order.findById(OrderID).then(resultOrderOld => {
            returnOrder.findOne({ OrderID: OrderID }).sort({ _id: -1 }).exec()
                .then(resultReturn => {
                    ProductArr = [];
                    ReturnProductArr = resultOrderOld.ReturnProducts;
                    resultOrderOld.Products.map(p => {
                        var index = resultReturn.Products.findIndex(x => x.ProductCode == p.ProductCode);
                        if (index != -1) {
                            p.Quantity = parseInt(p.Quantity) - parseInt(resultReturn.Products[index].Quantity);
                            if (p.Quantity > 0)
                                ProductArr.push(p);
                        } else {
                            ProductArr.push(p);
                        }
                    })

                    resultReturn.Products.map(x => {
                        ReturnProductArr.push(x);
                    })

                    var Status = 9;
                    if (ProductArr.length > 0) {
                        Status = 6;
                    }

                    order.findByIdAndUpdate(OrderID, { Status: Status, IsReturned: true, Products: ProductArr, ReturnProducts: ReturnProductArr, ModificationDate: new Date() }, { new: true })
                        .populate({ path: 'Products.ProductID' })
                        .populate({ path: 'SalesPersonID' })
                        .then(resultOrder => {
                            var OrderTimeLine = {
                                OrderID: OrderID,
                                Message: 'Order return request accepted successfully',
                                ModifiedBy: _currentUser,
                                Status: 9
                            }
                            createOrderTimeLine(OrderTimeLine);
                            createNotificationTimeLine(OrderTimeLine);

                            var emailData = {
                                ServerLink: common_config.server_domain,
                                SalesPerson: {
                                    Name: resultOrder.SalesPersonID ? resultOrder.SalesPersonID.FirstName + ' ' + resultOrder.SalesPersonID.LastName : '',
                                    Email: resultOrder.SalesPersonID ? resultOrder.SalesPersonID.Email : common_config.EmailID,
                                    Phone: resultOrder.SalesPersonID ? resultOrder.SalesPersonID.Phone : ''
                                },
                                OrderDetail: {
                                    ServiceType: resultOrder.ServiceType,
                                    RequestID: resultOrder.RequestID,
                                    Status: 9,
                                    PickUpLocation: resultReturn.PickUpLocation,
                                    PickUpDate: dateFormat(resultReturn.PickUpDate, 'dddd, mmmm dS, yyyy')
                                },
                                Detail: 'Order return request accepted successfully',
                                Product: resultOrder.Products,
                                Attachment: []
                            }
                            email(emailData.SalesPerson.Email, 'Order return request accepted successfully - ' + resultOrder.RequestID, 'order-return-request-accepted', emailData);
                            for (let p of resultOrder.Products) {
                                const qty = p.ProductID.AvailableQuantity + p.Quantity;
                                let data = {
                                    'AvailableQuantity': qty
                                };
                                if (p.ProductConsumable == 'No') {
                                    product.findByIdAndUpdate(p.ProductID._id, data, { new: true })
                                        .then(resultProduct => {}).catch(err => {
                                            next(err);
                                        })
                                }
                            }
                            res.status(200).json({
                                'status': true,
                                'message': 'Order return request accepted successfully',
                                'result': resultOrder
                            })
                        }).catch(err => {
                            next(err);
                        })
                }).catch(err => {
                    console.log(err)
                    next(err);
                })
        }).catch(err => {
            next(err);
        })
    } catch (e) {
        console.log(e)
    }
}

// getOrderAcknowledgeDetail
exports.getOrderAcknowledgeDetail = function(req, res, next) {
    let OrderID = req.params._id;
    acknowledge.findOne({ 'Active': 1, 'OrderID': OrderID })
        .sort({ _id: -1 })
        .then(result => {
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': result
            })
        }).catch(err => {
            next(err);
        })
}

// authenticationLogin
exports.authenticationLogin = function(req, res, next) {
    let data = req.body;
    user.findOne({ 'Active': 1, 'UserName': data.Username, 'Password': data.Password })
        .populate({ path: 'RoleID', select: ['Name', 'Description'] })
        .sort({ _id: -1 })
        .then(result => {
            if (result) {
                if (result.RoleID.Description == 'Approver') {
                    res.status(200).json({
                        'status': true,
                        'message': 'Success',
                        'result': result
                    })
                } else {
                    res.status(200).json({
                        'status': false,
                        'message': 'Invalid user role',
                        'result': null
                    })
                }
            } else {
                res.status(200).json({
                    'status': false,
                    'message': 'Invalid username and password',
                    'result': null
                })
            }
        }).catch(err => {
            console.log(err)
            next(err);
        })
}

// updateProductDetail
exports.updateProductDetail = function(req, res, next) {
    let data = req.body.data;
    let currentUser = req.body.currentUser;
    let OrderID = req.params._id;
    var OrderTimeLine = {
        OrderID: OrderID,
        Message: 'Products detail updated successfully',
        ModifiedBy: currentUser,
        Status: 2
    }
    createOrderTimeLine(OrderTimeLine);
    createNotificationTimeLine(OrderTimeLine);

    var TotalAmount = 0;
    data.map(x => {
        TotalAmount += parseInt(x.Quantity) * parseFloat(x.Price);
        if (x.Quantity == 0) {
            x.Active = 0;
        }
    })
    order.findByIdAndUpdate(OrderID, { TotalAmount: TotalAmount, Products: data, ModificationDate: new Date(), IsUpdated: true }, { new: true })
        .then(result => {
            res.status(200).json({
                'status': true,
                'message': 'Successfully Product updated',
                'result': result
            })

        }).catch(err => {
            console.log(err)
            next(err);
        })
}

// getOrderReturnData
exports.getOrderReturnData = function(req, res, next) {
    let OrderID = req.params._id;
    returnOrder.findOne({ OrderID: OrderID })
        .sort({ _id: -1 })
        .populate({ path: 'Products.ProductID' })
        .then(result => {
            res.status(200).json({
                'status': true,
                'message': 'Successfully Product updated',
                'result': result
            })

        }).catch(err => {
            next(err);
        })
}

// updateScheduledReturnDate
exports.updateScheduledReturnDate = function(req, res, next) {
    let OrderID = req.params._id;
    let data = req.body;
    order.findById(OrderID)
        .then(resultOrder => {
            order.findByIdAndUpdate(OrderID, { ScheduleReturnDate: new Date(data.ScheduledReturnDate), ModificationDate: new Date() }, { new: true }).then(result => {
                var OrderTimeLine = {
                    OrderID: OrderID,
                    Message: 'Scheduled return date updated successfully',
                    ModifiedBy: data.CurrentUser,
                    Status: resultOrder.Status
                }
                createOrderTimeLine(OrderTimeLine);
                createNotificationTimeLine(OrderTimeLine);
                res.status(200).json({
                    'status': true,
                    'message': 'Scheduled return date updated successfully',
                    'result': result
                })
            }).catch(err => {
                console.log(err)
                next(err);
            })
        }).catch(err => {
            next(err);
        })
}


// acknowledgeReminder
exports.acknowledgeReminder = function() {
    order.find({ Active: 1, Status: 5 })
        .populate({ path: 'Products.ProductID' })
        .populate({ path: 'SalesPersonID' })
        .then(resultOrder => {
            resultOrder.map(item => {
                var emailData = {
                    ServerLink: common_config.server_domain,
                    SalesPerson: {
                        Name: item.SalesPersonID ? item.SalesPersonID.FirstName + ' ' + item.SalesPersonID.LastName : '',
                        Email: item.SalesPersonID ? item.SalesPersonID.Email : common_config.EmailID,
                        Phone: item.SalesPersonID ? item.SalesPersonID.Phone : ''
                    },
                    OrderDetail: {
                        ServiceType: item.ServiceType,
                        RequestID: item.RequestID,
                        Status: 5,
                        DeliveryLocation: item.DeliveryLocation,
                        TotalAmount: item.TotalAmount,
                        ScheduleReturnDate: item.ScheduleReturnDate ? dateFormat(item.ScheduleReturnDate, 'dddd, mmmm dS, yyyy') : null,
                    },
                    Detail: 'Order Delivered successfully',
                    Product: item.Products,
                    Attachment: []
                }
                email(emailData.SalesPerson.Email, 'Acknowledge your order - ' + item.RequestID, 'order-acknowledge-reminder', emailData);
                if (resultOrder.CustomerDetails && resultOrder.CustomerDetails.Email) {
                    email(item.CustomerDetails.Email, 'Acknowledge your order - ' + resultOrder.RequestID, 'order-acknowledge-reminder', emailData);
                }
                console.log('order acknowledge reminder send successfully')
            })
        }).catch(err => {
            console.log(err);
        })
}

// readAllNotification
exports.readAllNotification = function(req, res, next) {
    const notificationArr = req.body.data;
    const UserID = req.params.UserID;
    Promise.all(notificationArr.map((row) => {
        return new Promise(async(resolvenew, rejectnew) => {
            notificationTimeLine.findById(row._id).exec().then(result => {
                var arr = result.ReadBy ? result.ReadBy : [];
                arr.push(UserID);
                var data = {
                    ReadBy: arr
                }
                notificationTimeLine.findByIdAndUpdate(row._id, data, { new: true }).then(resultNotification => {
                    resolvenew();
                }).catch(err => {
                    rejectnew();
                })
            }).catch(err => {
                rejectnew();
            })
        })
    }))
    res.status(200).json({
        'status': true,
        'message': 'Successfully notification read',
        'result': null
    })
}