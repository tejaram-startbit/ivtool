const user = require('../../models/user.model');

// create
exports.create = function(req, res, next) {
    let data = JSON.parse(req.body.data);
    data.ModificationDate = new Date();
    data.Active = 1;
    let UID = 0;

    const users = new user(data);
    if (req.file) {
        let path = req.file.path;
        path = path.replace('public\\', '');
        path = path.replace('public/', '');
        users.Avatar = path;
    }
    user.findOne().sort({ _id: 'desc' }).then(result => {
        if (result) {
            UID = Number(result['UID_Temp']) + 1;
        } else {
            UID = 1;
        }
        users['UID_Temp'] = UID;
        users['UID'] = 'IVTOOL' + UID;

        users.save().then(result => {
            res.status(200).json({
                'status': true,
                'message': 'Successfully created user',
                'result': users
            })
        }).catch(err => {
            console.log(err)
            next(err);
        })
    }).catch(err => {
        next(err);
    });
}

// update
exports.update = function(req, res, next) {
    let data = JSON.parse(req.body.data);
    let id = req.params.id;
    data.ModificationDate = new Date();
    data.Active = 1;
    if (req.file) {
        let path = req.file.path;
        path = path.replace('public\\', '');
        path = path.replace('public/', '');
        data.Avatar = path;
    } else {
        delete data.Avatar;
    }
    user.findByIdAndUpdate(id, data, { new: true }).then(result => {
        res.status(200).json({
            'status': true,
            'message': 'Successfully updated record',
            'result': result
        })
    }).catch(err => {
        console.log(err);
        next(err);
    })
}

// getOne
exports.getOne = function(req, res, next) {
    let id = req.params._id;
    user.findById(id)
        .select(['UserName', 'FirstName', 'LastName', 'Email', 'Phone', 'Gender', 'DOB', 'Avatar', 'Address', 'RoleID', 'MisBUGroup', 'MisFranchise'])
        .populate({ path: 'RoleID', select: ['Name', 'Description'] })
        .then(result => {
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': result
            })
        }).catch(err => {
            next(err);
        })
}

// getAll
exports.getAll = function(req, res, next) {
    const condition = {
        'Active': 1
    };
    user.find(condition)
        .sort({ _id: 'desc' })
        .select(['UserName', 'FirstName', 'LastName', 'Email', 'Phone', 'Gender', 'DOB', 'Avatar', 'Address', 'RoleID', 'MinCostLevel', 'MaxCostLevel'])
        .populate({ path: 'RoleID', select: ['Name', 'Description'] })
        .then(result => {
            var finalResult = [];
            finalResult = result.filter(x => x.RoleID.Description != 'Customer');
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': finalResult
            })
        }).catch(err => {
            next(err);
        })
}

// getAllUsersByType
exports.getAllUsersByType = function(req, res, next) {
    const condition = {
        'Active': 1
    };
    user.find(condition)
        .sort({ _id: 'desc' })
        .select(['UserName', 'FirstName', 'LastName', 'Email', 'Phone', 'Gender', 'DOB', 'Address', 'RoleID'])
        .populate({ path: 'RoleID', select: ['Name', 'Description'] })
        .then(result => {
            console.log(result)
            var finalResult = [];
            if (req.params.ServiceType == 'Demo') {
                finalResult = result.filter(x => x.RoleID.Description == 'Customer');
            } else {
                finalResult = result.filter(x => x.RoleID.Description == 'Dealers');
            }
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': finalResult
            })
        }).catch(err => {
            next(err);
        })
}

// delete
exports.delete = function(req, res, next) {
    let id = req.params._id;
    user.findByIdAndUpdate(id, { Active: 0 }).then(result => {
        res.status(200).json({
            'status': true,
            'message': 'Successfully deleted record',
            'result': result
        })
    }).catch(err => {
        next(err);
    })
}


// getAllCustomer
exports.getAllCustomer = function(req, res, next) {
    const condition = {
        'Active': 1
    };
    user.find(condition)
        .sort({ _id: 'desc' })
        .select(['UserName', 'FirstName', 'LastName', 'Email', 'Phone', 'Gender', 'DOB', 'Avatar', 'Address', 'RoleID', 'MinCostLevel', 'MaxCostLevel'])
        .populate({ path: 'RoleID', select: ['Name', 'Description'], match: { Description: 'Customer' } })
        .then(result => {
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': result.filter(x => x.RoleID)
            })
        }).catch(err => {
            next(err);
        })
}

// getAllSalesperson
exports.getAllSalesperson = function(req, res, next) {
    const condition = {
        'Active': 1
    };
    user.find(condition)
        .sort({ _id: 'desc' })
        .select(['UserName', 'FirstName', 'LastName', 'Email', 'Phone', 'Gender', 'DOB', 'Avatar', 'Address', 'RoleID', 'MinCostLevel', 'MaxCostLevel'])
        .populate({ path: 'RoleID', select: ['Name', 'Description'], match: { Description: 'Dealer' } })
        .then(result => {
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': result.filter(x => x.RoleID)
            })
        }).catch(err => {
            next(err);
        })
}

// getAllCustomerSalesPersonAddress
exports.getAllCustomerSalesPersonAddress = function(req, res, next) {
    const condition = {
        'Active': 1
    };
    user.find(condition)
        .sort({ _id: 'desc' })
        .select(['Address'])
        .populate({ path: 'RoleID', select: ['Name', 'Description'], match: { Description: { '$in': ['Customer', 'Dealer'] } } })
        .then(result => {
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': result.filter(x => x.RoleID)
            })
        }).catch(err => {
            next(err);
        })
}


// getAllApprover
exports.getAllApprover = function(req, res, next) {
    const condition = {
        'Active': 1
    };
    user.find(condition)
        .sort({ _id: 'desc' }) //cust name address phone ContactPerson 
        .select(['MisFranchise', 'MisBUGroup'])
        .populate({ path: 'RoleID', select: ['Name', 'Description'], match: { Description: 'Approver' } })
        .then(result => {
            res.status(200).json({
                'status': true,
                'message': 'Success',
                'result': result.filter(x => x.RoleID)
            })
        }).catch(err => {
            next(err);
        })
}