const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;

const HospitalSchema = new Schema({
    Name: {type: String, required: true},
    Avatar: {type: String},
    OwnerName: {type: String},
    Address: {type: String},
    City: {type: String},
    State: {type: String},
    Country: {type: String},
    PIN: {type: Number},
    ModificationDate: {type: Date, required: true, default: new Date()},
    ModifiedBy: {type: mongoose.Schema.ObjectId, ref: 'Users'},
    Active: {type: Number, required: true, default: 1},
});

// Export the model
module.exports = mongoose.model('Hospitals', HospitalSchema);