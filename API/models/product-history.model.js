const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;

const ProductHistorySchema = new Schema({
    ProductID: {type: String, required: true},
    Quantity: {type: Number, default: 0},
    Price: {type: Number, default: 0},
    ModificationDate: {type: Date, required: true, default: new Date()},
    ModifiedBy: {type: mongoose.Schema.ObjectId, ref: 'Users'},
    Active: {type: Number, required: true, default: 1},
});

// Export the model
module.exports = mongoose.model('Product_Histories', ProductHistorySchema);