const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;

const OrderSchema = new Schema({
    ServiceType: { type: String },
    RequestID: { type: String },
    RequestType: { type: String },
    KitStatus: { type: String },
    OrderType: { type: String },
    Products: [{
        ProductCode: { type: String },
        KitName: { type: String },
        ProductID: { type: mongoose.Schema.ObjectId, ref: 'Products' },
        Quantity: { type: Number, default: 0 },
        ProductConsumable: { type: String },
        Price: { type: Number },
        Inventory: { type: String },
        BranchCode: { type: String },
        Active: { type: Number, default: 1 }
    }],
    ReturnProducts: [{
        ProductCode: { type: String },
        KitName: { type: String },
        ProductID: { type: mongoose.Schema.ObjectId, ref: 'Products' },
        Quantity: { type: Number, default: 0 },
        ProductConsumable: { type: String },
        Price: { type: Number },
        Inventory: { type: String },
        BranchCode: { type: String },
        Status: { type: Number, default: 1 },
        Active: { type: Number, default: 1 }
    }],
    IsReturned: { type: Boolean, default: false },
    Consumable: { type: String },
    UserID: { type: mongoose.Schema.ObjectId, ref: 'Users' },
    CustomerDetails: { type: Object },
    DeliveryLocation: { type: String },
    ScheduleReturnDate: { type: Date },
    Status: { type: Number },
    TotalAmount: { type: mongoose.Schema.Types.Double },
    Attachment: { type: String },
    ApproverID: { type: mongoose.Schema.ObjectId, ref: 'Users' },
    SalesPersonID: { type: mongoose.Schema.ObjectId, ref: 'Users' },
    OldSalesPersonID: { type: mongoose.Schema.ObjectId, ref: 'Users' },
    OrderManagementID: { type: mongoose.Schema.ObjectId, ref: 'Users' },
    OldOrderID: { type: mongoose.Schema.ObjectId, ref: 'Orders' },
    CreatedDate: { type: Date, required: true, default: new Date() },
    ReasonMessage: { type: String },
    DeclineReasonMessage: { type: String },
    IsUpdated: { type: Number, default: 0 },
    ModificationDate: { type: Date, required: true, default: new Date() },
    ModifiedBy: { type: mongoose.Schema.ObjectId, ref: 'Users' },
    Active: { type: Number, required: true, default: 1 },
});

// Export the model
module.exports = mongoose.model('Orders', OrderSchema);