const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;

const UserSchema = new Schema({
    UserName: {type: String},
    Password: {type: String},
    FirstName: {type: String},
    LastName: {type: String},
    Email: {type: String},
    Phone: {type: Number},
    Gender: {type: String},
    DOB: {type: Date},
    Avatar: {type: String},
    Address: {type: String},
    UID_Temp: {type: Number, default: 1},
    UID: {type: String},
    ResetToken: {type: String},
    MisFranchise: {type: String},
    MisBUGroup: {type: String},
    ResetExpires: {type: Date},
    RoleID: {type: mongoose.Schema.ObjectId, ref: 'roles'},
    MinCostLevel: {type: Number, default: 0},
    MaxCostLevel: {type: Number, default: 1},
    ModificationDate: {type: Date, default: new Date()},
    ModifiedBy: {type: mongoose.Schema.ObjectId, ref: 'Users'},
    Active: {type: Number, required: true, default: 1}
});

// Export the model
module.exports = mongoose.model('Users', UserSchema);