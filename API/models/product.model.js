const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;

const ProductSchema = new Schema({
    Name: { type: String, required: true },
    ProductCode: { type: String },
    Type: { type: String },
    Image: { type: String },
    Description: { type: String },
    Price: { type: mongoose.Schema.Types.Double },
    TotalQuantity: { type: Number, default: 0 },
    AvailableQuantity: { type: Number, default: 0 },
    ProductConsumable: { type: String },
    Inventory: { type: String },
    BranchCode: { type: String },
    ProductType: { type: String, default: 'Product' },
    MisFranchise: { type: String },
    MisBUGroup: { type: String },
    Location: { type: String },
    ModificationDate: { type: Date, required: true, default: new Date() },
    ModifiedBy: { type: mongoose.Schema.ObjectId, ref: 'Users' },
    Active: { type: Number, required: true, default: 1 },
});

// Export the model
module.exports = mongoose.model('Products', ProductSchema);