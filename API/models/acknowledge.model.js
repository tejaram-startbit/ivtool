const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;

const AcknowledgeSchema = new Schema({
    Remarks: {type: String, required: true},
    Picture: {type: String},
    OrderID: {type: mongoose.Schema.ObjectId, ref: 'Orders'},
    Active: {type: Number, required: true, default: 1}
});

// Export the model
module.exports = mongoose.model('Acknowledges', AcknowledgeSchema);