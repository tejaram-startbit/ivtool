const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;

const TransferHistorySchema = new Schema({
    OrderID: {type: mongoose.Schema.ObjectId, ref: 'Orders'},
    OldOrderDetail: {
        SalesPersonID: {type: mongoose.Schema.ObjectId, ref: 'Users'},
        CustomerDetails: {type: Object},
        DeliveryLocation: {type: String}
    },
    NewOrderDetail: {
        SalesPersonID: {type: mongoose.Schema.ObjectId, ref: 'Users'},
        CustomerDetails: {type: Object},
        DeliveryLocation: {type: String}
    },
    ModificationDate: {type: Date, required: true, default: new Date()},
    ModifiedBy: {type: mongoose.Schema.ObjectId, ref: 'Users'},
    Active: {type: Number, required: true, default: 1},
});

// Export the model
module.exports = mongoose.model('Transfer_Histories', TransferHistorySchema);