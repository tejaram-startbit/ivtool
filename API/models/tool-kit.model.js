const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;

const InventorySchema = new Schema({
    KitCode: { type: String },
    Name: { type: String, required: true },
    Image: { type: String },
    Description: { type: String },
    Inventory: [{
        Inventory_ID: { type: mongoose.Schema.ObjectId, ref: 'Products' },
        Name: { type: String },
        Quantity: { type: Number, default: 0 }
    }],
    ModificationDate: { type: Date, required: true, default: new Date() },
    ModifiedBy: { type: mongoose.Schema.ObjectId, ref: 'Users' },
    Active: { type: Number, required: true, default: 1 },
});

// Export the model
module.exports = mongoose.model('Toolkits', InventorySchema);