const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;

const ReturnOrderSchema = new Schema({
    PickUpDate: { type: Date },
    PickUpLocation: { type: String },
    OrderID: { type: mongoose.Schema.ObjectId, ref: 'Orders' },
    Products: [{
        ProductCode: { type: String },
        ProductID: { type: mongoose.Schema.ObjectId, ref: 'Products' },
        Quantity: { type: Number, default: 0 },
        ProductConsumable: { type: String },
        Price: { type: Number },
        Inventory: { type: String },
        BranchCode: { type: String },
        Status: { type: Number, default: 1 },
        Active: { type: Number, default: 1 }
    }],
    ModificationDate: { type: Date, required: true, default: new Date() },
    ModifiedBy: { type: mongoose.Schema.ObjectId, ref: 'Users' },
    Active: { type: Number, required: true, default: 1 },
});

// Export the model
module.exports = mongoose.model('ReturnOrder', ReturnOrderSchema);