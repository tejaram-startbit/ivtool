const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;

const TransferRequestOrderSchema = new Schema({
    OldSalesPersonID: {type: mongoose.Schema.ObjectId, ref: 'Users'},
    NewSalesPersonID: {type: mongoose.Schema.ObjectId, ref: 'Users'},
    OrderID: {type: mongoose.Schema.ObjectId, ref: 'Orders'},
    TransferData: {type: Object},
    Status: {type: Number},
    ModificationDate: {type: Date, required: true, default: new Date()},
    ModifiedBy: {type: mongoose.Schema.ObjectId, ref: 'Users'},
    Active: {type: Number, required: true, default: 1},
});

// Export the model
module.exports = mongoose.model('TransferRequestOrder', TransferRequestOrderSchema);