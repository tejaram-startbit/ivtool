const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;

const OrderTimeLineSchema = new Schema({
    OrderID: {type: mongoose.Schema.ObjectId, ref: 'Orders'},
    Status: {type: Number},
    Message: {type: String},
    Description: {type: String},
    Attachment: {type: String},
    ModificationDate: {type: Date, required: true, default: new Date()},
    ModifiedBy: {type: mongoose.Schema.ObjectId, ref: 'Users'},
    Active: {type: Number, required: true, default: 1},
});

// Export the model
module.exports = mongoose.model('OrderTimeLines', OrderTimeLineSchema);