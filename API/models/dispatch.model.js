const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;

const DispatchedSchema = new Schema({
    HAWB_No: {type: String},
    HAWB_Date: {type: Date},
    ETA: {type: Date},
    JDE_Invoice_No: {type: String},
    Invoice_Date: {type: Date},
    Documents: {type: Array},
    ProductDetailAttachment: {type: String},
    ProductInstructionPdf: {type: String},
    OrderID: {type: mongoose.Schema.ObjectId, ref: 'Orders'},
    ModificationDate: {type: Date, required: true, default: new Date()},
    ModifiedBy: {type: mongoose.Schema.ObjectId, ref: 'Users'},
    Active: {type: Number, required: true, default: 1},
});

// Export the model
module.exports = mongoose.model('Dispatched', DispatchedSchema);