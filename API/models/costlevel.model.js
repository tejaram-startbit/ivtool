const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CostLevelSchema = new Schema({
    MinRange: {type: Number},
    MaxRange: {type: Number},
    Description: {type: String},
    ModificationDate: {type: Date, required: true, default: new Date()},
    ModifiedBy: {type: mongoose.Schema.ObjectId, ref: 'Users'},
    Active: {type: Number, required: true, default: 1},
});

// Export the model
module.exports = mongoose.model('costlevels', CostLevelSchema);