//Import the mongoose module
var mongoose = require('mongoose');
var config = require('./common.config');

//Set up default mongoose connection
mongoose.connect(config.ConnectionString, {'useNewUrlParser': true, 'useFindAndModify': false, 'useCreateIndex': true, useUnifiedTopology: true}, (err) => {
    if(err)
    console.log(err);
else
console.log('Successfull');
});
// Get Mongoose to use the global promise library
mongoose.Promise = global.Promise;
//Get the default connection
var db = mongoose.connection;

//Bind connection to error event (to get notification of connection errors)
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

module.exports = db;