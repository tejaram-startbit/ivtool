var express = require('express');
var router = express.Router();
const User = require('../models/user.model');
var token = '';
const btoa = require('btoa');
var nodemailer = require('nodemailer');
const roles = require('../models/role.model');

require('crypto').randomBytes(48, function(err, buffer) {
    token = buffer.toString('hex');
});

router.post('/login', function(req, res, next) {
    let UserName = req.body.username;
    let Password = req.body.password;
    User.findOne({ 'UserName': UserName, 'Password': Password, 'Active': 1 })
        .populate({ path: 'RoleID' })
        .exec(function(err, data) {
            if (err || data == null) {
                res.send(JSON.stringify({
                    'status': false,
                    'message': 'Invalid UserName and Password',
                    'result': data
                }))
            } else {
                res.send(JSON.stringify({
                    'status': true,
                    'message': 'Succesfully Login',
                    'token': token,
                    'id': data._id,
                    'username': data.UserName,
                    'RoleID': data.RoleID,
                    'Email': data.Email,
                    'Address': data.Address
                }))
            }
        })
})

router.post('/applogin', function(req, res, next) {
    let UserName = req.body.username;
    let Password = req.body.password;

    //roles.findOne({'Active': 1, 'Description': 'Dealer'})
    //.then(resultrole => {

    User.findOne({ 'UserName': UserName, 'Password': Password, 'Active': 1 })
        .populate({ path: 'RoleID' })
        .exec(function(err, data) {
            if (err || data == null) {
                res.send(JSON.stringify({
                    'status': false,
                    'message': 'Invalid Username and Password',
                    'result': data
                }))
            } else {
                res.send(JSON.stringify({
                    'status': true,
                    'message': 'Succesfully Login',
                    'token': token,
                    'id': data._id,
                    'username': data.UserName,
                    'FirstName': data.FirstName,
                    'LastName': data.LastName,
                    'RoleID': data.RoleID,
                    'Email': data.Email,
                    'Address': data.Address
                }))
            }
        })
        // }).catch(err => {
        //     next(err);
        // });
})

router.post('/forgot', function(req, res, next) {
    let useremail = req.body.useremail;
    let adminappUrl = req.body.adminappUrl;
    User.findOne({ 'Email': useremail })
        .exec(function(err, data) {
            console.log(JSON.stringify(data));
            if (err || data == null) {
                res.send(JSON.stringify({
                    'status': false,
                    'message': 'Mail ID not exists in Database.',
                    'result': data
                }))
            } else {

                data.ResetToken = token;
                data.ResetExpires = Date.now() + 3600000; // 1 hour
                User.findByIdAndUpdate(data._id, data, { new: true }, (err, data) => {
                    if (err) {
                        return next(err);
                    } else {
                        var transporter = nodemailer.createTransport({
                            host: 'smtp.gmail.com',
                            port: 587,
                            secure: true,
                            service: 'Gmail',
                            auth: {
                                user: 'supportntest@gmail.com',
                                pass: 'vivatest@321'
                            }
                        });

                        var mailOptions = {
                            to: useremail,
                            from: 'passwordreset@demo.com',
                            subject: 'Password Reset',
                            text: 'You are receiving this because you (or someone else) have requested the reset of the password for your account.\n\n' +
                                'Please click on the following link, or paste this into your browser to complete the process:\n\n' +
                                adminappUrl + '/#/auth/reset/' + token + '\n\n' +
                                'If you did not request this, please ignore this email and your password will remain unchanged.\n'
                        };
                        transporter.sendMail(mailOptions, function(err) {
                            res.send(JSON.stringify({
                                'status': true,
                                'message': 'Mail Sent Successfully'
                            }))
                        });
                        res.status(200).json({
                            'status': true,
                            'message': 'Successfully updated record',
                            'result': data
                        })
                    }
                })

            }
        })

});


router.post('/reset', function(req, res, next) {
    let password = req.body.UserPassword;
    let token = req.body.token;

    User.findOne({ 'ResetToken': token })
        .exec(function(err, data) {
            console.log(data);
            if (err || data == null) {
                res.send(JSON.stringify({
                    'status': false,
                    'message': 'Token Expired',
                    'result': data
                }))
            } else {
                data.Password = password;
                var currenttime = Date.now();
                if (data.ResetExpires > currenttime) {
                    data.ResetExpires = '';
                    data.ResetToken = '';

                    User.findByIdAndUpdate(data._id, data, { new: true }, (err, data) => {
                        if (err) {
                            return next(err);
                        } else {

                            res.status(200).json({
                                'status': true,
                                'message': 'Successfully Updated Password',
                                'result': data
                            })
                        }
                    })
                } else {
                    res.send(JSON.stringify({
                        'status': false,
                        'message': 'Token Expired',
                        'result': data
                    }));
                }

            }
        })

});


module.exports = router;