const multer = require('multer');
const path = require('path');


//file upload in User for Avatar
var storageAvatarImage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, './public/Files/Users/')
    },

    filename: (req, file, cb) => {
        cb(null, file.fieldname + '-image-' + Date.now() + path.extname(file.originalname))
    }
});
exports.uploadAvatarImage = multer({ storage: storageAvatarImage });


//file upload in Product for image
var storageProductImage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, './public/Files/Products/')
    },

    filename: (req, file, cb) => {
        cb(null, file.fieldname + 'product-image-' + Date.now() + path.extname(file.originalname))
    }
});
exports.uploadProductImage = multer({ storage: storageProductImage });



//file upload in Product for image
var storageInventoryImage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, './public/Files/Inventorys/')
    },

    filename: (req, file, cb) => {
        cb(null, file.fieldname + 'inventory-image-' + Date.now() + path.extname(file.originalname))
    }
});
exports.uploadInventoryImage = multer({ storage: storageInventoryImage });

//file upload in Hospital for image
var storageHospitalImage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, './public/Files/Hospitals/')
    },

    filename: (req, file, cb) => {
        cb(null, file.fieldname + 'hospital-image-' + Date.now() + path.extname(file.originalname))
    }
});
exports.uploadHospitalImage = multer({ storage: storageHospitalImage });


//file upload excel
var storageUploadExcel = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, './public/Files/Products/')
    },

    filename: (req, file, cb) => {
        cb(null, file.fieldname + 'excel-file' + path.extname(file.originalname))
    }
});
exports.uploadExcel = multer({ storage: storageUploadExcel });

//file upload excel
var storageUploadOrderFile = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, './public/Files/Orders/')
    },

    filename: (req, file, cb) => {
        cb(null, file.fieldname + '-dispatched-attachment-' + Date.now() + path.extname(file.originalname))
    }
});
exports.uploadOrderFile = multer({ storage: storageUploadOrderFile });

//file upload excel
var storageUploadOrderImage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, './public/Files/Orders/')
    },

    filename: (req, file, cb) => {
        cb(null, file.fieldname + '-order-' + Date.now() + path.extname(file.originalname))
    }
});
exports.uploadOrderImage = multer({ storage: storageUploadOrderImage });

//upload Acknowledgement Picture
var storageUploadAcknowledgeImage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, './public/Files/Acknowledges/')
    },

    filename: (req, file, cb) => {
        cb(null, file.fieldname + '-acknowledge-' + Date.now() + path.extname(file.originalname))
    }
});
exports.uploadAcknowledgeImage = multer({ storage: storageUploadAcknowledgeImage });