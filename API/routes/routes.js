var express = require('express');
var router = express.Router();
const fileUploadConfig = require('../routes/fileUpload.config');

// Controller
var userCtrl = require('../controller/User/user.controller');
var roleCtrl = require('../controller/Role/role.controller');
var costlevelCtrl = require('../controller/CostLevel/costlevel.controller');
var productCtrl = require('../controller/Product/product.controller');
var inventoryCtrl = require('../controller/Inventory/inventory.controller');
var toolkitCtrl = require('../controller/Tool-kit/tool-kit.controller');
var hospitalCtrl = require('../controller/Hospital/hospital.controller');
var orderCtrl = require('../controller/Order/order.controller');
var reportCtrl = require('../controller/Report/report.controller');


// User Controller Routing
router.post('/user/create', fileUploadConfig.uploadAvatarImage.single('file'), userCtrl.create);
router.post('/user/update/:id', fileUploadConfig.uploadAvatarImage.single('file'), userCtrl.update);
router.get('/user/getOne/:_id', userCtrl.getOne);
router.get('/user/getAll', userCtrl.getAll);
router.delete('/user/delete/:_id', userCtrl.delete);
router.get('/user/getAllUsersByType/:ServiceType', userCtrl.getAllUsersByType);
router.get('/user/getAllCustomer', userCtrl.getAllCustomer);
router.get('/user/getAllApprover', userCtrl.getAllApprover);
router.get('/user/getAllSalesperson', userCtrl.getAllSalesperson);
router.get('/user/getAllCustomerSalesPersonAddress', userCtrl.getAllCustomerSalesPersonAddress);

// Role Controller Routing
router.post("/role/create", roleCtrl.create);
router.post('/role/update/:_id', roleCtrl.update);
router.get('/role/getOne/:_id', roleCtrl.getOne);
router.get('/role/getAll/', roleCtrl.getAll);
router.delete('/role/delete/:_id', roleCtrl.delete);

// CostLevel Controller Routing
router.post("/costlevel/create", costlevelCtrl.create);
router.post('/costlevel/update/:_id', costlevelCtrl.update);
router.get('/costlevel/getOne/:_id', costlevelCtrl.getOne);
router.get('/costlevel/getAll/', costlevelCtrl.getAll);
router.delete('/costlevel/delete/:_id', costlevelCtrl.delete);

// Product Controller Routing
router.post('/product/create', fileUploadConfig.uploadProductImage.single('file'), productCtrl.create);
router.post('/product/update/:id', fileUploadConfig.uploadProductImage.single('file'), productCtrl.update);
router.get('/product/getOne/:_id', productCtrl.getOne);
router.get('/product/getProductLength', productCtrl.getProductLength);
router.get('/product/getAll', productCtrl.getAll);
router.delete('/product/delete/:_id', productCtrl.delete);
router.post('/product/uploadExcel', fileUploadConfig.uploadExcel.single('file'), productCtrl.uploadExcel);
router.get('/product/getAllProductsByType/:ServiceType', productCtrl.getAllProductsByType);

// Inventory Controller Routing
router.post('/inventory/create', fileUploadConfig.uploadInventoryImage.single('file'), inventoryCtrl.create);
router.post('/inventory/update/:id', fileUploadConfig.uploadInventoryImage.single('file'), inventoryCtrl.update);
router.get('/inventory/getOne/:_id', inventoryCtrl.getOne);
router.get('/inventory/getAll', inventoryCtrl.getAll);
router.delete('/inventory/delete/:_id', inventoryCtrl.delete);
router.post('/inventory/uploadExcel', fileUploadConfig.uploadExcel.single('file'), inventoryCtrl.uploadExcel);
router.get('/inventory/getAllInventorysByType/:ServiceType', inventoryCtrl.getAllInventorysByType);

// Tool-kit Controller Routing
router.post('/toolkit/create', fileUploadConfig.uploadExcel.single('file'), toolkitCtrl.create);
router.post('/toolkit/update/:id', fileUploadConfig.uploadExcel.single('file'), toolkitCtrl.update);
router.get('/toolkit/getOne/:_id', toolkitCtrl.getOne);
router.get('/toolkit/getAll', toolkitCtrl.getAll);
router.delete('/toolkit/delete/:_id', toolkitCtrl.delete);


// Hospital Controller Routing
router.post('/hospital/create', fileUploadConfig.uploadHospitalImage.single('file'), hospitalCtrl.create);
router.post('/hospital/update/:id', fileUploadConfig.uploadHospitalImage.single('file'), hospitalCtrl.update);
router.get('/hospital/getOne/:_id', hospitalCtrl.getOne);
router.get('/hospital/getAll', hospitalCtrl.getAll);
router.delete('/hospital/delete/:_id', hospitalCtrl.delete);

// Order Controller Routing
router.post('/order/create', fileUploadConfig.uploadOrderImage.single('file'), orderCtrl.create);
router.post('/order/update/:_id', fileUploadConfig.uploadOrderImage.single('file'), orderCtrl.update);
router.get('/order/getOne/:_id', orderCtrl.getOne);
router.get('/order/getOneOrderByRequestID/:_id/:_salesid', orderCtrl.getOneOrderByRequestID);
router.post('/order/acknoledgeCreate', fileUploadConfig.uploadAcknowledgeImage.single('file'), orderCtrl.acknoledgeCreate);
router.get('/order/getAll', orderCtrl.getAll);
router.get('/order/sales/getAll/:salesid', orderCtrl.getSalesAll);
router.get('/order/getAllByLastOneYear', orderCtrl.getAllByLastOneYear);
router.get('/order/getAllLastMonth', orderCtrl.getAllLastMonth);
router.delete('/order/delete/:_id', orderCtrl.delete);
router.post('/order/dispatchedOrder', fileUploadConfig.uploadOrderFile.fields([{ name: 'excelFile' }, { name: 'pdfFile' }]), orderCtrl.dispatchedOrder);
router.get('/order/orderDelivered/:_currentUser/:_id', orderCtrl.orderDelivered);
router.post('/order/approveOrder/:_id', orderCtrl.approveOrder);
router.post('/order/disapproveOrder/:_id', orderCtrl.disapproveOrder);
router.post('/order/declineOrder/:_id', orderCtrl.declineOrder);
router.get('/order/getDispatcheOrderDetail/:_id', orderCtrl.getDispatcheOrderDetail);
router.get('/order/getOrderTimeLine/:_id', orderCtrl.getOrderTimeLine);
router.post('/order/returnOrder/:_id', orderCtrl.returnOrder);
router.post('/order/transferOrder/:_id', orderCtrl.transferOrder);
router.get('/order/getNotificationByUser/:_id', orderCtrl.getNotificationByUser);
router.post('/order/readNotificationByUser', orderCtrl.readNotificationByUser);
router.post('/order/getAllOrdersByRole/', orderCtrl.getAllOrdersByRole);
router.get('/order/getLastOrderRequestID', orderCtrl.getLastOrderRequestID);
router.get('/order/getAllCustomerNSP', orderCtrl.getAllCustomerNSP);
router.get('/order/getAllSP/:_salesid', orderCtrl.getAllSP);
router.get('/order/getOrderAcknowledgeDetail/:_id', orderCtrl.getOrderAcknowledgeDetail);
router.post('/order/authenticationLogin', orderCtrl.authenticationLogin);
router.post('/order/updateProductDetail/:_id', orderCtrl.updateProductDetail);
router.post('/order/updateFinalProductDetail', fileUploadConfig.uploadOrderFile.array('file'), orderCtrl.updateFinalProductDetail);
router.get('/order/orderReturnAccept/:_currentUser/:_id', orderCtrl.orderReturnAccept);
router.get('/order/getOrderReturnData/:_id', orderCtrl.getOrderReturnData);
router.post('/order/updateScheduledReturnDate/:_id', orderCtrl.updateScheduledReturnDate);
router.post('/order/transferRequestOrder/:_id', orderCtrl.transferRequestOrder);
router.get('/order/getTransferRequest/:_id', orderCtrl.getTransferRequest);
router.get('/order/acknowledgeReminder', orderCtrl.acknowledgeReminder);
router.post('/order/readAllNotification/:UserID', orderCtrl.readAllNotification);

//Report Controller Routing
router.post('/report/getDataOrderWise', reportCtrl.getDataOrderWise);
router.post('/report/getDataProductWise', reportCtrl.getDataProductWise);
router.post('/report/getAllByTrendService', reportCtrl.getAllByTrendService);
router.get('/report/getAllOrdersBySortReport', reportCtrl.getAllOrdersBySortReport);
router.post('/report/getDataFranchiseBuHeadWise', reportCtrl.getDataFranchiseBuHeadWise);
router.post('/report/getDataForTatReport', reportCtrl.getDataForTatReport);



module.exports = router;