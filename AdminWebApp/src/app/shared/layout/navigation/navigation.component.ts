import {Router} from '@angular/router';
import {Component, OnInit} from '@angular/core';
import {LoginInfoComponent} from "../../user/login-info/login-info.component";
import {CommonService} from "@app/core/common/common.service";
import {LayoutService} from "@app/core/services/layout.service";

@Component({

    selector: 'sa-navigation',
    templateUrl: './navigation.component.html'
})
export class NavigationComponent implements OnInit {
    currentUser: any;
    NavItems: any = [];
    checkMenu = false;

    constructor(private commonService: CommonService, public router: Router, private layoutService: LayoutService) {
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if(this.currentUser && this.currentUser.RoleID && this.currentUser.RoleID.Name == 'Super Admin') {
            document.getElementById('webapp-title').innerText = 'Stryker India | Instrument tracking tool';
            this.NavItems = [
                {
                    title: 'Users',
                    name: 'Users',
                    url: '/users',
                    icon: 'fa-user'
                },
                {
                    title: 'Roles',
                    name: 'Roles',
                    url: '/roles',
                    icon: 'fa-group'
                },
                {
                    title: 'Products',
                    name: 'Products',
                    url: '/products',
                    icon: 'fa-product-hunt'
                },
                {
                    title: 'Inventories',
                    name: 'Inventories',
                    url: '/inventorys',
                    icon: 'fa-houzz'
                },

                {
                    title: 'Tool-kit',
                    name: 'Tool-kit',
                    url: '/toolkits',
                    icon: 'fa-wrench'
                },
                // {
                //     title: 'costlevels',
                //     name: 'CostLevels',
                //     url: '/costlevels',
                //     icon: 'fa-level-up'
                // },
                {
                    title: 'Customers',
                    name: 'Customers',
                    url: '/customers',
                    icon: 'fa-user'
                },
                // {
                //     title: 'Orders',
                //     name: 'Orders',
                //     url: '/orders',
                //     icon: 'fa-shopping-cart'
                // },
                {
                    title: 'reports',
                    name: 'Reports',
                    url: '',
                    icon: 'fa-bar-chart',
                    children: [
                        {
                            title: 'Trend Of Service',
                            name: 'Trend Of Service',
                            url: '/reports/trend-service'
                        },
                        {
                            title: 'BU & Franchise Wise',
                            name: 'BU & Franchise Wise',
                            url: '/reports/franchise-buhead'
                        },
                        {
                            title: 'Orders',
                            name: 'Orders Wise',
                            url: '/reports/orders'
                        },
                        {
                            title: 'products',
                            name: 'Products Wise',
                            url: '/reports/products'
                        },
                        {
                            title: 'Quantity & Amount Wise',
                            name: 'Quantity & Amount Wise',
                            url: '/reports/sort-order'
                        },
                        {
                            title: 'TAT Report',
                            name: 'TAT Report',
                            url: '/reports/tat'
                        }
                    ]
                }
            ];
        } else if(this.currentUser && this.currentUser.RoleID && this.currentUser.RoleID.Description == 'Dealer') {
            document.getElementById('webapp-title').innerText = 'Stryker India | Instrument tracking tool';
            this.NavItems = [
                {
                    title: 'Create Order',
                    name: 'Create Order',
                    url: '/orders/form/-1',
                    icon: 'fa-plus-square'
                },
                {
                    title: 'Return Order',
                    name: 'Return Order',
                    url: '/orders/returnform',
                    icon: 'fa-undo'
                },
                {
                    title: 'Transfer Order',
                    name: 'Transfer Order',
                    url: '/orders/transferform',
                    icon: 'fa-exchange'
                },
                {
                    title: 'Acknowledge Order',
                    name: 'Acknowledge Order',
                    url: '/orders/view/1',
                    icon: 'fa-check-circle'
                }
            ];
        } else if(this.currentUser && this.currentUser.RoleID && this.currentUser.RoleID.Description == 'DealerChild') {
            document.getElementById('webapp-title').innerText = 'Stryker India | Instrument tracking tool';
            this.NavItems = [
                {
                    title: 'Create Order',
                    name: 'Create Order',
                    url: '/orders/form/-1',
                    icon: 'fa-plus-square'
                }
            ];
        } else if(this.currentUser && this.currentUser.RoleID && this.currentUser.RoleID.Description == 'OM Supervisory') {
            document.getElementById('webapp-title').innerText = 'Stryker India | Instrument tracking tool';
            this.NavItems = [
                {
                    title: 'Create Order',
                    name: 'Create Order',
                    url: '/orders/form/-1',
                    icon: 'fa-plus-square'
                }
            ];
        }
        this.checkMenu = true;
    }

    ngOnInit() {

    }

    onClickMenu(){
        if(window.innerWidth < 768){
            this.layoutService.onCollapseMenu()
        }
    }
}
