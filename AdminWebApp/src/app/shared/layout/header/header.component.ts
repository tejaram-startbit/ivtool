import { Component, OnInit, Inject } from '@angular/core';
import {Router, ActivatedRoute, Event, NavigationStart, NavigationEnd, NavigationError} from "@angular/router";
import { DOCUMENT } from '@angular/platform-browser';

declare var $: any;

@Component({
    selector: 'sa-header',
    templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit {
    currentUser = JSON.parse(localStorage.getItem('currentUser'));
    constructor(private router: Router, @Inject(DOCUMENT) private document: Document) {
        // this.router.events.subscribe((event: Event)=> {
        //     if (event instanceof NavigationEnd) {
        //         this.document.body.classList.remove('hidden-menu-mobile-lock');
        //         this.document.body.classList.remove('hidden-menu');
        //     };
        // });
    }

    ngOnInit() {
    }


    searchMobileActive = false;

    toggleSearchMobile() {
        this.searchMobileActive = !this.searchMobileActive;

        $('body').toggleClass('search-mobile', this.searchMobileActive);
    }

    onSubmit() {
        this.router.navigate(['/miscellaneous/search']);

    }

    onClickLogo(){
        this.router.navigate(['/dashboard']);
    }
}
