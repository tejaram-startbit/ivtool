import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {NotificationService} from '@app/core/services';
import {MatSnackBar} from "@angular/material";
import {RoleService} from "@app/features/Role/role.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {Location} from "@angular/common";
import {Observable} from "rxjs";
import {Ng4LoadingSpinnerService} from "ng4-loading-spinner";


@Component({
    selector: 'project-role-form',
    templateUrl: './role-form.component.html',
})
export class RoleFormComponent implements OnInit {
    ID: any;
    name: any;
    States: any;
    RoleForm: FormGroup;
    showSpinner: boolean = false;
    ImageBase64Data: any;
    showForm: boolean = false;
    validationRoleFormDetailOptions = {};

    constructor(private http: HttpClient,
                private router: Router,
                private fb: FormBuilder,
                private location: Location,
                private route: ActivatedRoute,
                private roleService: RoleService,
                private notificationService: NotificationService,
                private snackBar: MatSnackBar,
                private spinnerService: Ng4LoadingSpinnerService) {

        this.RoleForm = fb.group({
            Name: ['', [Validators.required]],
            Description: ['', [Validators.required]]
        });

        this.validationRoleFormDetailOptions = {
            // Rules for form validation
            rules: {
                Name: {
                    required: true
                }
            },

            // Messages for form validation
            messages: {
                Name: {
                    required: 'Please enter your Role Name'
                }
            },
            submitHandler: this.onSubmit

        };
        this.ID = this.route.params['value'].id;
        if (this.ID !== '-1') {
            this.getOne(this.ID);
        }
        this.showForm = true;
    }

    onSubmit() {
        console.log('\n', 'submit handler for validated form', '\n\n')
    }

    ngOnInit() {

    }

    onSubmitForm() {
        if (this.RoleForm.invalid) {
            return 0;
        }
        this.RoleForm.value.Avatar = this.ImageBase64Data;
        if (this.ID === '-1') {
            this.createNew();
        } else {
            this.editExisting();
        }
    }


    createNew() {
        this.showSpinner = true;
        this.roleService.addRole(this.RoleForm.value).subscribe((data: {}) => {
            if (data['status']) {
                this.snackBar.open('Record created successfully', 'success', {
                    duration: 5000,
                    panelClass: ['success-snackbar'],
                    verticalPosition: 'top'
                });
                this.location.back();
            }
        }, err => {

        });

    }

    editExisting() {
        this.notificationService.smartMessageBox({
            title: "Update!",
            content: "Are you sure update this record?",
            buttons: '[No][Yes]'
        }, (ButtonPressed) => {
            if (ButtonPressed === "Yes") {
                this.showSpinner = true;
                this.roleService.updateRole(this.ID, this.RoleForm.value).subscribe((data: {}) => {
                    if (data['status']) {
                        this.snackBar.open('Record updated successfully', 'success', {
                            duration: 5000,
                            panelClass: ['success-snackbar'],
                            verticalPosition: 'top'
                        });
                        this.location.back();
                    }
                }, err => {

                });
            }
            if (ButtonPressed === "No") {

            }

        });

    }

    getOne(id) {
        this.spinnerService.show();
        this.roleService.getOneRole(id).subscribe((data: {}) => {
            if (data['status']) {
                this.RoleForm = this.fb.group({
                    Name: [data['result'].Name, [Validators.required]],
                    Description: [data['result'].Description, [Validators.required]]
                });
            }
            this.spinnerService.hide();
        }, err => {

        });
    }

    register() {
        this.router.navigate(['/role/list']);
    }

    cancel() {
        this.location.back();
    }
}