import {NgModule} from "@angular/core";

import {routing} from "@app/features/Role/role.routing";
import {SharedModule} from "@app/shared/shared.module";
import {SmartadminDatatableModule} from '@app/shared/ui/datatable/smartadmin-datatable.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {Ng4LoadingSpinnerModule} from "ng4-loading-spinner";

import {SmartadminInputModule} from '@app/shared/forms/input/smartadmin-input.module';
import {SmartadminWizardsModule} from "@app/shared/forms/wizards/smartadmin-wizards.module";
import {NgMultiSelectDropDownModule} from "ng-multiselect-dropdown";
import {MatStepperModule, MatIconModule} from "@angular/material";
import {MaterialModuleModule} from "@app/core/common/material-module/material-module.module";
import {SmartadminValidationModule} from "@app/shared/forms/validation/smartadmin-validation.module";
import {TreeviewModule} from 'ngx-treeview';
import {SmartadminEditorsModule} from "@app/shared/forms/editors/smartadmin-editors.module";

import {RoleComponent} from "@app/features/Role/role.component";
import {RoleFormComponent} from "@app/features/Role/role-form.component";

@NgModule({
    declarations: [
        RoleComponent,
        RoleFormComponent
    ],
    imports: [
        SharedModule,
        routing,
        SmartadminDatatableModule,
        FormsModule,
        ReactiveFormsModule,
        SmartadminInputModule,
        SmartadminWizardsModule,
        NgMultiSelectDropDownModule.forRoot(),
        MaterialModuleModule,
        SmartadminValidationModule,
        TreeviewModule.forRoot(),
        SmartadminEditorsModule,
        Ng4LoadingSpinnerModule.forRoot()

    ],
    providers: [],
    exports: [
        FormsModule,
        ReactiveFormsModule,
        MatStepperModule,
        MatIconModule
    ]
})
export class RoleModule {

}
