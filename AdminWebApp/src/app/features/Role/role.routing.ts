import {RouterModule, Routes} from "@angular/router";
import {RoleComponent} from "@app/features/Role/role.component";
import {RoleFormComponent} from "@app/features/Role/role-form.component";

export const routes: Routes = [

    {
        path: '',
        redirectTo: 'list'
    },
    {
        path: 'list',
        component: RoleComponent
    },
    {
        path: 'form/:id',
        component: RoleFormComponent
    }
];

export const routing = RouterModule.forChild(routes);
