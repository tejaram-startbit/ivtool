import {RouterModule, Routes} from "@angular/router";
import {ToolkitComponent} from "@app/features/Tool-kit/tool-kit.component";
import {ToolkitFormComponent} from "@app/features/Tool-kit/tool-kit-form.component";


export const routes: Routes = [

    {
        path: '',
        redirectTo: 'list'
    },
    {
        path: 'list',
        component: ToolkitComponent
    },
    {
        path: 'form/:id',
        component: ToolkitFormComponent
    }
];

export const routing = RouterModule.forChild(routes);
