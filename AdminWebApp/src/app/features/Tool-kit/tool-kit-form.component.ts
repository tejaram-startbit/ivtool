import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {NotificationService} from '@app/core/services';
import {MatSnackBar} from "@angular/material";
import {ToolkitService} from "@app/features/Tool-kit/tool-kit.service";
import {InventoryService} from "@app/features/Inventory/Inventory.service";
import {FormBuilder, FormGroup, Validators, FormControl} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {Location} from "@angular/common";
import {Observable, Subject} from "rxjs";
import {Ng4LoadingSpinnerService} from "ng4-loading-spinner";
import {RoleService} from "@app/features/Role/role.service";
import { takeUntil } from 'rxjs/operators';


@Component({
    selector: 'project-tool-kit-form',
    templateUrl: './tool-kit-form.component.html',
})
export class ToolkitFormComponent implements OnInit {
    ID: any;
    name: any;
    States: any;
    ToolkitForm: FormGroup;
    showSpinner: boolean = false;
    ImageData: any;
    selectedExcelFile: any;
    showForm: boolean = false;
    validationToolkitFormDetailOptions = {};
    public bankCtrl: FormControl = new FormControl();
    public bankFilterCtrl: FormControl = new FormControl();
    public filteredBanks: any = [];
    Products2: any = [];
    Products: any = [];
    private _onDestroy = new Subject<void>();
    Inventory_ID: any;
    Quantity: any;
    showerror1: boolean = false;
    showerror2: boolean = false;
    ToolkitInventories = []; 
    constructor(private http: HttpClient,
                private router: Router,
                private fb: FormBuilder,
                private location: Location,
                private route: ActivatedRoute,
                private toolkitService: ToolkitService,
                private inventoryService: InventoryService,
                private notificationService: NotificationService,
                private snackBar: MatSnackBar,
                private spinnerService: Ng4LoadingSpinnerService,
                private roleServece: RoleService) {

        this.ToolkitForm = fb.group({
            KitCode: ['', [Validators.required]],
            Name: ['', [Validators.required]],
            Description: ['']
        });


        this.inventoryService.getAllInventorys().subscribe((data: {}) => {
            this.Products = data["result"];
            this.Products2 = data["result"];
        });

        this.validationToolkitFormDetailOptions = {
            // Rules for form validation
            rules: {
                KitCode: {
                    required: true
                },
                Name: {
                    required: true
                },
                Description: {
                    required: true
                },
            },

            // Messages for form validation
            messages: {
                KitCode: {
                    required: 'Please enter tool kit code  Name'
                },
                Name: {
                    required: 'Please enter tool kit  Name'
                },
                Description: {
                    required: 'Please enter tool-kit Description'
                }
            },
            submitHandler: this.onSubmit

        };
        this.ID = this.route.params['value'].id;
        if (this.ID !== '-1') {
            this.getOne(this.ID);
        }
        this.showForm = true;
    }

    onSubmit() {
        console.log('\n', 'submit handler for validated form', '\n\n')
    }

    ngOnInit() {
        this.bankFilterCtrl.valueChanges
        .pipe(takeUntil(this._onDestroy))
        .subscribe(() => {
            this.filterBanks();
        });
    }

    private filterBanks() {
        if (!this.Products) {
            return;
        }
        
        // get the search keyword
        let search = this.bankFilterCtrl.value;
        if(search.length < 3){
            // this.filteredBanks = [];
            return;
        }
        if (!search) {
            this.filteredBanks = this.Products.slice();
            return;
        } else {
            search = search.toLowerCase();
        }
        // filter the banks
        this.filteredBanks = this.Products.filter(
            product => product.Name.toLowerCase().indexOf(search) > -1
        );
    }

    async addProduct() {
        this.Inventory_ID = this.bankCtrl.value;
        if (this.Inventory_ID == undefined || this.Inventory_ID == null) {
            this.showerror1 = true;
            return;
        } else {
            this.showerror1 = false;
        }
        if (this.Quantity == undefined || this.Quantity == null) {
            this.showerror2 = true;
            return;
        }

        var indexData = this.ToolkitInventories.findIndex(x => x.Inventory_ID == this.Inventory_ID);

        if(indexData != -1){
            this.ToolkitInventories[indexData].Quantity =  this.ToolkitInventories[indexData].Quantity + this.Quantity;
        }
        else{
            var ind = this.filteredBanks.findIndex(x => x._id == this.Inventory_ID);
            this.ToolkitInventories.push({
                Inventory_ID: this.Inventory_ID,
                Name: this.filteredBanks[ind].Name,
                Quantity: this.Quantity,
            });
        }
            this.bankCtrl.setValue(null);

            this.filteredBanks = [];
            this.Inventory_ID = null;
            this.Quantity = null;
        


    }

    deleteProduct(productid, index) {
        let lab = 'Are you sure delete this record?';
        this.notificationService.smartMessageBox({
            title: "Delete!",
            content: lab,
            buttons: '[No][Yes]'
        }, (ButtonPressed) => {
            if (ButtonPressed === "Yes") {
        this.ToolkitInventories.splice(index, 1);
        }
        if (ButtonPressed === "No") {

        }
    });
    }

    modelChanged1() {
        if (this.Inventory_ID == undefined) {
            this.showerror1 = true;
        } else {
            this.showerror1 = false;
        }

    }

    modelChanged2() {

        if (this.Quantity == undefined) {
            this.showerror2 = true;
        } else {
            this.showerror2 = false;
        }

    }

    onSubmitForm() {
        if (this.ToolkitForm.invalid) {
            return 0;
        }
        if(this.ToolkitInventories.length == 0){
            this.snackBar.open('Please select atleast one inventory in tool-kit.', 'OK', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
            return 0;
        }
        if (this.ID === '-1') {
            this.createNew();
        } else {
            this.editExisting();
        }
    }

    createNew() {
        this.showSpinner = true;
        var dataInput = this.ToolkitForm.value;
        dataInput.Inventory = this.ToolkitInventories
        this.toolkitService.addToolkit(dataInput, this.ImageData).subscribe(data => {
            if (data['status']) {
                this.snackBar.open('Record created successfully', 'OK', {
                    duration: 5000,
                    panelClass: ['success-snackbar'],
                    verticalPosition: 'top'
                });
                this.location.back();
            }
            else{
                this.showSpinner = false;
                this.snackBar.open(data['message'], 'OK', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
            }
        }, err => {
            this.snackBar.open('something went error', 'OK', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        });

    }

    editExisting() {
        this.notificationService.smartMessageBox({
            title: "Update!",
            content: "Are you sure update this record?",
            buttons: '[No][Yes]'
        }, (ButtonPressed) => {
            if (ButtonPressed === "Yes") {
                this.showSpinner = true;
                var dataInput = this.ToolkitForm.value;
                dataInput.Inventory = this.ToolkitInventories
                this.toolkitService.updateMyToolkit(this.ID, dataInput).subscribe(data => {
                    if (data['status']) {
                        this.snackBar.open('Record updated successfully', 'OK', {
                            duration: 5000,
                            panelClass: ['success-snackbar'],
                            verticalPosition: 'top'
                        });
                        this.location.back();
                    }
                }, err => {
                    this.snackBar.open('something went error', 'OK', {
                        duration: 5000,
                        panelClass: ['danger-snackbar'],
                        verticalPosition: 'top'
                    });
                });
            }
            if (ButtonPressed === "No") {

            }

        });

    }

    getOne(id) {
        this.spinnerService.show();
        this.toolkitService.getOneToolkit(id).subscribe(data => {
            if (data['status']) {
                this.ToolkitForm = this.fb.group({
                    KitCode: [data['result'].KitCode, [Validators.required]],
                    Name: [data['result'].Name, [Validators.required]],
                    Description: [data['result'].Description, [Validators.required]]
                });
                this.ToolkitInventories = data['result'].Inventory;
            }
            this.spinnerService.hide();
        }, err => {
            this.snackBar.open('something went error', 'OK', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        });
    }

    onChangeCustomers(){
        // if(this.customerCtrl.value != 1){
        //     this.OrderTransferForm.patchValue({
        //         CustomerID: this.customerCtrl.value
        //     })
        // } else {
        //     this.OrderTransferForm.patchValue({
        //         CustomerID: ''
        //     })
        // }
    }

    onFileChange(event) {
        const reader = new FileReader();

        if (event.target.files && event.target.files.length) {
            const [file] = event.target.files;
            reader.readAsDataURL(file);
            const fileName = event.target.files[0].name;
            const lastIndex = fileName.lastIndexOf('.');
            const extension = fileName.substr(lastIndex + 1);
            if (extension.toLowerCase() === 'jpg' || extension.toLowerCase() === 'jpeg' || extension.toLowerCase() === 'png') {
                reader.onload = () => {
                    this.ImageData = event.target.files[0];
                };
            } else {
                this.snackBar.open('Invalid selected file', 'OK', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
            }
        }
    }

    register() {
        this.router.navigate(['/inventory/list']);
    }

    cancel() {
        this.location.back();
    }

    onExcelFileChange(args) {
        const reader = new FileReader();
        const self = this, file = args.srcElement && args.srcElement.files && args.srcElement.files[0];
        if(file) {
            const fileName = file.name;
            const lastIndex = fileName.lastIndexOf('.');
            const extension = fileName.substr(lastIndex + 1);
            if(extension == 'xlsx' || extension == 'xls'){
                this.selectedExcelFile = file;
            } else {
                this.snackBar.open('Invalid selected file', 'OK', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
            }
        }
    }

    uploadExcel(){
        if(this.selectedExcelFile) {
            this.showSpinner = true;
            this.toolkitService.uploadExcel(this.selectedExcelFile).subscribe(data => {
                if (data['status']) {
                    this.snackBar.open('Successfully uploaded excel', 'OK', {
                        duration: 5000,
                        panelClass: ['success-snackbar'],
                        verticalPosition: 'top'
                    });
                    this.location.back();
                }
            }, err => {
                this.showSpinner = false;
                this.snackBar.open('something went error', 'OK', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
            });
        } else {
            this.snackBar.open('Please select excel file', 'OK', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        }
    }
}