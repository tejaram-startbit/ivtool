import {Injectable} from '@angular/core';

import {HttpClient, HttpHeaders, HttpErrorResponse} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {map, catchError, tap} from 'rxjs/operators';

import {CommonService} from "@app/core/common/common.service";

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json'
    })
};

@Injectable({
    providedIn: 'root'
})
export class ToolkitService {
    url: string;
    DocUrl: string;

    constructor(private http: HttpClient, private commService: CommonService) {
        this.url = commService.getApiUrl() + '/toolkit/';
    }

    private extractData(res: Response) {
        let body = res;
        return body || {};
    }

    getAllToolkits(): Observable<any> {
        return this.http.get(this.url + 'getAll/').pipe(
            map(this.extractData));
    }

    getOneToolkit(id): Observable<any> {
        return this.http.get(this.url + 'getOne/' + id).pipe(
            map(this.extractData));
    }

    addToolkit(data, file): Observable<any> {
        const formData = new FormData();
        formData.append('data', JSON.stringify(data));
        if(file){
            formData.append('file', file);
        }
        return this.http.post<any>(this.url + 'create', formData).pipe(
            tap(() => console.log(`added role w/ id=${data._id}`)),
            catchError(this.handleError<any>('addToolkit'))
        );
    }

    updateMyToolkit(id, data,): Observable<any> {
        const formData = new FormData();
        formData.append('data', JSON.stringify(data));
        formData.append('id', id);
    
        return this.http.post<any>(this.url + 'update/' + id, formData).pipe(
            tap(() => console.log(`updated role w/ id=${data._id}`)),
            catchError(this.handleError<any>('updateToolkits'))
        );
    }

    deleteToolkit(id): Observable<any> {
        return this.http.delete<any>(this.url + 'delete/' + id, httpOptions).pipe(
            tap(() => console.log(`deleted role w/ id=${id}`)),
            catchError(this.handleError<any>('deleteToolkits'))
        );
    }

    uploadExcel(file){
        const formData = new FormData();
        if(file){
            formData.append('file', file);
        }
        return this.http.post<any>(this.url + 'uploadExcel', formData).pipe(
            tap(() => console.log(`uploadExcel`)),
            catchError(this.handleError<any>('uploadExcel'))
        );
    }


    private handleError<T>(operation = 'operation', result?: any) {
        return (error: any): Observable<any> => {

            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // TODO: better job of transforming error for home consumption
            console.log(`${operation} failed: ${error.message}`);
            const errorData = {
                status: false,
                message: 'Server Error'
            };
            // Let the app keep running by returning an empty result.
            return of(errorData);
        };
    }
}
