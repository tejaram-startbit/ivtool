import {RouterModule, Routes} from "@angular/router";
import {UserComponent} from "@app/features/User/user.component";
import {UserFormComponent} from "@app/features/User/user-form.component";

export const routes: Routes = [

    {
        path: '',
        redirectTo: 'list'
    },
    {
        path: 'list',
        component: UserComponent
    },
    {
        path: 'form/:id',
        component: UserFormComponent
    }
];

export const routing = RouterModule.forChild(routes);
