import { takeUntil } from 'rxjs/operators';
import { FormControl } from '@angular/forms';
import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {NotificationService} from '@app/core/services';
import {ReportService} from "@app/features/Report/report.service";
import {Observable} from "rxjs";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {MatSnackBar} from "@angular/material";
import {CommonService} from "@app/core/common/common.service";
import {Ng4LoadingSpinnerService} from "ng4-loading-spinner";
import {UserService} from "@app/features/User/user.service";
import {ProductService} from "@app/features/Product/product.service";
import {OrderService} from "@app/features/Order/order.service";
import {ExcelService} from "@app/core/services/excel/excel.service";

import {ReplaySubject, Subject} from "rxjs";

declare var $: any;

@Component({
    selector: 'franchise-buhead-order-report',
    templateUrl: './franchise-buhead-report.component.html',
})
export class FranchiseBuHeadReportComponent implements OnInit {
    Reports: any;
    displayedColumns: string[] = [
        'SN',
        'RequestID',
        'SalesPerson',
        'SalesPersonContact',
        'CreatedDate',
        'Quantity',
        'TotalAmount',
        'Action'
    ];
    dataSource: MatTableDataSource<any>;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    apiUrl: string;
    currentUser: any;
    orderFilterData = {
        MisFranchise: '0',
            MisBuHead: '0',
    };

    allUsers: any = [];
    allProducts: any = [];
    selectedOrder: any = [];
    allOrderStatus: any = [
        {value: 1, name: 'Pending'},
        {value: 2, name: 'Approved'},
        {value: 3, name: 'Disapproved'},
        {value: 4, name: 'Dispatched'},
        {value: 5, name: 'Delivered'},
        {value: 6, name: 'Acknowledged'},
        {value: 7, name: 'Return request'},
        // {value: 8, name: 'Transfered'},
        {value: 9, name: 'Returned'}
    ];

    public franchiseCtrl: FormControl = new FormControl('0');
    public franchiseFilterCtrl: FormControl = new FormControl();
    public filteredFranchises: any = [];

    public buHeadCtrl: FormControl = new FormControl('0');
    public buHeadFilterCtrl: FormControl = new FormControl();
    public filteredBuHead: any = [];

    allFranchise: any = [];
    allBuHead: any = [];

    constructor(private http: HttpClient,
                private notificationService: NotificationService,
                private reportsService: ReportService,
                private snackBar: MatSnackBar,
                private commonService: CommonService,
                private userService: UserService,
                private productService: ProductService,
                public dialog: MatDialog,
                private spinnerService: Ng4LoadingSpinnerService,
                private excelService:ExcelService
    ) {
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.apiUrl = this.commonService.getApiUrl();

        this.getAllApprover();

    }

    private _onDestroy = new Subject<void>();

    ngOnInit() {
        this.franchiseFilterCtrl.valueChanges
        .pipe(takeUntil(this._onDestroy))
        .subscribe(() => {
            this.filterFranchise();
        });

        this.buHeadFilterCtrl.valueChanges
        .pipe(takeUntil(this._onDestroy))
        .subscribe(() => {
            this.filterBuHead();
        });
    }

    clearOrderWise(){
        this.orderFilterData = {
            MisFranchise: '0',
            MisBuHead: '0',
        };
        this.selectedOrder = [];
        this.dataSource = new MatTableDataSource(this.selectedOrder);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;

        this.franchiseCtrl.setValue('0');
        this.buHeadCtrl.setValue('0');
    }

    getDataFranchiseBuHeadWise(){
        this.spinnerService.show();
        this.reportsService.getDataFranchiseBuHeadWise(this.orderFilterData).subscribe(data => {
            if(data['status']){
                this.selectedOrder = data['result'];
                this.dataSource = new MatTableDataSource(this.selectedOrder);
                this.dataSource.paginator = this.paginator;
                this.dataSource.sort = this.sort;
                this.spinnerService.hide();
                if(this.selectedOrder.length == 0){
                    this.snackBar.open('Record not found', 'OK', {
                        duration: 5000,
                        panelClass: ['warning-snackbar'],
                        verticalPosition: 'top'
                    });
                }
            } else {
                this.snackBar.open(data['message'], 'OK', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
            }
            this.spinnerService.hide();
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('something went error', 'OK', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        })
    }

    getAllApprover(){
        this.spinnerService.show();
        this.userService.getAllApprover().subscribe(data => {
            if(data['status']){
                const map = new Map();
                data['result'].map(item => {
                    if(item.MisFranchise && !map.has(item.MisFranchise)){
                        map.set(item.MisFranchise, true);    // set any value to Map
                        this.allFranchise.push({
                            Name: item.MisFranchise
                        });
                    }
                    if(item.MisBUGroup && !map.has(item.MisBUGroup)){
                        map.set(item.MisBUGroup, true);    // set any value to Map
                        this.allBuHead.push({
                            Name: item.MisBUGroup
                        });
                    }
                });
                this.filteredFranchises = this.allFranchise;
                this.filteredBuHead = this.allBuHead;
            } else {
                this.snackBar.open(data['message'], 'OK', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
            }
            this.spinnerService.hide();
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('something went error', 'OK', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        })
    }

    exportOrderAsXLSX():void {
        this.excelService.exportAsExcelFile(this.selectedOrder, 'order');
    }

    public getTotalQuantity() {
        return this.selectedOrder.reduce((accum, curr) => accum + curr.Products.length, 0);
      }

      public getTotalAmount() {
        return this.selectedOrder.reduce((accum, curr) => accum + curr.TotalAmount, 0);
      }

      private filterFranchise() {
        // get the search keyword
        let search = this.franchiseFilterCtrl.value;
        if (!search) {
            this.filteredFranchises = this.allFranchise.slice();
            return;
        } else {
            search = search.toLowerCase();
        }
        // filter the Dealer
        this.filteredFranchises = this.allFranchise.filter(
            x => x.Name.toLowerCase().indexOf(search) > -1
        );
    }

    onChangeFranchise(){
        this.orderFilterData.MisFranchise = this.franchiseCtrl.value;
    }

    private filterBuHead() {
        // get the search keyword
        let search = this.buHeadFilterCtrl.value;
        if (!search) {
            this.filteredBuHead = this.allBuHead.slice();
            return;
        } else {
            search = search.toLowerCase();
        }
        // filter the Dealer
        this.filteredBuHead = this.allBuHead.filter(
            x => x.Name.toLowerCase().indexOf(search) > -1
        );
    }

    onChangeBuHead(){
        this.orderFilterData.MisBuHead = this.buHeadCtrl.value;
    }

    openDialog(orderDetail): void {
        const dialogRef = this.dialog.open(BuFranchiseDetailDialogReport, {
            width: '99%',
            data: {
                orderDetail: orderDetail
            }
        });

        dialogRef.afterClosed().subscribe(result => {

        });
    }
}


@Component({
    selector: 'bu-franchise-detail-dialog-report',
    templateUrl: 'bu-franchise-detail-dialog-report.html',
})
export class BuFranchiseDetailDialogReport{
    productEdited: boolean = false;
    enableEdit = -1;
    productOldArr = [];

    currentUser: any;
    apiUrl: string;
    orderDetail: any = {};
    dispatchDetail: any;
    orderTimeLineDetail: any;
    acknowledgeDetail: any;
    orderReturDetail: any;
    allStatus: any = ['', 'Pending', 'Approved', 'Disapproved', 'Dispatched', 'Delivered', 'Acknowledged', 'Return request', 'Transfer', 'Returned', 'Declined'];
    currentDate: Date = new Date();
    scheduledReturnDate: Date;
    transferDetail: any;
    productInTransferRequest: any = [];
    constructor(public commonService: CommonService,
                public orderService: OrderService,
                public spinnerService: Ng4LoadingSpinnerService,
                private snackBar: MatSnackBar,
                private notificationService: NotificationService,
                public dialogRef: MatDialogRef<BuFranchiseDetailDialogReport>,
                @Inject(MAT_DIALOG_DATA) public data: any) {
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.apiUrl = this.commonService.getApiUrl();
        this.orderDetail = data.orderDetail;
        this.scheduledReturnDate = this.orderDetail.ScheduleReturnDate;
        this.orderDetail.Products.map(item => {
            this.productOldArr.push({
                ProductConsumable: item.ProductConsumable,
                Quantity: item.Quantity,
                Price: item.Price
            });
        });
        this.getDispatcheOrderDetail(this.orderDetail._id);
        this.getOrderTimeLine(this.orderDetail._id);
        this.getOrderAcknowledgeDetail(this.orderDetail._id);
        this.getOrderReturnData(this.orderDetail._id);
        this.getTransferRequestDetail(this.orderDetail._id);
    }

    getOrderReturnData(id){
        this.spinnerService.show();
        this.orderService.getOrderReturnData(id).subscribe(data => {
            if(data['status']) {
                this.orderReturDetail = data['result'];
            } else {
                this.snackBar.open(data['message'], 'OK', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
            }
            this.spinnerService.hide();
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('something went error', 'OK', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        })
    }

    getTransferRequestDetail(id){
        this.spinnerService.show();
        this.orderService.getTransferRequest(id).subscribe(data => {
            if(data['status']) {
                this.transferDetail = data['result'];
                if(this.transferDetail)
                this.productInTransferRequest = this.transferDetail.TransferData.Products.filter(x => x.value)
            } else {
                this.snackBar.open(data['message'], 'OK', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
            }
            this.spinnerService.hide();
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('something went error', 'OK', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        })
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    getDispatcheOrderDetail(OrderID){
        this.spinnerService.show();
        this.orderService.getDispatcheOrderDetail(OrderID).subscribe(data => {
            if(data['status']) {
                this.dispatchDetail = data['result'];
            } else {
                this.snackBar.open(data['message'], 'OK', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
            }
            this.spinnerService.hide();
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('something went error', 'OK', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        })
    }

    getOrderTimeLine(OrderID){
        this.spinnerService.show();
        this.orderService.getOrderTimeLine(OrderID).subscribe(data => {
            if(data['status']) {
                this.orderTimeLineDetail = data['result'];
            } else {
                this.snackBar.open(data['message'], 'OK', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
            }
            this.spinnerService.hide();
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('something went error', 'OK', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        })
    }

    getOrderAcknowledgeDetail(OrderID) {
        this.spinnerService.show();
        this.orderService.getOrderAcknowledgeDetail(OrderID).subscribe(data => {
            if(data['status']) {
                this.acknowledgeDetail = data['result'];
            } else {
                this.snackBar.open(data['message'], 'OK', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
            }
            this.spinnerService.hide();
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('something went error', 'OK', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        })
    }

    onClickDelivered(OrderID){
        let lab = 'Are you sure this order has been successfully delivered?';
        this.notificationService.smartMessageBox({
            title: "Delivered!",
            content: lab,
            buttons: '[No][Yes]'
        }, (ButtonPressed) => {
            if (ButtonPressed === "Yes") {
                this.spinnerService.show();
                this.orderService.orderDelivered(this.currentUser.id, OrderID).subscribe(data => {
                    this.spinnerService.hide();
                    this.snackBar.open('Order Delivered successfully', 'OK', {
                        duration: 5000,
                        panelClass: ['success-snackbar'],
                        verticalPosition: 'top'
                    });
                    this.dialogRef.close(true);
                }, err => {
                    this.spinnerService.hide();
                    this.snackBar.open('something went error', 'OK', {
                        duration: 5000,
                        panelClass: ['danger-snackbar'],
                        verticalPosition: 'top'
                    });
                })
            }
            if (ButtonPressed === "No") {
                this.spinnerService.hide();
            }
        });
    }

    approveOrder(OrderID) {
        let lab = 'Are you sure to approve this order request?';
        this.notificationService.smartMessageBox({
            title: "Approve!",
            content: lab,
            buttons: '[No][Yes]'
        }, (ButtonPressed) => {
            if (ButtonPressed === "Yes") {
                this.spinnerService.show();
                this.orderService.approveOrder(OrderID, this.currentUser.id).subscribe(data => {
                    this.spinnerService.hide();
                    this.snackBar.open('Order Approved successfully', 'OK', {
                        duration: 5000,
                        panelClass: ['success-snackbar'],
                        verticalPosition: 'top'
                    });
                    this.dialogRef.close(true);
                }, err => {
                    this.spinnerService.hide();
                    this.snackBar.open('something went error', 'OK', {
                        duration: 5000,
                        panelClass: ['danger-snackbar'],
                        verticalPosition: 'top'
                    });
                })
            }
            if (ButtonPressed === "No") {
                this.spinnerService.hide();
            }
        });
    }

    disapproveOrder(OrderID) {
        let lab = 'Do you want to disapprove this order request?';
        this.notificationService.smartMessageBox({
            title: "Disapprove!",
            content: lab,
            buttons: '[No][Yes]'
        }, (ButtonPressed) => {
            if (ButtonPressed === "Yes") {
                this.spinnerService.show();
                this.orderService.disapproveOrder(OrderID, this.currentUser.id, '').subscribe(data => {
                    this.spinnerService.hide();
                    this.snackBar.open('Order Disapproved successfully', 'OK', {
                        duration: 5000,
                        panelClass: ['success-snackbar'],
                        verticalPosition: 'top'
                    });
                    this.dialogRef.close(true);
                }, err => {
                    this.spinnerService.hide();
                    this.snackBar.open('something went error', 'OK', {
                        duration: 5000,
                        panelClass: ['danger-snackbar'],
                        verticalPosition: 'top'
                    });
                })
            }
            if (ButtonPressed === "No") {
                this.spinnerService.hide();
            }
        });
    }

    onClickReturAccept(OrderID){
        let lab = 'Are you sure this order has been successfully returned?';
        this.notificationService.smartMessageBox({
            title: "Returned!",
            content: lab,
            buttons: '[No][Yes]'
        }, (ButtonPressed) => {
            if (ButtonPressed === "Yes") {
                this.spinnerService.show();
                this.orderService.orderReturnAccept(OrderID).subscribe(data => {
                    this.spinnerService.hide();
                    this.snackBar.open('Order Returned successfully', 'OK', {
                        duration: 5000,
                        panelClass: ['success-snackbar'],
                        verticalPosition: 'top'
                    });
                    this.dialogRef.close(true);
                }, err => {
                    this.spinnerService.hide();
                    this.snackBar.open('something went error', 'OK', {
                        duration: 5000,
                        panelClass: ['danger-snackbar'],
                        verticalPosition: 'top'
                    });
                })
            }
            if (ButtonPressed === "No") {
                this.spinnerService.hide();
            }
        });
    }

    onClickTransferAccept(OrderID){
        let lab = 'Are you sure want to transfer this order from ' + this.transferDetail.OldSalesPersonID.FirstName + ' to ' + this.transferDetail.NewSalesPersonID.FirstName;
        this.notificationService.smartMessageBox({
            title: "Accept Transfer!",
            content: lab,
            buttons: '[No][Yes]'
        }, (ButtonPressed) => {
            if (ButtonPressed === "Yes") {
                this.spinnerService.show();
                var data = {
                    CustomerID: this.transferDetail.NewSalesPersonID._id,
                    CustomerDetails: this.transferDetail.TransferData.CustomerDetails,
                    DeliveryLocation: this.transferDetail.TransferData.DeliveryLocation,
                    Products: this.transferDetail.TransferData.Products,
                    currentUserID: this.currentUser.id,
                    transferRequestID: this.transferDetail._id
                }
                this.orderService.transferedOrder(OrderID, data).subscribe(data => {
                    this.spinnerService.hide();
                    this.snackBar.open('Order Transfered successfully', 'OK', {
                        duration: 5000,
                        panelClass: ['success-snackbar'],
                        verticalPosition: 'top'
                    });
                    this.dialogRef.close(true);
                }, err => {
                    this.spinnerService.hide();
                    this.snackBar.open('something went error', 'OK', {
                        duration: 5000,
                        panelClass: ['danger-snackbar'],
                        verticalPosition: 'top'
                    });
                })
            }
            if (ButtonPressed === "No") {
                this.spinnerService.hide();
            }
        });
    }

    dismissDialog() {
        this.dialogRef.close(true);
    }

    updateList(id: number, property: string, value: any) {
        if(property == 'ProductConsumable' && value != 'Yes' && value != 'No'){
            this.orderDetail.Products[id][property] = this.productOldArr[id][property];
        }
    }

    updateProductDetail(){
        let lab = 'Are you sure you want to update this order products detail?';
        this.notificationService.smartMessageBox({
            title: "Update Products!",
            content: lab,
            buttons: '[No][Yes]'
        }, (ButtonPressed) => {
            if (ButtonPressed === "Yes") {
                this.spinnerService.show();
                const prdArr = [];
                this.orderDetail.Products.map(row => {
                    prdArr.push({
                        ProductCode: row.ProductID.ProductCode,
                        ProductID: row.ProductID._id,
                        Quantity: row.Quantity,
                        ProductConsumable: row.ProductConsumable,
                        Price: row.Price ? row.Price : 0,
                        Inventory: row.Inventory,
                        BranchCode: row.BranchCode
                    })
                })
                this.orderService.updateProductDetail(this.orderDetail._id, prdArr, this.currentUser.id).subscribe(data => {
                    this.spinnerService.hide();
                    this.snackBar.open('Product detail updated successfully', 'OK', {
                        duration: 5000,
                        panelClass: ['success-snackbar'],
                        verticalPosition: 'top'
                    });
                    this.dialogRef.close(true);
                }, err => {
                    this.spinnerService.hide();
                    this.snackBar.open('something went error', 'OK', {
                        duration: 5000,
                        panelClass: ['danger-snackbar'],
                        verticalPosition: 'top'
                    });
                })
            }
            if (ButtonPressed === "No") {
                this.spinnerService.hide();
            }
        });
    }

    declineOrder(OrderID) {
        let lab = 'Do you want to decline this order?<br><br><textarea class="form-control" style="color: black" type="text" value="" placeholder="order decline reason" id="decline-reason" name="decline-reason"></textarea>';
        this.notificationService.smartMessageBox({
            title: "Decline Order!",
            content: lab, 
            buttons: '[No][Yes]'
        }, (ButtonPressed) => {
            if (ButtonPressed === "Yes") {
                if(!document.getElementById('decline-reason')['value']){
                    this.snackBar.open('Please post order decline reason', 'OK', {
                                duration: 5000,
                                panelClass: ['danger-snackbar'],
                                verticalPosition: 'top'
                            });
                    return;
                }
                this.spinnerService.show();
                this.orderService.declineOrder(OrderID, this.currentUser.id, document.getElementById('decline-reason')['value']).subscribe(data => {
                    this.spinnerService.hide();
                    this.snackBar.open('Order Declined successfully', 'OK', {
                        duration: 5000,
                        panelClass: ['success-snackbar'],
                        verticalPosition: 'top'
                    });
                    this.dialogRef.close(true);
                }, err => {
                    this.spinnerService.hide();
                    this.snackBar.open('something went error', 'OK', {
                        duration: 5000,
                        panelClass: ['danger-snackbar'],
                        verticalPosition: 'top'
                    });
                })
            }
            if (ButtonPressed === "No") {
                this.spinnerService.hide();
            }
        });
    }

    ocChangeScheduleReturDate(){
        // this.scheduledReturnDate = new Date(event.target.value);
        let lab = 'Do you want to update this order scheduled return date?';
        this.notificationService.smartMessageBox({
            title: "Update Scheduled Return Date!",
            content: lab, 
            buttons: '[No][Yes]'
        }, (ButtonPressed) => {
            if (ButtonPressed === "Yes") {
                this.spinnerService.show();
                this.orderService.updateScheduledReturnDate(this.orderDetail._id, this.currentUser.id, this.scheduledReturnDate).subscribe(data => {
                    this.spinnerService.hide();
                    this.snackBar.open('Scheduled return date updated successfully', 'OK', {
                        duration: 5000,
                        panelClass: ['success-snackbar'],
                        verticalPosition: 'top'
                    });
                    this.dialogRef.close(true);
                }, err => {
                    this.spinnerService.hide();
                    this.snackBar.open('something went error', 'OK', {
                        duration: 5000,
                        panelClass: ['danger-snackbar'],
                        verticalPosition: 'top'
                    });
                })
            }
            if (ButtonPressed === "No") {
                this.scheduledReturnDate = this.orderDetail.ScheduleReturnDate;
                this.spinnerService.hide();
            }
        });
    }

}

