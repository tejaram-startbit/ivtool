import { takeUntil } from 'rxjs/operators';
import { FormControl } from '@angular/forms';
import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {NotificationService} from '@app/core/services';
import {ReportService} from "@app/features/Report/report.service";
import {Observable} from "rxjs";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {MatSnackBar} from "@angular/material";
import {CommonService} from "@app/core/common/common.service";
import {Ng4LoadingSpinnerService} from "ng4-loading-spinner";
import {UserService} from "@app/features/User/user.service";
import {ProductService} from "@app/features/Product/product.service";
import {OrderService} from "@app/features/Order/order.service";
import {ExcelService} from "@app/core/services/excel/excel.service";

import {ReplaySubject, Subject} from "rxjs";

declare var $: any;

@Component({
    selector: 'project-tat-report',
    templateUrl: './tat-report.component.html',
})
export class TatReportComponent implements OnInit {
    Reports: any;
    displayedColumns: string[] = [
        'SN',
        'RequestID',
        'Created',
        'Approved',
        'Dispatched',
        'Delivered',
        'Acknowledged'
    ];
    dataSource: MatTableDataSource<any>;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    apiUrl: string;
    currentUser: any;
    orderFilterData = {
        UserID: '0',
        ProductID: '0',
        FromDate: '',
        ToDate: '',
        RequestID: '',
        Status: '0',
        CustomerName: '',
        CustomerMobile: '',
        ServiceType: '0',
        ProductCode: ''
    };

    allUsers: any = [];
    allProducts: any = [];
    selectedOrder: any = [];
    allOrderStatus: any = [
        {value: 1, name: 'Pending'},
        {value: 2, name: 'Approved'},
        {value: 3, name: 'Disapproved'},
        {value: 4, name: 'Dispatched'},
        {value: 5, name: 'Delivered'},
        {value: 6, name: 'Acknowledged'},
        {value: 7, name: 'Return request'},
        // {value: 8, name: 'Transfered'},
        {value: 9, name: 'Returned'}
    ];

    public customerCtrl: FormControl = new FormControl('0');
    public customerFilterCtrl: FormControl = new FormControl();
    public filteredCustomers: any = [];

    public productCtrl: FormControl = new FormControl('0');
    public productFilterCtrl: FormControl = new FormControl();
    public filteredProducts: any = [];

    constructor(private http: HttpClient,
                private notificationService: NotificationService,
                private reportsService: ReportService,
                private snackBar: MatSnackBar,
                private commonService: CommonService,
                private userService: UserService,
                private productService: ProductService,
                public dialog: MatDialog,
                private spinnerService: Ng4LoadingSpinnerService,
                private excelService:ExcelService
    ) {
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.apiUrl = this.commonService.getApiUrl();

        this.getAllUsers();
        this.getAllProducts();

    }

    private _onDestroy = new Subject<void>();

    ngOnInit() {
        this.customerFilterCtrl.valueChanges
        .pipe(takeUntil(this._onDestroy))
        .subscribe(() => {
            this.filterCustomers();
        });

        this.productFilterCtrl.valueChanges
        .pipe(takeUntil(this._onDestroy))
        .subscribe(() => {
            this.filterProducts();
        });
    }

    private filterCustomers() {
        // get the search keyword
        let search = this.customerFilterCtrl.value;
        if (!search) {
            this.filteredCustomers = this.allUsers.slice();
            return;
        } else {
            search = search.toLowerCase();
        }
        // filter the Dealer
        this.filteredCustomers = this.allUsers.filter(
            x => x.FirstName.toLowerCase().indexOf(search) > -1 ||
            x.LastName.toLowerCase().indexOf(search) > -1
        );
    }

    onChangeCustomers(){
        this.orderFilterData.UserID = this.customerCtrl.value;
    }

    private filterProducts() {
        // get the search keyword
        let search = this.productFilterCtrl.value;
        if(search.length < 2){
            return;
        }

        if (!search) {
            this.filteredProducts = this.allProducts.slice();
            return;
        } else {
            search = search.toLowerCase();
        }

        // filter the product
        this.filteredProducts = this.allProducts.filter(
            x => x.Name.toLowerCase().indexOf(search) > -1
        );
    }

    onChangeProducts(){
        this.orderFilterData.ProductID = this.productCtrl.value;
    }

    getAllUsers(){
        this.spinnerService.show();
        this.userService.getAllUsers().subscribe(data => {
            if(data['status']){
                this.allUsers = data['result'];
                this.filteredCustomers = data['result'];
            } else {
                this.snackBar.open(data['message'], 'OK', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
            }
            this.spinnerService.hide();
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('something went error', 'OK', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        })
    }

    getAllProducts(){
        this.spinnerService.show();
        this.productService.getAllProducts().subscribe(data => {
            if(data['status']){
                this.allProducts = data['result'];
            } else {
                this.snackBar.open(data['message'], 'OK', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
            }
            this.spinnerService.hide();
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('something went error', 'OK', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        })
    }

    clearOrderWise(){
        this.orderFilterData = {
            UserID: '0',
            ProductID: '0',
            FromDate: '',
            ToDate: '',
            RequestID: '',
            Status: '0',
            CustomerName: '',
            CustomerMobile: '',
            ServiceType: '0',
            ProductCode: ''
        };
        this.selectedOrder = [];
        this.dataSource = new MatTableDataSource(this.selectedOrder);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;

        this.customerCtrl.setValue('0');
        this.productCtrl.setValue('0');
    }

    filterOrderWise(){
        this.spinnerService.show();
        this.reportsService.getDataForTatReport(this.orderFilterData).subscribe(data => {
            if(data['status']){
                this.selectedOrder = data['result'];
                this.dataSource = new MatTableDataSource(this.selectedOrder);
                this.dataSource.paginator = this.paginator;
                this.dataSource.sort = this.sort;
                this.spinnerService.hide();
                if(this.selectedOrder.length == 0){
                    this.snackBar.open('Record not found', 'OK', {
                        duration: 5000,
                        panelClass: ['warning-snackbar'],
                        verticalPosition: 'top'
                    });
                }
            } else {
                this.snackBar.open(data['message'], 'OK', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
            }
            this.spinnerService.hide();
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('something went error', 'OK', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        })
    }

    exportOrderAsXLSX():void {
        this.excelService.exportAsExcelFile(this.selectedOrder, 'order');
    }
}