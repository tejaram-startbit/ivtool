import {Injectable} from '@angular/core';

import {HttpClient, HttpHeaders, HttpErrorResponse} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {map, catchError, tap} from 'rxjs/operators';

import {CommonService} from "@app/core/common/common.service";

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json'
    })
};

@Injectable({
    providedIn: 'root'
})
export class ReportService {
    url: string;
    DocUrl: string;

    constructor(private http: HttpClient, private commService: CommonService) {
        this.url = commService.getApiUrl() + '/report/';
    }

    private extractData(res: Response) {
        let body = res;
        return body || {};
    }

    getDataOrderWise(data): Observable<any> {
        return this.http.post<any>(this.url + 'getDataOrderWise', JSON.stringify(data), httpOptions).pipe(
            tap(() => console.log(`getDataOrderWise`)),
            catchError(this.handleError<any>('getDataOrderWise'))
        );
    }

    getDataProductWise(data): Observable<any> {
        return this.http.post<any>(this.url + 'getDataProductWise', JSON.stringify(data), httpOptions).pipe(
            tap(() => console.log(`getDataProductWise`)),
            catchError(this.handleError<any>('getDataProductWise'))
        );
    }

    getAllByTrendService(Type, Status): Observable<any> {
        var data = {
            Type: Type,
            Status: Status
        }
        return this.http.post<any>(this.url + 'getAllByTrendService', data, httpOptions).pipe(
            tap(() => console.log(`getAllByTrendService`)),
            catchError(this.handleError<any>('getAllByTrendService'))
        );
    }

    getAllOrdersBySortReport(): Observable<any> {
        return this.http.get(this.url + 'getAllOrdersBySortReport').pipe(
            map(this.extractData));
    }

    getDataFranchiseBuHeadWise(data): Observable<any> {
        return this.http.post<any>(this.url + 'getDataFranchiseBuHeadWise', JSON.stringify(data), httpOptions).pipe(
            tap(() => console.log(`getDataFranchiseBuHeadWise`)),
            catchError(this.handleError<any>('getDataFranchiseBuHeadWise'))
        );
    }

    getDataForTatReport(data): Observable<any> {
        return this.http.post<any>(this.url + 'getDataForTatReport', JSON.stringify(data), httpOptions).pipe(
            tap(() => console.log(`getDataForTatReport`)),
            catchError(this.handleError<any>('getDataForTatReport'))
        );
    }

    private handleError<T>(operation = 'operation', result?: any) {
        return (error: any): Observable<any> => {

            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // TODO: better job of transforming error for home consumption
            console.log(`${operation} failed: ${error.message}`);
            const errorData = {
                status: false,
                message: 'Server Error'
            };
            // Let the app keep running by returning an empty result.
            return of(errorData);
        };
    }
}
