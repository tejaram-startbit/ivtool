import {NgModule} from "@angular/core";

import {routing} from "@app/features/Report/report.routing";
import {SharedModule} from "@app/shared/shared.module";
import {SmartadminDatatableModule} from '@app/shared/ui/datatable/smartadmin-datatable.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {Ng4LoadingSpinnerModule} from "ng4-loading-spinner";

import {SmartadminInputModule} from '@app/shared/forms/input/smartadmin-input.module';
import {SmartadminWizardsModule} from "@app/shared/forms/wizards/smartadmin-wizards.module";
import {NgMultiSelectDropDownModule} from "ng-multiselect-dropdown";
import {MatStepperModule, MatIconModule} from "@angular/material";
import {MaterialModuleModule} from "@app/core/common/material-module/material-module.module";
import {SmartadminValidationModule} from "@app/shared/forms/validation/smartadmin-validation.module";
import {TreeviewModule} from 'ngx-treeview';
import {SmartadminEditorsModule} from "@app/shared/forms/editors/smartadmin-editors.module";

import {OrderReportComponent, OrderDetailDialogReport} from "@app/features/Report/Order/order-report.component";
import {TatReportComponent} from "@app/features/Report/Tat/tat-report.component";
import {ProductReportComponent} from "@app/features/Report/Product/product-report.component";
import {TrendServiceReportComponent} from "@app/features/Report/TrendService/trendservice-report.component";
import {SortOrderReportComponent} from "@app/features/Report/SortOrder/sortorder-report.component";
import {FranchiseBuHeadReportComponent, BuFranchiseDetailDialogReport} from "@app/features/Report/FranchiseBuHead/franchise-buhead-report.component";

import { MatSelectSearchModule } from "@app/core/common/material-module/mat-select-search/mat-select-search.module";
import {Ng5SliderModule} from "ng5-slider";
@NgModule({
    declarations: [
        OrderReportComponent,
        TatReportComponent,
        OrderDetailDialogReport,
        ProductReportComponent,
        TrendServiceReportComponent,
        SortOrderReportComponent,
        FranchiseBuHeadReportComponent,
        BuFranchiseDetailDialogReport
    ],
    imports: [
        SharedModule,
        routing,
        SmartadminDatatableModule,
        FormsModule,
        ReactiveFormsModule,
        SmartadminInputModule,
        SmartadminWizardsModule,
        NgMultiSelectDropDownModule.forRoot(),
        MaterialModuleModule,
        SmartadminValidationModule,
        TreeviewModule.forRoot(),
        SmartadminEditorsModule,
        Ng4LoadingSpinnerModule.forRoot(),
        MatSelectSearchModule,
        Ng5SliderModule
    ],
    providers: [],
    exports: [
        FormsModule,
        ReactiveFormsModule,
        MatStepperModule,
        MatIconModule
    ],
    entryComponents: [
        OrderDetailDialogReport,
        BuFranchiseDetailDialogReport
    ]
})
export class ReportModule {

}
