import { takeUntil } from 'rxjs/operators';
import { FormControl } from '@angular/forms';
import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {NotificationService} from '@app/core/services';
import {ReportService} from "@app/features/Report/report.service";
import {Observable} from "rxjs";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {MatSnackBar} from "@angular/material";
import {CommonService} from "@app/core/common/common.service";
import {Ng4LoadingSpinnerService} from "ng4-loading-spinner";
import {UserService} from "@app/features/User/user.service";
import {ProductService} from "@app/features/Product/product.service";
import {OrderService} from "@app/features/Order/order.service";
import {ExcelService} from "@app/core/services/excel/excel.service";

import {ReplaySubject, Subject} from "rxjs";

declare var $: any;

@Component({
    selector: 'project-product-report',
    templateUrl: './product-report.component.html',
})
export class ProductReportComponent implements OnInit {
    Reports: any;
    displayedColumnsProduct: string[] = [
        'SN',
        'ProductCode',
        'Name',
        'MisFranchise',
        'MisBuHead',
        'Quantity',
        // 'Action'
    ];
    dataSourceProduct: MatTableDataSource<any>;
    @ViewChild(MatPaginator) paginatorProduct: MatPaginator;
    @ViewChild(MatSort) sortProduct: MatSort;

    apiUrl: string;
    currentUser: any;

    productFilterData = {
        UserID: '0',
        ProductID: '0',
        FromDate: '',
        ToDate: '',
        RequestID: '',
        Status: '0',
        CustomerName: '',
        CustomerMobile: '',
        ServiceType: '0',
        ProductCode: '',
        MisFranchise: '0',
        MisBuHead: '0'
    };
    allUsers: any = [];
    allProducts: any = [];
    selectedProduct: any = [];
    allOrderStatus: any = [
        {value: 1, name: 'Pending'},
        {value: 2, name: 'Approved'},
        {value: 3, name: 'Disapproved'},
        {value: 4, name: 'Dispatched'},
        {value: 5, name: 'Delivered'},
        {value: 6, name: 'Acknowledged'},
        {value: 7, name: 'Return request'},
        // {value: 8, name: 'Transfered'},
        {value: 9, name: 'Returned'}
    ];

    public customerCtrl: FormControl = new FormControl('0');
    public customerFilterCtrl: FormControl = new FormControl();
    public filteredCustomers: any = [];

    public productCtrl: FormControl = new FormControl('0');
    public productFilterCtrl: FormControl = new FormControl();
    public filteredProducts: any = [];

    public franchiseCtrl: FormControl = new FormControl('0');
    public franchiseFilterCtrl: FormControl = new FormControl();
    public filteredFranchises: any = [];

    public buHeadCtrl: FormControl = new FormControl('0');
    public buHeadFilterCtrl: FormControl = new FormControl();
    public filteredBuHead: any = [];

    allFranchise: any = [];
    allBuHead: any = [];

    constructor(private http: HttpClient,
                private notificationService: NotificationService,
                private reportsService: ReportService,
                private snackBar: MatSnackBar,
                private commonService: CommonService,
                private userService: UserService,
                private productService: ProductService,
                public dialog: MatDialog,
                private spinnerService: Ng4LoadingSpinnerService,
                private excelService:ExcelService
    ) {
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.apiUrl = this.commonService.getApiUrl();

        this.getAllUsers();
        this.getAllProducts();
        this.getAllApprover();
    }

    private _onDestroy = new Subject<void>();

    ngOnInit() {
        this.customerFilterCtrl.valueChanges
        .pipe(takeUntil(this._onDestroy))
        .subscribe(() => {
            this.filterCustomers();
        });

        this.productFilterCtrl.valueChanges
        .pipe(takeUntil(this._onDestroy))
        .subscribe(() => {
            this.filterProducts();
        });

        this.franchiseFilterCtrl.valueChanges
        .pipe(takeUntil(this._onDestroy))
        .subscribe(() => {
            this.filterFranchise();
        });

        this.buHeadFilterCtrl.valueChanges
        .pipe(takeUntil(this._onDestroy))
        .subscribe(() => {
            this.filterBuHead();
        });
    }

    private filterCustomers() {
        // get the search keyword
        let search = this.customerFilterCtrl.value;
        if (!search) {
            this.filteredCustomers = this.allUsers.slice();
            return;
        } else {
            search = search.toLowerCase();
        }
        // filter the Dealer
        this.filteredCustomers = this.allUsers.filter(
            x => x.FirstName.toLowerCase().indexOf(search) > -1 ||
            x.LastName.toLowerCase().indexOf(search) > -1
        );
    }

    onChangeCustomers(){
        this.productFilterData.UserID = this.customerCtrl.value;
    }

    private filterProducts() {
        // get the search keyword
        let search = this.productFilterCtrl.value;
        if(search.length < 2){
            return;
        }

        if (!search) {
            this.filteredProducts = this.allProducts.slice();
            return;
        } else {
            search = search.toLowerCase();
        }

        // filter the product
        this.filteredProducts = this.allProducts.filter(
            x => x.Name.toLowerCase().indexOf(search) > -1
        );
    }

    onChangeProducts(){
        this.productFilterData.ProductID = this.productCtrl.value;
    }

    private filterFranchise() {
        // get the search keyword
        let search = this.franchiseFilterCtrl.value;
        if (!search) {
            this.filteredFranchises = this.allFranchise.slice();
            return;
        } else {
            search = search.toLowerCase();
        }
        // filter the Dealer
        this.filteredFranchises = this.allFranchise.filter(
            x => x.Name.toLowerCase().indexOf(search) > -1
        );
    }

    onChangeFranchise(){
        this.productFilterData.MisFranchise = this.franchiseCtrl.value;
    }

    private filterBuHead() {
        // get the search keyword
        let search = this.buHeadFilterCtrl.value;
        if (!search) {
            this.filteredBuHead = this.allBuHead.slice();
            return;
        } else {
            search = search.toLowerCase();
        }
        // filter the Dealer
        this.filteredBuHead = this.allBuHead.filter(
            x => x.Name.toLowerCase().indexOf(search) > -1
        );
    }

    onChangeBuHead(){
        this.productFilterData.MisBuHead = this.buHeadCtrl.value;
    }

    getAllUsers(){
        this.spinnerService.show();
        this.userService.getAllUsers().subscribe(data => {
            if(data['status']){
                this.allUsers = data['result'];
            } else {
                this.snackBar.open(data['message'], 'OK', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
            }
            this.spinnerService.hide();
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('something went error', 'OK', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        })
    }

    getAllProducts(){
        this.spinnerService.show();
        this.productService.getAllProducts().subscribe(data => {
            if(data['status']){
                this.allProducts = data['result'];
            } else {
                this.snackBar.open(data['message'], 'OK', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
            }
            this.spinnerService.hide();
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('something went error', 'OK', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        })
    }

    getAllApprover(){
        this.spinnerService.show();
        this.userService.getAllApprover().subscribe(data => {
            if(data['status']){
                const map = new Map();
                data['result'].map(item => {
                    if(item.MisFranchise && !map.has(item.MisFranchise)){
                        map.set(item.MisFranchise, true);    // set any value to Map
                        this.allFranchise.push({
                            Name: item.MisFranchise
                        });
                    }
                    if(item.MisBUGroup && !map.has(item.MisBUGroup)){
                        map.set(item.MisBUGroup, true);    // set any value to Map
                        this.allBuHead.push({
                            Name: item.MisBUGroup
                        });
                    }
                });
                this.filteredFranchises = this.allFranchise;
                this.filteredBuHead = this.allBuHead;
            } else {
                this.snackBar.open(data['message'], 'OK', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
            }
            this.spinnerService.hide();
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('something went error', 'OK', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        })
    }

    clearProductWise(){
        this.productFilterData = {
            UserID: '0',
            ProductID: '0',
            FromDate: '',
            ToDate: '',
            RequestID: '',
            Status: '0',
            CustomerName: '',
            CustomerMobile: '',
            ServiceType: '0',
            ProductCode: '',
            MisFranchise: '0',
            MisBuHead: '0'
        };
        this.selectedProduct = [];
        this.dataSourceProduct = new MatTableDataSource(this.selectedProduct);
        this.dataSourceProduct.paginator = this.paginatorProduct;
        this.dataSourceProduct.sort = this.sortProduct;

        this.customerCtrl.setValue('0');
        this.productCtrl.setValue('0');
        this.franchiseCtrl.setValue('0');
        this.buHeadCtrl.setValue('0');
    }

    filterProductWise(){
        this.spinnerService.show();
        this.reportsService.getDataProductWise(this.productFilterData).subscribe(data => {
            if(data['status']){
                this.selectedProduct = data['result'];
                this.dataSourceProduct = new MatTableDataSource(this.selectedProduct);
                this.dataSourceProduct.paginator = this.paginatorProduct;
                this.dataSourceProduct.sort = this.sortProduct;
                this.spinnerService.hide();
                if(this.selectedProduct.length == 0){
                    this.snackBar.open('Record not found', 'OK', {
                        duration: 5000,
                        panelClass: ['warning-snackbar'],
                        verticalPosition: 'top'
                    });
                }
            } else {
                this.snackBar.open(data['message'], 'OK', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
            }
            this.spinnerService.hide();
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('something went error', 'OK', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        })
    }

    exportProductAsXLSX():void {
        this.excelService.exportAsExcelFile(this.selectedProduct, 'product');
    }

    getTotalQuantity(){
        const total = this.selectedProduct.reduce((prev,next) => prev + next.Quantity,0);
        return total;
    }
}