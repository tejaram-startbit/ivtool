import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {NotificationService} from '@app/core/services';
import {ReportService} from "@app/features/Report/report.service";
import {Observable} from "rxjs";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {MatSnackBar} from "@angular/material";
import {CommonService} from "@app/core/common/common.service";
import {Ng4LoadingSpinnerService} from "ng4-loading-spinner";
import {UserService} from "@app/features/User/user.service";
import {ProductService} from "@app/features/Product/product.service";
import {OrderService} from "@app/features/Order/order.service";
import {ExcelService} from "@app/core/services/excel/excel.service";
import { Options, LabelType } from 'ng5-slider';

declare var $: any;

@Component({
    selector: 'sort-order-report',
    templateUrl: './sortorder-report.component.html',
})
export class SortOrderReportComponent implements OnInit {
    allStatus: any = ['', 'Pending', 'Approved', 'Disapproved', 'Dispatched', 'Delivered', 'Acknowledged', 'Return request', 'Transfer', 'Returned', 'Declined'];
    displayedColumnsProduct: string[] = [
        'SN',
        'RequestID',
        'SalesPerson',
        'SalesPersonContact',
        'Status',
        'OrderCreated',
        'ProductQuantity',
        'TotalAmount'
    ];
    dataSourceProduct: MatTableDataSource<any>;
    @ViewChild(MatPaginator) paginatorProduct: MatPaginator;
    @ViewChild(MatSort) sortProduct: MatSort;

    apiUrl: string;
    currentUser: any;

    allReportData: any = [];
    Type: string = 'Quantity';

    minQuantity: number = 1;
    maxQuantity: number = 0;
    QuantityRangeSliderOptions: Options = {
        
    };

    minAmount: number = 0;
    maxAmount: number = 100000;
    AmountRangeSliderOptions: Options = {
        
    };

    showQuantitySlider: boolean = false;
    showAmountSlider: boolean = false;
    constructor(private http: HttpClient,
                private notificationService: NotificationService,
                private reportsService: ReportService,
                private snackBar: MatSnackBar,
                private commonService: CommonService,
                private userService: UserService,
                private orderService: OrderService,
                private productService: ProductService,
                public dialog: MatDialog,
                private spinnerService: Ng4LoadingSpinnerService,
                private excelService:ExcelService
    ) {
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.apiUrl = this.commonService.getApiUrl();

        this.getAllOrdersBySortReport();
    }

    ngOnInit() {
    }

    getAllOrdersBySortReport(){
        this.spinnerService.show();
        this.reportsService.getAllOrdersBySortReport().subscribe(data => {
            if(data['status']){
                this.allReportData = data['result'];
                var maxQuantityTemp =  Math.max.apply(Math, this.allReportData.map(function(o) { return o.Products.length; }));
                var maxAmountTemp =  Math.max.apply(Math, this.allReportData.map(function(o) { return o.TotalAmount; }));
                
                this.AmountRangeSliderOptions = {
                    floor: 0,
                    ceil: Math.ceil(maxAmountTemp/100000)*100000,
                    step: 25000,
                    minRange: 25000,
                    showTicks: true,
                    showTicksValues: false,
                    translate: (value: number, label: LabelType): string => {
                        switch (label) {
                            case LabelType.Low:
                                return '<b>₹</b>' + value;
                            case LabelType.High:
                                return '<b>₹</b>' + value;
                            default:
                                return '' + value;
                        }
                    }
                }
                this.QuantityRangeSliderOptions = {
                    floor: 0,
                    ceil: Math.ceil(maxQuantityTemp/10)*10,
                    step: 1,
                    showTicks: true,
                    showTicksValues: false,
                    translate: (value: number, label: LabelType): string => {
                        switch (label) {
                            case LabelType.Low:
                                return '<b>Min:</b>' + value;
                            case LabelType.High:
                                return '<b>Max:</b>' + value;
                            default:
                                return '' + value;
                        }
                    }
                }

                this.maxQuantity = maxQuantityTemp;
                this.maxAmount = maxAmountTemp;
                this.showQuantitySlider = true;
                this.showAmountSlider = true;
                this.sortOrder();
            } else {
                this.snackBar.open(data['message'], 'OK', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
            }
            this.spinnerService.hide();
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('something went error', 'OK', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        })
    }

    sortOrder(){
        if(this.Type == 'Quantity') {
            this.allReportData.sort(function(a, b){
                return b.Products.length-a.Products.length
            })
        } else {
            this.allReportData.sort(function(a, b){
                return b.TotalAmount-a.TotalAmount
            })
        }

        this.dataSourceProduct = new MatTableDataSource(this.allReportData.filter(x => x.Products.length >= this.minQuantity && x.Products.length <= this.maxQuantity && x.TotalAmount >= this.minAmount && x.TotalAmount <= this.maxAmount));
        this.dataSourceProduct.paginator = this.paginatorProduct;
        // this.dataSourceProduct.sort = this.sortProduct;
    }

    onChangeQuantitySlider(){
        console.log(this.minQuantity, this.maxQuantity)
    }


    exportProductAsXLSX():void {
        this.excelService.exportAsExcelFile(this.allReportData, 'product');
    }

    public getTotalQuantity() {
        if(this.dataSourceProduct)
            return this.dataSourceProduct.data.reduce((accum, curr) => accum + curr.Products.length, 0);
        else
            return 0;
      }

      public getTotalAmount() {
        if(this.dataSourceProduct)
            return this.dataSourceProduct.data.reduce((accum, curr) => accum + curr.TotalAmount, 0);
        else
            return 0;
      }

}