import {RouterModule, Routes} from "@angular/router";
import {ProductReportComponent} from "@app/features/Report/Product/product-report.component";
import {OrderReportComponent} from "@app/features/Report/Order/order-report.component";
import {TatReportComponent} from "@app/features/Report/Tat/tat-report.component";
import {TrendServiceReportComponent} from "@app/features/Report/TrendService/trendservice-report.component";
import {SortOrderReportComponent} from "@app/features/Report/SortOrder/sortorder-report.component";
import {FranchiseBuHeadReportComponent} from "@app/features/Report/FranchiseBuHead/franchise-buhead-report.component";

export const routes: Routes = [

    {
        path: '',
        redirectTo: 'orders'
    },
    {
        path: 'orders',
        component: OrderReportComponent
    },
    {
        path: 'products',
        component: ProductReportComponent
    },
    {
        path: 'trend-service',
        component: TrendServiceReportComponent
    },
    {
        path: 'sort-order',
        component: SortOrderReportComponent
    },
    {
        path: 'franchise-buhead',
        component: FranchiseBuHeadReportComponent
    },
    {
        path: 'tat',
        component: TatReportComponent
    }
];

export const routing = RouterModule.forChild(routes);
