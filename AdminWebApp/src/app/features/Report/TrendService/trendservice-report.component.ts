import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {NotificationService} from '@app/core/services';
import {ReportService} from "@app/features/Report/report.service";
import {Observable} from "rxjs";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {MatSnackBar} from "@angular/material";
import {CommonService} from "@app/core/common/common.service";
import {Ng4LoadingSpinnerService} from "ng4-loading-spinner";
import {UserService} from "@app/features/User/user.service";
import {ProductService} from "@app/features/Product/product.service";
import {OrderService} from "@app/features/Order/order.service";
import {ExcelService} from "@app/core/services/excel/excel.service";

declare var $: any;

@Component({
    selector: 'trendservice-product-report',
    templateUrl: './trendservice-report.component.html',
})
export class TrendServiceReportComponent implements OnInit {
    displayedColumnsProduct: string[] = [
        'SN',
        'RequestID',
        'SalesPerson',
        'SalesPersonContact',
        'OrderCreated',
        'ProductQuantity',
        'TotalAmount'
    ];
    dataSourceProduct: MatTableDataSource<any>;
    @ViewChild(MatPaginator) paginatorProduct: MatPaginator;
    @ViewChild(MatSort) sortProduct: MatSort;

    apiUrl: string;
    currentUser: any;

    allReportData: any = [];
    Type: string = 'Y';
    Status: string = '4';
    constructor(private http: HttpClient,
                private notificationService: NotificationService,
                private reportsService: ReportService,
                private snackBar: MatSnackBar,
                private commonService: CommonService,
                private userService: UserService,
                private productService: ProductService,
                public dialog: MatDialog,
                private spinnerService: Ng4LoadingSpinnerService,
                private excelService:ExcelService
    ) {
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.apiUrl = this.commonService.getApiUrl();

        this.getAllByTrendService();
    }

    ngOnInit() {
    }

    getAllByTrendService(){
        this.spinnerService.show();
        this.reportsService.getAllByTrendService(this.Type, this.Status).subscribe(data => {
            if(data['status']){
                this.allReportData = data['result']
            
                this.dataSourceProduct = new MatTableDataSource(this.allReportData);
                this.dataSourceProduct.paginator = this.paginatorProduct;
                this.dataSourceProduct.sort = this.sortProduct;
            } else {
                this.snackBar.open(data['message'], 'OK', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
            }
            this.spinnerService.hide();
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('something went error', 'OK', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        })
    }


    exportProductAsXLSX():void {
        this.excelService.exportAsExcelFile(this.allReportData, 'product');
    }

    public getTotalQuantity() {
        return this.allReportData.reduce((accum, curr) => accum + curr.Products.length, 0);
      }

      public getTotalAmount() {
        return this.allReportData.reduce((accum, curr) => accum + curr.TotalAmount, 0);
      }

}