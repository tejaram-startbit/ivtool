import {RouterModule, Routes} from "@angular/router";
import {OrderRequestComponent} from "@app/features/OrderRequest/order-request.component";

export const routes: Routes = [
    {
        path: '',
        component: OrderRequestComponent
    },
];

export const routing = RouterModule.forChild(routes);
