import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {NotificationService} from '@app/core/services';
import {OrderService} from "@app/features/Order/order.service";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {MatSnackBar} from "@angular/material";
import {CommonService} from "@app/core/common/common.service";
import {Ng4LoadingSpinnerService} from "ng4-loading-spinner";
import {ActivatedRoute} from "@angular/router";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

declare var $: any;

@Component({
    selector: 'project-order-request',
    templateUrl: './order-request.component.html',
})

export class OrderRequestComponent implements OnInit {
    allOrders: any = [];
    selectedOrders: any = [];
    apiUrl: string;
    ApproverID: any;
    OrderID: any;
    Token: any;
    orderDetail: any;
    allStatus: any = ['', 'Pending', 'Approved', 'Disapproved', 'Dispatched', 'Delivered', 'Acknowledged', 'Return request', 'Transfer', 'Returned'];
    showThankYouMessage = false;
    linkVisible = false;
    userMessage: string = '';
    isUserAuthenticated: boolean = false;

    authenticationForm: FormGroup;
    error: string;
    constructor(private http: HttpClient,
                private notificationService: NotificationService,
                private ordersService: OrderService,
                private snackBar: MatSnackBar,
                private commonService: CommonService,
                private route: ActivatedRoute,
                private spinnerService: Ng4LoadingSpinnerService,
                private orderService: OrderService,
                private formBuilder: FormBuilder,
                public dialog: MatDialog) {
        this.apiUrl = this.commonService.getApiUrl();

        this.authenticationForm = this.formBuilder.group({
            UserName: ['', Validators.required],
            Password: ['', Validators.required]
        });

        this.ApproverID = this.route.params['value'].UserID;
        this.OrderID = this.route.params['value'].OrderID;
        this.Token = this.route.params['value'].Token;
        this.getOrderDetail(this.OrderID)
    }

    ngOnInit() {

    }

    getOrderDetail(OrderID){
        this.orderService.getOneOrder(OrderID).subscribe(data => {
            if (data['status']) {
                this.orderDetail = data['result'];
                if(this.orderDetail && this.orderDetail.RequestID == this.Token) {
                    if(this.orderDetail.Status == 1){
                        this.linkVisible = true;
                    } else {
                        this.userMessage = 'This order is already picked by another cost approver';
                    }
                } else {
                    this.userMessage = 'Invalid link';
                    // this.linkVisible = true;
                }
            }
        }, err => {
            this.snackBar.open('something went error', 'OK', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        })
    }

    openDialog(): void {
        let lab = 'Do you want to disapprove this order request?';
        this.notificationService.smartMessageBox({
            title: "Disapprove!",
            content: lab,
            buttons: '[No][Yes]'
        }, (ButtonPressed) => {
            if (ButtonPressed === "Yes") {
                const dialogRef = this.dialog.open(OrderDisapproveDialog, {
                    width: '99%',
                    data: {
                        OrderID: this.orderDetail._id,
                        ApproverID: this.ApproverID
                    }
                });

                dialogRef.afterClosed().subscribe(result => {
                    if(result){
                        this.showThankYouMessage = true;
                        this.userMessage = 'Thank you for the update order request, We will be notify to our Team';
                    }
                });
            }
            if (ButtonPressed === "No") {
                this.spinnerService.hide();
            }
        });

    }

    approveOrder(): void {
        let lab = 'Are you sure to approve this order request?';
        this.notificationService.smartMessageBox({
            title: "Approve!",
            content: lab,
            buttons: '[No][Yes]'
        }, (ButtonPressed) => {
            if (ButtonPressed === "Yes") {
                this.spinnerService.show();
                this.ordersService.approveOrder(this.OrderID, this.ApproverID).subscribe(data => {
                    this.spinnerService.hide();
                    if(data['status']){
                        this.snackBar.open('Order Approved successfully', 'OK', {
                            duration: 5000,
                            panelClass: ['success-snackbar'],
                            verticalPosition: 'top'
                        });
                        this.userMessage = 'Thank you for the update order request, We will be notify to our Team';
                        this.showThankYouMessage = true;
                    } else {
                        this.snackBar.open(data['message'], 'OK', {
                            duration: 5000,
                            panelClass: ['danger-snackbar'],
                            verticalPosition: 'top'
                        });
                    }
                }, err => {
                    this.spinnerService.hide();
                    this.snackBar.open('something went error', 'OK', {
                        duration: 5000,
                        panelClass: ['danger-snackbar'],
                        verticalPosition: 'top'
                    });
                })
            }
            if (ButtonPressed === "No") {
                this.spinnerService.hide();
            }
        });
    }

    authentication(event) {
        event.preventDefault();
        if (this.authenticationForm.value.UserName == '' && this.authenticationForm.value.Password == '') {
            this.error = 'Please fill email and password fields';
            return;
        }
        if (this.authenticationForm.value.UserName == '') {
            this.error = 'Please fill email field';
            return;
        }
        if (this.authenticationForm.value.Password == '') {
            this.error = 'Please fill password field';
            return;
        }
        if (this.authenticationForm.invalid) {
            return;
        }
        this.error = '';
        this.ordersService.authenticationLogin(this.authenticationForm.value.UserName, this.authenticationForm.value.Password)
            .pipe()
            .subscribe(
                data => {
                    if (data['status']) {
                        this.isUserAuthenticated = true;
                    } else {
                        this.error = data['message'];
                    }
                },
                error => {
                    this.error = 'Invalid username and password';
                });
    }
}


@Component({
    selector: 'order-disapprove-dialog',
    templateUrl: 'order-disapprove-dialog.html',
})
export class OrderDisapproveDialog {
    apiUrl: string;
    OrderID: any;
    ReasonMessage: string;
    ApproverID: any;
    constructor(public commonService: CommonService,
                public orderService: OrderService,
                public spinnerService: Ng4LoadingSpinnerService,
                private snackBar: MatSnackBar,
                private notificationService: NotificationService,
                public dialogRef: MatDialogRef<OrderDisapproveDialog>,
                @Inject(MAT_DIALOG_DATA) public data: any) {
        this.apiUrl = this.commonService.getApiUrl();
        this.OrderID = data.OrderID;
        this.ApproverID = data.ApproverID;
    }

    onNoClick(): void {
        this.dialogRef.close(false);
    }

    onSubmit() {
        if(!this.ReasonMessage) {
            this.snackBar.open('Please enter reason for disapprove this order request', 'OK', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
            return 0;
        }
        this.spinnerService.show();
        this.orderService.disapproveOrder(this.OrderID, this.ApproverID, this.ReasonMessage).subscribe(data => {
            this.spinnerService.hide();
            if(data['status']){
                this.snackBar.open('Order Disapproved successfully', 'OK', {
                    duration: 5000,
                    panelClass: ['success-snackbar'],
                    verticalPosition: 'top'
                });
                this.dialogRef.close(true);
            } else {
                this.snackBar.open(data['message'], 'OK', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
            }
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('something went error', 'OK', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        })
    }

}