import {RouterModule, Routes} from "@angular/router";
import {CostLevelComponent} from "@app/features/CostLevel/costlevel.component";
import {CostLevelFormComponent} from "@app/features/CostLevel/costlevel-form.component";

export const routes: Routes = [

    {
        path: '',
        redirectTo: 'list'
    },
    {
        path: 'list',
        component: CostLevelComponent
    },
    {
        path: 'form/:id',
        component: CostLevelFormComponent
    }
];

export const routing = RouterModule.forChild(routes);
