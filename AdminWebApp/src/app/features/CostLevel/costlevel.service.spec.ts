import {TestBed} from '@angular/core/testing';

import {CostLevelService} from "@app/features/CostLevel/costlevel.service";

describe('CostLevelService', () => {
    beforeEach(() => TestBed.configureTestingModule({}));

    it('should be created', () => {
        const service: CostLevelService = TestBed.get(CostLevelService);
        expect(service).toBeTruthy();
    });
});
