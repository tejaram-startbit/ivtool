import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {NotificationService} from '@app/core/services';
import {MatSnackBar} from "@angular/material";
import {CostLevelService} from "@app/features/CostLevel/costlevel.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {Location} from "@angular/common";
import {Observable} from "rxjs";
import {Ng4LoadingSpinnerService} from "ng4-loading-spinner";


@Component({
    selector: 'project-costlevel-form',
    templateUrl: './costlevel-form.component.html',
})
export class CostLevelFormComponent implements OnInit {
    ID: any;
    name: any;
    States: any;
    CostLevelForm: FormGroup;
    showSpinner: boolean = false;
    ImageBase64Data: any;
    showForm: boolean = false;
    validationCostLevelFormDetailOptions = {};

    constructor(private http: HttpClient,
                private router: Router,
                private fb: FormBuilder,
                private location: Location,
                private route: ActivatedRoute,
                private costlevelService: CostLevelService,
                private notificationService: NotificationService,
                private snackBar: MatSnackBar,
                private spinnerService: Ng4LoadingSpinnerService) {

        this.CostLevelForm = fb.group({
            MinRange: ['', [Validators.required]],
            MaxRange: ['', [Validators.required]],
            Description: ['']
        });

        this.validationCostLevelFormDetailOptions = {
            // Rules for form validation
            rules: {
                MinRange: {
                    required: true
                },
                MaxRange: {
                    required: true
                }
            },

            // Messages for form validation
            messages: {
                MinRange: {
                    required: 'Please enter your costlevel min range',
                    max: 'please enter less then max range'
                },
                MaxRange: {
                    required: 'Please enter your costlevel max range'
                }
            },
            submitHandler: this.onSubmit

        };
        this.ID = this.route.params['value'].id;
        if (this.ID !== '-1') {
            this.getOne(this.ID);
        }
        this.showForm = true;
    }

    onSubmit() {
        console.log('\n', 'submit handler for validated form', '\n\n')
    }

    ngOnInit() {

    }

    onSubmitForm() {
        if (this.CostLevelForm.invalid) {
            return 0;
        }
        this.CostLevelForm.value.Avatar = this.ImageBase64Data;
        if (this.ID === '-1') {
            this.createNew();
        } else {
            this.editExisting();
        }
    }


    createNew() {
        this.showSpinner = true;
        this.costlevelService.addCostLevel(this.CostLevelForm.value).subscribe((data: {}) => {
            if (data['status']) {
                this.snackBar.open('Record created successfully', 'success', {
                    duration: 5000,
                    panelClass: ['success-snackbar'],
                    verticalPosition: 'top'
                });
                this.location.back();
            }
        }, err => {

        });

    }

    editExisting() {
        this.notificationService.smartMessageBox({
            title: "Update!",
            content: "Are you sure update this record?",
            buttons: '[No][Yes]'
        }, (ButtonPressed) => {
            if (ButtonPressed === "Yes") {
                this.showSpinner = true;
                this.costlevelService.updateCostLevel(this.ID, this.CostLevelForm.value).subscribe((data: {}) => {
                    if (data['status']) {
                        this.snackBar.open('Record updated successfully', 'success', {
                            duration: 5000,
                            panelClass: ['success-snackbar'],
                            verticalPosition: 'top'
                        });
                        this.location.back();
                    }
                }, err => {

                });
            }
            if (ButtonPressed === "No") {

            }

        });

    }

    getOne(id) {
        this.spinnerService.show();
        this.costlevelService.getOneCostLevel(id).subscribe((data: {}) => {
            if (data['status']) {
                this.CostLevelForm = this.fb.group({
                    MinRange: [data['result'].MinRange, [Validators.required]],
                    MaxRange: [data['result'].MaxRange, [Validators.required]],
                    Description: [data['result'].Description]
                });
            }
            this.spinnerService.hide();
        }, err => {

        });
    }

    register() {
        this.router.navigate(['/costlevel/list']);
    }

    cancel() {
        this.location.back();
    }
}