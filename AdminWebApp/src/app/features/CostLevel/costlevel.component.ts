import {Component, OnInit, ViewChild} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {NotificationService} from '@app/core/services';
import {CostLevelService} from "@app/features/CostLevel/costlevel.service";
import {Observable} from "rxjs";
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {MatSnackBar} from "@angular/material";
import {CommonService} from "@app/core/common/common.service";
import {Ng4LoadingSpinnerService} from "ng4-loading-spinner";

declare var $: any;

@Component({
    selector: 'project-costlevel',
    templateUrl: './costlevel.component.html',
})
export class CostLevelComponent implements OnInit {
    CostLevels: any;
    displayedColumns: string[] = [
        'SN',
        'MinRange',
        'MaxRange',
        'Description',
        'Action'
    ];
    dataSource: MatTableDataSource<any>;

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    apiUrl: string;

    constructor(private http: HttpClient,
                private notificationService: NotificationService,
                private costlevelsService: CostLevelService,
                private snackBar: MatSnackBar,
                private commonService: CommonService,
                private spinnerService: Ng4LoadingSpinnerService) {
        this.apiUrl = this.commonService.getApiUrl();
        this.getAllCostLevels();
    }

    ngOnInit() {
    }


    applyFilter(filterValue: string) {
        this.dataSource.filter = filterValue.trim().toLowerCase();

        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    }

    getAllCostLevels() {
        this.spinnerService.show();
        this.costlevelsService.getAllCostLevels().subscribe((data: {}) => {
            this.CostLevels = data['result'];
            this.dataSource = new MatTableDataSource(data['result']);
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
            this.spinnerService.hide();
        });
    }


    delete(id) {

        let lab = 'Are you sure delete this record?';
        this.notificationService.smartMessageBox({
            title: "Delete!",
            content: lab,
            buttons: '[No][Yes]'
        }, (ButtonPressed) => {
            if (ButtonPressed === "Yes") {
                this.costlevelsService.deleteCostLevel(id).subscribe((data: {}) => {
                    if (data['status']) {
                        this.snackBar.open('Record deleted successfully', 'Delete', {
                            duration: 5000,
                            panelClass: ['danger-snackbar'],
                            verticalPosition: 'top'
                        });
                        this.getAllCostLevels();
                    }
                })
            }
            if (ButtonPressed === "No") {

            }
        });
    }
}