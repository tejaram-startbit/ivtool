import {NgModule} from '@angular/core';
import {Ng4LoadingSpinnerModule} from "ng4-loading-spinner";

import {routing} from './dashboard.routing';
import {SharedModule} from '@app/shared/shared.module';


@NgModule({
    imports: [
        SharedModule,
        routing,
        Ng4LoadingSpinnerModule.forRoot()
    ],
    declarations: [],
    providers: [],
})
export class DashboardModule {

}
