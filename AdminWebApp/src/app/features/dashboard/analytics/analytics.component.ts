import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {Store} from '@ngrx/store';

import * as fromCalendar from "@app/core/store/calendar";
import {OrderService} from "@app/features/Order/order.service";
import {Ng4LoadingSpinnerService} from "ng4-loading-spinner";
import {
    MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatPaginator, MatSnackBar, MatSort,
    MatTableDataSource
} from "@angular/material";
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import {Label, SingleDataSet} from 'ng2-charts';
import {CommonService} from "@app/core/common/common.service";
import {NotificationService} from "@app/core/services";
import {ProductService} from "@app/features/Product/product.service";
import {Router} from "@angular/router";

@Component({
    selector: 'sa-analytics',
    templateUrl: './analytics.component.html',
})
export class AnalyticsComponent implements OnInit {
    //bar chart
    public barChartOptions: ChartOptions = {
        responsive: true,
    };
    public barChartLabels: Label[] = [];
    public barChartType: ChartType = 'bar';
    public barChartLegend = true;
    public barChartPlugins = [];

    // Pie chart
    public pieChartOptions: ChartOptions = {
        responsive: true,
    };
    public pieChartLabels: Label[] = ['Pending', 'Approved', 'Disapproved', 'Dispatched', 'Delivered', 'Acknowledge'];
    public pieChartData: SingleDataSet = [];
    public pieChartType: ChartType = 'pie';
    public pieChartLegend = true;
    public pieChartPlugins = [];
    showPieChart: boolean = false;


    public calendar$
    allOrders: any = [];
    selectedOrders: any = [];
    selectedOrderStatus = 1;
    showOrderDetailGraph: boolean = false;
    monthArr = ['', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    orderDetailGraph: ChartDataSets[];
    currentUser: any;

    displayedColumns: string[] = [
        'SN',
        'RequestID',
        'ServiceType',
        'Status',
        'Action'
    ];
    dataSource: MatTableDataSource<any>;
    isLoadingResults: any;
    isRateLimitReached: any;

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    allStatus: any = ['', 'Pending', 'Approved', 'Disapproved', 'Dispatched', 'Delivered', 'Acknowledged', 'Return request', 'Transfer', 'Returned', 'Declined'];
    allProductsLength: Number = 0;
    allNotifications: any = [];
    allOrderStatusByRole: any = [
        {value: 1, name: 'Pending'},
        {value: 2, name: 'Approved'},
        {value: 3, name: 'Disapproved'},
        {value: 4, name: 'Dispatched'},
        {value: 5, name: 'Delivered'},
        {value: 6, name: 'Acknowledged'},
        {value: 7, name: 'Returne request'},
        {value: 9, name: 'Returned'},
        {value: 10, name: 'Declined'}
    ];
    constructor(
        private store: Store<any>,
        public spinnerService: Ng4LoadingSpinnerService,
        private snackBar: MatSnackBar,
        private orderService: OrderService,
        private productService: ProductService,
        private notificationService: NotificationService,
        private router: Router,
        public dialog: MatDialog
    ) {
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if(this.currentUser && this.currentUser.RoleID.Description == 'OM Supervisory'){
            this.allOrderStatusByRole = [
                {value: 2, name: 'Approved'},
                {value: 3, name: 'Disapproved'},
                {value: 4, name: 'Dispatched'},
                {value: 5, name: 'Delivered'},
                {value: 6, name: 'Acknowledged'},
                {value: 7, name: 'Returne request'},
                {value: 9, name: 'Returned'},
                {value: 10, name: 'Declined'}
            ];
            this.selectedOrderStatus = 2;
        } else if(this.currentUser && this.currentUser.RoleID.Description == 'Approver'){
            this.allOrderStatusByRole = [
                {value: 1, name: 'Pending'},
                {value: 2, name: 'Approved'},
                {value: 3, name: 'Disapproved'}
            ];
            this.selectedOrderStatus = 1;
        } else if(this.currentUser && this.currentUser.RoleID.Description == 'OM Supervisor'){
            this.allOrderStatusByRole = [
                {value: 1, name: 'Pending'},
                {value: 2, name: 'Approved'},
                {value: 10, name: 'Declined'}
            ];
            this.selectedOrderStatus = 1;
        }
        //  else if(this.currentUser && this.currentUser.RoleID.Description == 'Dealer'){
        //     this.router.navigate(['/orders']);
        // }
        if(this.currentUser.RoleID && this.currentUser.RoleID.Description == 'OM Supervisory')
        this.calendar$ = this.store.select(fromCalendar.getCalendarState);
        // this.getAllByLastOneYear();
        this.getAllOrders();
        this.getAllProducts();
        this.getAllLastMonthOrders();
        this.getAllNotifications();
    }

    ngOnInit() {
    }

    getAllProducts() {
        this.spinnerService.show();
        this.productService.getProductLength().subscribe(data => {
            if(data['status']) {
                this.allProductsLength = parseInt(data['result']);
            } else {
                this.snackBar.open(data['message'], 'OK', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
            }
            this.spinnerService.hide();
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('something went error', 'OK', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        })
    }

    getAllOrders() {
        this.spinnerService.show();
        this.orderService.getAllOrdersByRole(this.currentUser.RoleID.Description, this.currentUser.id).subscribe(data => {
            if(data['status']) {
                this.allOrders = data['result'];
                this.selectedOrders = this.allOrders.filter(x => x.Status == 1);
                this.onChangeStatus();
            } else {
                this.snackBar.open(data['message'], 'OK', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
            }
            this.spinnerService.hide();
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('something went error', 'OK', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        })
    }

    getAllNotifications() {
        if(this.currentUser.RoleID && this.currentUser.RoleID.Description == 'Approver'){
            return;
        }
        this.spinnerService.show();
        this.orderService.getNotificationByUser(this.currentUser.id).subscribe(data => {
            if(data['status']) {
                this.allNotifications = data['result'];
            } else {
                this.snackBar.open(data['message'], 'OK', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
            }
            this.spinnerService.hide();
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('something went error', 'OK', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        })
    }

    readNotiFications(NotificationID){
        this.spinnerService.show();
        this.orderService.readNotificationByUser(NotificationID, this.currentUser.id).subscribe(data => {
            if(data['status']) {
                this.getAllNotifications();
            } else {
                this.snackBar.open(data['message'], 'OK', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
            }
            this.spinnerService.hide();
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('something went error', 'OK', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        })
    }

    getAllByLastOneYear() {
        this.spinnerService.show();
        this.orderService.getAllByLastOneYear().subscribe(data => {
            if(data['status']) {
                this.allOrders = data['result'];
                this.selectedOrders = this.allOrders.filter(x => x.Status == 1 || x.Status == 2);
                this.onChangeStatus();

                let orders = this.allOrders.map(item => {
                    const dt = new Date(item.CreatedDate);
                    item.CreatedMonth = dt.getMonth() + 1;
                    item.CreatedYear = dt.getFullYear();
                    return item;
                });
                orders.sort(function(a, b){return a.CreatedMonth-b.CreatedMonth});
                orders.sort(function(a, b){return a.CreatedYear-b.CreatedYear});
                const unqMonth = orders.map(item => item.CreatedMonth)
                    .filter((value, index, self) => self.indexOf(value) === index);
                // unqMonth.sort(function(a, b){return a-b});
                const TotalArr = [], ApprovedgArr = [], DispatchedArr = [], DeliveredArr = [];
                unqMonth.map(item => {
                    var totalOrders = orders.filter(x => x.CreatedMonth == item);
                    var ApprovedOrders = orders.filter(x => x.CreatedMonth == item && x.Status == 2);
                    var DispatchedOrders = orders.filter(x => x.CreatedMonth == item && x.Status == 3);
                    var DeliveredOrders = orders.filter(x => x.CreatedMonth == item && x.Status == 4);
                    this.barChartLabels.push(this.monthArr[item]);
                    TotalArr.push(totalOrders.length);
                    ApprovedgArr.push(ApprovedOrders.length);
                    DispatchedArr.push(DispatchedOrders.length);
                    DeliveredArr.push(DeliveredOrders.length);
                });
                this.orderDetailGraph = [
                    { data: TotalArr, label: 'Total' },
                    { data: ApprovedgArr, label: 'Approved' },
                    { data: DispatchedArr, label: 'Dispatched' },
                    { data: DeliveredArr, label: 'Delivered' }
                ];
                this.showOrderDetailGraph = true;
            } else {
                this.snackBar.open(data['message'], 'OK', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
            }
            this.spinnerService.hide();
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('something went error', 'OK', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        })
    }

    onChangeStatus(){
        if(this.selectedOrderStatus == 0){
            this.dataSource = new MatTableDataSource(this.allOrders);
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
        } else {
            this.selectedOrders = this.allOrders.filter(x => x.Status == this.selectedOrderStatus);
            this.dataSource = new MatTableDataSource(this.selectedOrders);
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
        }
    }

    getAllLastMonthOrders(){
        this.spinnerService.show();
        this.orderService.getAllLastMonth().subscribe(data => {
            if(data['status']) {
                var orders = data['result'];
                var PendingOrders = orders.filter(x => x.Status == 1);
                var ApprovedOrders = orders.filter(x => x.Status == 2);
                var DisapprovedOrders = orders.filter(x => x.Status == 6);
                var DispatchedOrders = orders.filter(x => x.Status == 3);
                var DeliveredOrders = orders.filter(x => x.Status == 4);
                var AckOrders = orders.filter(x => x.Status == 5);
                this.pieChartData = [
                    PendingOrders.length, ApprovedOrders.length, DisapprovedOrders.length, DispatchedOrders.length, DeliveredOrders.length, AckOrders.length
                ]
                this.showPieChart = true;
            } else {
                this.spinnerService.hide();
                this.snackBar.open(data['message'], 'OK', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
            }
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('something went error', 'OK', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        })
    }

    onClickOrderNotification(OrderID, NotificationID){
        this.readNotiFications(NotificationID);
        this.openDialog(OrderID);
    }

    readAllNotification(){
        let lab = 'Are you sure want to read all notification?';
        this.notificationService.smartMessageBox({
            title: "Read All",
            content: lab,
            buttons: '[No][Yes]'
        }, (ButtonPressed) => {
            if (ButtonPressed === "Yes") {
                this.spinnerService.show();
                this.orderService.readAllNotification(this.allNotifications, this.currentUser.id).subscribe(data => {
                    this.getAllNotifications();
                    this.spinnerService.hide();
                    this.snackBar.open('All notification readed successfully', 'OK', {
                        duration: 5000,
                        panelClass: ['success-snackbar'],
                        verticalPosition: 'top'
                    });
                }, err => {
                    this.spinnerService.hide();
                    this.snackBar.open('something went error', 'OK', {
                        duration: 5000,
                        panelClass: ['danger-snackbar'],
                        verticalPosition: 'top'
                    });
                })
            }
            if (ButtonPressed === "No") {
                this.spinnerService.hide();
            }
        });
    }

    openDialog(OrderID): void {
        var index = this.allOrders.findIndex(function(x) {return x._id == OrderID});
        if(index == -1){
            return;
        }
        const dialogRef = this.dialog.open(OrderDetailDialogDashboard, {
            width: '99%',
            data: {
                orderDetail: this.allOrders[index]
            }
        });

        dialogRef.afterClosed().subscribe(result => {
            if(result){
                this.getAllOrders();
                this.getAllNotifications();
            }
        });
    }

}




@Component({
    selector: 'order-detail-dialog-dashboard',
    templateUrl: 'order-detail-dialog-dashboard.html',
})
export class OrderDetailDialogDashboard {
    productEdited: boolean = false;
    enableEdit = -1;
    productOldArr = [];

    currentUser: any;
    apiUrl: string;
    orderDetail: any = {};
    dispatchDetail: any;
    orderTimeLineDetail: any;
    acknowledgeDetail: any;
    orderReturDetail: any;
    allStatus: any = ['', 'Pending', 'Approved', 'Disapproved', 'Dispatched', 'Delivered', 'Acknowledged', 'Return request', 'Transfer', 'Returned', 'Declined'];
    currentDate: Date = new Date();
    scheduledReturnDate: Date;
    transferDetail: any;
    productInTransferRequest: any = [];
    constructor(public commonService: CommonService,
                public orderService: OrderService,
                public spinnerService: Ng4LoadingSpinnerService,
                private snackBar: MatSnackBar,
                private notificationService: NotificationService,
                public dialogRef: MatDialogRef<OrderDetailDialogDashboard>,
                @Inject(MAT_DIALOG_DATA) public data: any) {
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.apiUrl = this.commonService.getApiUrl();
        this.orderDetail = data.orderDetail;
        this.scheduledReturnDate = this.orderDetail.ScheduleReturnDate;
        this.orderDetail.Products.map(item => {
            this.productOldArr.push({
                ProductConsumable: item.ProductConsumable,
                Quantity: item.Quantity,
                Price: item.Price
            });
        });
        this.getDispatcheOrderDetail(this.orderDetail._id);
        this.getOrderTimeLine(this.orderDetail._id);
        this.getOrderAcknowledgeDetail(this.orderDetail._id);
        this.getOrderReturnData(this.orderDetail._id);
        this.getTransferRequestDetail(this.orderDetail._id);
    }

    getOrderReturnData(id){
        this.spinnerService.show();
        this.orderService.getOrderReturnData(id).subscribe(data => {
            if(data['status']) {
                this.orderReturDetail = data['result'];
            } else {
                this.snackBar.open(data['message'], 'OK', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
            }
            this.spinnerService.hide();
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('something went error', 'OK', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        })
    }

    getTransferRequestDetail(id){
        this.spinnerService.show();
        this.orderService.getTransferRequest(id).subscribe(data => {
            if(data['status']) {
                this.transferDetail = data['result'];
                if(this.transferDetail)
                this.productInTransferRequest = this.transferDetail.TransferData.Products.filter(x => x.value)
            } else {
                this.snackBar.open(data['message'], 'OK', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
            }
            this.spinnerService.hide();
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('something went error', 'OK', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        })
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    getDispatcheOrderDetail(OrderID){
        this.spinnerService.show();
        this.orderService.getDispatcheOrderDetail(OrderID).subscribe(data => {
            if(data['status']) {
                this.dispatchDetail = data['result'];
            } else {
                this.snackBar.open(data['message'], 'OK', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
            }
            this.spinnerService.hide();
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('something went error', 'OK', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        })
    }

    getOrderTimeLine(OrderID){
        this.spinnerService.show();
        this.orderService.getOrderTimeLine(OrderID).subscribe(data => {
            if(data['status']) {
                this.orderTimeLineDetail = data['result'];
            } else {
                this.snackBar.open(data['message'], 'OK', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
            }
            this.spinnerService.hide();
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('something went error', 'OK', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        })
    }

    getOrderAcknowledgeDetail(OrderID) {
        this.spinnerService.show();
        this.orderService.getOrderAcknowledgeDetail(OrderID).subscribe(data => {
            if(data['status']) {
                this.acknowledgeDetail = data['result'];
            } else {
                this.snackBar.open(data['message'], 'OK', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
            }
            this.spinnerService.hide();
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('something went error', 'OK', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        })
    }

    onClickDelivered(OrderID){
        let lab = 'Are you sure this order has been successfully delivered?';
        this.notificationService.smartMessageBox({
            title: "Delivered!",
            content: lab,
            buttons: '[No][Yes]'
        }, (ButtonPressed) => {
            if (ButtonPressed === "Yes") {
                this.spinnerService.show();
                this.orderService.orderDelivered(this.currentUser.id, OrderID).subscribe(data => {
                    this.spinnerService.hide();
                    this.snackBar.open('Order Delivered successfully', 'OK', {
                        duration: 5000,
                        panelClass: ['success-snackbar'],
                        verticalPosition: 'top'
                    });
                    this.dialogRef.close(true);
                }, err => {
                    this.spinnerService.hide();
                    this.snackBar.open('something went error', 'OK', {
                        duration: 5000,
                        panelClass: ['danger-snackbar'],
                        verticalPosition: 'top'
                    });
                })
            }
            if (ButtonPressed === "No") {
                this.spinnerService.hide();
            }
        });
    }

    approveOrder(OrderID) {
        let lab = 'Are you sure to approve this order request?';
        this.notificationService.smartMessageBox({
            title: "Approve!",
            content: lab,
            buttons: '[No][Yes]'
        }, (ButtonPressed) => {
            if (ButtonPressed === "Yes") {
                this.spinnerService.show();
                this.orderService.approveOrder(OrderID, this.currentUser.id).subscribe(data => {
                    this.spinnerService.hide();
                    this.snackBar.open('Order Approved successfully', 'OK', {
                        duration: 5000,
                        panelClass: ['success-snackbar'],
                        verticalPosition: 'top'
                    });
                    this.dialogRef.close(true);
                }, err => {
                    this.spinnerService.hide();
                    this.snackBar.open('something went error', 'OK', {
                        duration: 5000,
                        panelClass: ['danger-snackbar'],
                        verticalPosition: 'top'
                    });
                })
            }
            if (ButtonPressed === "No") {
                this.spinnerService.hide();
            }
        });
    }

    disapproveOrder(OrderID) {
        let lab = 'Do you want to disapprove this order request?';
        this.notificationService.smartMessageBox({
            title: "Disapprove!",
            content: lab,
            buttons: '[No][Yes]'
        }, (ButtonPressed) => {
            if (ButtonPressed === "Yes") {
                this.spinnerService.show();
                this.orderService.disapproveOrder(OrderID, this.currentUser.id, '').subscribe(data => {
                    this.spinnerService.hide();
                    this.snackBar.open('Order Disapproved successfully', 'OK', {
                        duration: 5000,
                        panelClass: ['success-snackbar'],
                        verticalPosition: 'top'
                    });
                    this.dialogRef.close(true);
                }, err => {
                    this.spinnerService.hide();
                    this.snackBar.open('something went error', 'OK', {
                        duration: 5000,
                        panelClass: ['danger-snackbar'],
                        verticalPosition: 'top'
                    });
                })
            }
            if (ButtonPressed === "No") {
                this.spinnerService.hide();
            }
        });
    }

    onClickReturAccept(OrderID){
        let lab = 'Are you sure this order has been successfully returned?';
        this.notificationService.smartMessageBox({
            title: "Returned!",
            content: lab,
            buttons: '[No][Yes]'
        }, (ButtonPressed) => {
            if (ButtonPressed === "Yes") {
                this.spinnerService.show();
                this.orderService.orderReturnAccept(OrderID).subscribe(data => {
                    this.spinnerService.hide();
                    this.snackBar.open('Order Returned successfully', 'OK', {
                        duration: 5000,
                        panelClass: ['success-snackbar'],
                        verticalPosition: 'top'
                    });
                    this.dialogRef.close(true);
                }, err => {
                    this.spinnerService.hide();
                    this.snackBar.open('something went error', 'OK', {
                        duration: 5000,
                        panelClass: ['danger-snackbar'],
                        verticalPosition: 'top'
                    });
                })
            }
            if (ButtonPressed === "No") {
                this.spinnerService.hide();
            }
        });
    }

    onClickTransferAccept(OrderID){
        let lab = 'Are you sure want to transfer this order from ' + this.transferDetail.OldSalesPersonID.FirstName + ' to ' + this.transferDetail.NewSalesPersonID.FirstName;
        this.notificationService.smartMessageBox({
            title: "Accept Transfer!",
            content: lab,
            buttons: '[No][Yes]'
        }, (ButtonPressed) => {
            if (ButtonPressed === "Yes") {
                this.spinnerService.show();
                var data = {
                    CustomerID: this.transferDetail.NewSalesPersonID._id,
                    CustomerDetails: this.transferDetail.TransferData.CustomerDetails,
                    DeliveryLocation: this.transferDetail.TransferData.DeliveryLocation,
                    Products: this.transferDetail.TransferData.Products,
                    currentUserID: this.currentUser.id,
                    transferRequestID: this.transferDetail._id
                }
                this.orderService.transferedOrder(OrderID, data).subscribe(data => {
                    this.spinnerService.hide();
                    this.snackBar.open('Order Transfered successfully', 'OK', {
                        duration: 5000,
                        panelClass: ['success-snackbar'],
                        verticalPosition: 'top'
                    });
                    this.dialogRef.close(true);
                }, err => {
                    this.spinnerService.hide();
                    this.snackBar.open('something went error', 'OK', {
                        duration: 5000,
                        panelClass: ['danger-snackbar'],
                        verticalPosition: 'top'
                    });
                })
            }
            if (ButtonPressed === "No") {
                this.spinnerService.hide();
            }
        });
    }

    dismissDialog() {
        this.dialogRef.close(true);
    }

    updateList(id: number, property: string, value: any) {
        if(property == 'ProductConsumable' && value != 'Yes' && value != 'No'){
            this.orderDetail.Products[id][property] = this.productOldArr[id][property];
        }
    }

    updateProductDetail(){
        let lab = 'Are you sure you want to update this order products detail?';
        this.notificationService.smartMessageBox({
            title: "Update Products!",
            content: lab,
            buttons: '[No][Yes]'
        }, (ButtonPressed) => {
            if (ButtonPressed === "Yes") {
                this.spinnerService.show();
                const prdArr = [];
                this.orderDetail.Products.map(row => {
                    prdArr.push({
                        ProductCode: row.ProductID.ProductCode,
                        ProductID: row.ProductID._id,
                        Quantity: row.Quantity,
                        ProductConsumable: row.ProductConsumable,
                        Price: row.Price ? row.Price : 0,
                        Inventory: row.Inventory,
                        BranchCode: row.BranchCode
                    })
                })
                this.orderService.updateProductDetail(this.orderDetail._id, prdArr, this.currentUser.id).subscribe(data => {
                    this.spinnerService.hide();
                    this.snackBar.open('Product detail updated successfully', 'OK', {
                        duration: 5000,
                        panelClass: ['success-snackbar'],
                        verticalPosition: 'top'
                    });
                    this.dialogRef.close(true);
                }, err => {
                    this.spinnerService.hide();
                    this.snackBar.open('something went error', 'OK', {
                        duration: 5000,
                        panelClass: ['danger-snackbar'],
                        verticalPosition: 'top'
                    });
                })
            }
            if (ButtonPressed === "No") {
                this.spinnerService.hide();
            }
        });
    }

    declineOrder(OrderID) {
        let lab = 'Do you want to decline this order?<br><br><textarea class="form-control" style="color: black" type="text" value="" placeholder="order decline reason" id="decline-reason" name="decline-reason"></textarea>';
        this.notificationService.smartMessageBox({
            title: "Decline Order!",
            content: lab, 
            buttons: '[No][Yes]'
        }, (ButtonPressed) => {
            if (ButtonPressed === "Yes") {
                if(!document.getElementById('decline-reason')['value']){
                    this.snackBar.open('Please post order decline reason', 'OK', {
                                duration: 5000,
                                panelClass: ['danger-snackbar'],
                                verticalPosition: 'top'
                            });
                    return;
                }
                this.spinnerService.show();
                this.orderService.declineOrder(OrderID, this.currentUser.id, document.getElementById('decline-reason')['value']).subscribe(data => {
                    this.spinnerService.hide();
                    this.snackBar.open('Order Declined successfully', 'OK', {
                        duration: 5000,
                        panelClass: ['success-snackbar'],
                        verticalPosition: 'top'
                    });
                    this.dialogRef.close(true);
                }, err => {
                    this.spinnerService.hide();
                    this.snackBar.open('something went error', 'OK', {
                        duration: 5000,
                        panelClass: ['danger-snackbar'],
                        verticalPosition: 'top'
                    });
                })
            }
            if (ButtonPressed === "No") {
                this.spinnerService.hide();
            }
        });
    }

    ocChangeScheduleReturDate(){
        // this.scheduledReturnDate = new Date(event.target.value);
        let lab = 'Do you want to update this order scheduled return date?';
        this.notificationService.smartMessageBox({
            title: "Update Scheduled Return Date!",
            content: lab, 
            buttons: '[No][Yes]'
        }, (ButtonPressed) => {
            if (ButtonPressed === "Yes") {
                this.spinnerService.show();
                this.orderService.updateScheduledReturnDate(this.orderDetail._id, this.currentUser.id, this.scheduledReturnDate).subscribe(data => {
                    this.spinnerService.hide();
                    this.snackBar.open('Scheduled return date updated successfully', 'OK', {
                        duration: 5000,
                        panelClass: ['success-snackbar'],
                        verticalPosition: 'top'
                    });
                    this.dialogRef.close(true);
                }, err => {
                    this.spinnerService.hide();
                    this.snackBar.open('something went error', 'OK', {
                        duration: 5000,
                        panelClass: ['danger-snackbar'],
                        verticalPosition: 'top'
                    });
                })
            }
            if (ButtonPressed === "No") {
                this.scheduledReturnDate = this.orderDetail.ScheduleReturnDate;
                this.spinnerService.hide();
            }
        });
    }

}