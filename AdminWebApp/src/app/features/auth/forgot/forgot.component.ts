import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from "@angular/router";
import {AuthenticationService} from "@app/core/common/_services/authentication.service";
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatSnackBar} from "@angular/material";

@Component({
    selector: 'app-forgot',
    templateUrl: './forgot.component.html',
    styles: []
})
export class ForgotComponent implements OnInit {
    forgotForm: FormGroup;
    //returnUrl: string;
    //showLoginForm = false;
    error = '';

    constructor(private router: Router,
                private authenticationService: AuthenticationService,
                private route: ActivatedRoute,
                private formBuilder: FormBuilder,
                private snackBar: MatSnackBar,) {
    }

    ngOnInit() {
        this.forgotForm = this.formBuilder.group({
            UserEmail: ['', Validators.required]
        });
        //this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/dashboard';
    }

    forgot(event) {
        if (this.forgotForm.controls.UserEmail.value == '') {
            this.error = 'Please enter valid email address';
            return 0;
        }
        if (this.forgotForm.invalid) {
            return;
        }
        this.error = '';
        this.authenticationService.forgot(this.forgotForm.value.UserEmail)
            .pipe()
            .subscribe(
                data => {
                    if (data['status']) {
                        this.forgotForm.reset();
                        this.snackBar.open('Email Sent to your email ID so please check that and reset your password.', 'OK', {
                            duration: 5000,
                            panelClass: ['success-snackbar'],
                            verticalPosition: 'top'
                        });
                    } else {
                        this.snackBar.open('Invalid email address', 'OK', {
                            duration: 5000,
                            panelClass: ['success-snackbar'],
                            verticalPosition: 'top'
                        });
                    }
                },
                error => {
                    this.error = 'Server error';
                });
        //this.router.navigate(['/dashboard/analytics'])
    }
}
