import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from "@angular/router";
import {AuthenticationService} from "@app/core/common/_services/authentication.service";
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
    selector: 'app-reset',
    templateUrl: './reset.component.html',
    styles: []
})
export class ResetComponent implements OnInit {
    resetForm: FormGroup;
    token: string;
    //showLoginForm = false;
    error = '';

    constructor(private router: Router,
                private authenticationService: AuthenticationService,
                private route: ActivatedRoute,
                private formBuilder: FormBuilder) {
    }

    ngOnInit() {
        this.resetForm = this.formBuilder.group({
            UserPassword: ['', Validators.required],
            UserConfirmPassword: ['', Validators.required]
        });
        this.token = this.route.snapshot.paramMap.get("id");
    }

    reset(event) {
        if (this.resetForm.invalid) {
            return;
        }
        //alert(this.token);
        if (this.resetForm.value.UserPassword == this.resetForm.value.UserConfirmPassword) {
            this.authenticationService.reset(this.resetForm.value.UserPassword, this.token)
                .pipe()
                .subscribe(
                    data => {
                        if (data['status'] == true) {
                            this.router.navigate(['/auth/login']);
                        } else {
                            this.error = data['message'];
                            alert(this.error);
                        }
                    },
                    error => {
                        this.error = 'Server error';
                        alert(this.error);
                    });
            //this.router.navigate(['/dashboard/analytics'])
        }
    }

}
