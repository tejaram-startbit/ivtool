import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from "@angular/router";
import {AuthenticationService} from "@app/core/common/_services/authentication.service";
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {
    loginForm: FormGroup;
    showSpinner: boolean;
    returnUrl: string;
    showLoginForm = false;
    error = '';

    constructor(private router: Router,
                private authenticationService: AuthenticationService,
                private route: ActivatedRoute,
                private formBuilder: FormBuilder,) {
        this.showSpinner = false;
        document.getElementById('webapp-title').innerText = 'Stryker India | Instrument tracking tool';
    }

    ngOnInit() {
        this.loginForm = this.formBuilder.group({
            UserName: ['', Validators.required],
            Password: ['', Validators.required]
        });
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/dashboard';
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if (currentUser) {
            this.router.navigate(['/dashboard']);
        } else {
            this.showLoginForm = true;
        }
    }

    login(event) {
        event.preventDefault();
        if (this.loginForm.value.UserName == '' && this.loginForm.value.Password == '') {
            this.error = 'Please fill email and password fields';
            return;
        }
        if (this.loginForm.value.UserName == '') {
            this.error = 'Please fill email field';
            return;
        }
        if (this.loginForm.value.Password == '') {
            this.error = 'Please fill password field';
            return;
        }
        if (this.loginForm.invalid) {
            return;
        }
        this.error = '';
        this.showSpinner = true;
        this.authenticationService.login(this.loginForm.value.UserName, this.loginForm.value.Password)
            .pipe()
            .subscribe(
                data => {
                    if (data['status']) {
                        this.showSpinner = false;
                        this.router.navigate(['/dashboard']);
                    } else {
                        this.error = data['message'];
                        this.showSpinner = false;
                    }
                },
                error => {
                    this.error = 'Server error';
                    this.showSpinner = false;
                });
        // this.router.navigate(['/dashboard'])
    }

}
