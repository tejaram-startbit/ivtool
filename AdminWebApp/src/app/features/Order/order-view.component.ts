import { Component, OnInit, ViewChild, AfterViewInit, Inject } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { NotificationService } from "@app/core/services";
import { MatSnackBar } from "@angular/material";
import { OrderService } from "@app/features/Order/order.service";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { Location } from "@angular/common";
import { Observable } from "rxjs";
import { Ng4LoadingSpinnerService } from "ng4-loading-spinner";

import { MatSelect } from "@angular/material";

import { ReplaySubject, Subject } from "rxjs";
import { take, takeUntil } from "rxjs/operators";
import {CommonService} from "@app/core/common/common.service";
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

export interface DialogData {
  animal: string;
  name: string;
}

@Component({
  selector: "project-order-view",
  templateUrl: "./order-view.component.html"
})
export class OrderViewComponent implements OnInit {
  ID: any;
  name: any;
  States: any;
  AckForm: FormGroup;
  showSpinner: boolean = false;
  ProductAdded: boolean = false;
  ImageData: any;
  showForm: boolean = false;
  validationAckFormDetailOptions = {};
  Products2: any = [];
  Products: any = [];
  OrderProducts: any = [];
  Order: any = {};
  localStorage: any;
  search: any;
  ServiceType: any = 'Instrument';
  month: any;
  date: any;
  year: any;
  dmonth: any;
  ddate: any;
  dyear: any;
  Status : any;
  animal: string;
  searchinput: boolean = false;

  constructor(
    private http: HttpClient,
    private router: Router,
    private fb: FormBuilder,
    private location: Location,
    private route: ActivatedRoute,
    private ordersService: OrderService,
    private notificationService: NotificationService,
    private snackBar: MatSnackBar,
    private spinnerService: Ng4LoadingSpinnerService,
    private commService: CommonService,
    public dialog: MatDialog
  ) {
    this.AckForm = fb.group({
      Remarks: ["", [Validators.required]],
      Picture: ["", [Validators.required]]
    });
    this.validationAckFormDetailOptions = {
      // Rules for form validation
      rules: {
        Picture: {
          required: true
        },
        Remarks: {
          required: true
        }
      },

      // Messages for form validation
      messages: {
        Picture: {
          required: "Please select image"
        },
        Remarks: {
          required: "Please enter your remarks"
        }
      },
      submitHandler: this.onSubmit
    };

    

    // this.ordersService.getAllProducts().subscribe((data: {}) => {
    //   for (let i = 0; i < data["result"].length; i++) {
    //     this.Products2.push({
    //       ProductID: data["result"][i]._id,
    //       ProductName: data["result"][i].Name,
    //       ProductQuantity: 0
    //     });
    //   }
    // });

    this.localStorage = JSON.parse(localStorage.getItem("currentUser"));
    this.commService.sharedVariable = 'Update';
    this.Order = {
      CustomerDetails : {
        CustomerName: '',
        Mobile:'',
        Email:'',
        Address:'',
        ContactPerson:'',
        ContactPersonMobile:''
      },
      Products: [],
      ScheduleReturnDate: '',
      ServiceType: 'Instrument'
    }
    this.ID = this.route.params["value"].id;
    if (this.ID !== "1") {
      this.getOne(this.ID, this.localStorage.id);
    }
  }

  onSubmit() {
    console.log('\n', 'submit handler for validated form', '\n\n')
  }

  ngOnInit() {

  }

  searchfun(){
    if(this.search){
      this.searchinput = true;
      if(this.ServiceType == 'Demo'){
        this.getOne('DR-'+this.search, this.localStorage.id);
      }else {
        this.getOne('IR-'+this.search, this.localStorage.id);
      }
    }
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
      width: '250px',
      data: {name: this.name, animal: this.animal}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.animal = result;
    });
  }
  
  openDialog3(): void {
    const dialogRef = this.dialog.open(OrderDetailDialogReport, {
        width: '50%',
        data: {
            orderDetail: this.Order,
            orderid : this.Order._id
            
        }
    });

    dialogRef.afterClosed().subscribe(result => {

    });
}
  onSubmitForm() {
      if (this.AckForm.invalid) {
          return 0;
      }
      this.showSpinner = true;
      this.ordersService.addAcknowledge(this.AckForm.value, this.ImageData, this.ID).subscribe(data => {
          if (data['status']) {
              this.snackBar.open('Acknowledge submitted successfully', 'OK', {
                  duration: 5000,
                  panelClass: ['success-snackbar'],
                  verticalPosition: 'top'
              });
              this.showSpinner = false;
              //this.location.back();
          }
      }, err => {
          this.snackBar.open('something went error', 'OK', {
              duration: 5000,
              panelClass: ['danger-snackbar'],
              verticalPosition: 'top'
          });
      });
  }

  onFileChange(event) {
    const reader = new FileReader();

    if (event.target.files && event.target.files.length) {
        const [file] = event.target.files;
        reader.readAsDataURL(file);
        const fileName = event.target.files[0].name;
        const lastIndex = fileName.lastIndexOf('.');
        const extension = fileName.substr(lastIndex + 1);
        if (extension.toLowerCase() === 'jpg' || extension.toLowerCase() === 'jpeg' || extension.toLowerCase() === 'png') {
            reader.onload = () => {
                this.ImageData = event.target.files[0];
            };
        } else {
            this.snackBar.open('Invalid selected file', 'OK', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        }
    }
}

focusOutFunction(e){
  e.preventDefault();
  if(this.search){
    this.searchinput = true;

    if(this.ServiceType == 'Demo'){
      this.getOne('DR-'+this.search, this.localStorage.id);
    }else {
      this.getOne('IR-'+this.search, this.localStorage.id);
    }
  }
}

update(){
 if(this.month == undefined && this.date == undefined && this.year == undefined){
    return;
 }

 this.Order.ScheduleReturnDate = this.year + '-' + this.month + '-' + this.date;

  this.ordersService.updateOrder(this.Order , this.Order._id).subscribe(
    data => {
      if (data["status"]) {
        this.snackBar.open("Record created successfully", "OK", {
          duration: 5000,
          panelClass: ["success-snackbar"],
          verticalPosition: "top"
        });
        this.showSpinner = false;
        this.router.navigate(["/dashboard"]);
      }
    },
    err => {
      this.snackBar.open("something went error", "OK", {
        duration: 5000,
        panelClass: ["danger-snackbar"],
        verticalPosition: "top"
      });
    }
  );

}

getOneOrder(id){
    this.spinnerService.show();
    this.ordersService.getOneOrder(id).subscribe(
      data => {
        if (data["result"].length > 0 && data["status"] == true) {
          this.OrderProducts = data["result"][0].Products;
         
          this.Order = data["result"][0];
          const sdate = this.Order.ScheduleReturnDate;
          var d = new Date(sdate);
          this.month = d.getMonth() + 1;
          if(this.Order.ScheduleReturnDate){
            if(this.month < 10){
              this.month = '0' + this.month;
            }

            this.date = d.getDate();
            if(this.date < 10){
              this.date = '0' + this.date;
            }
            this.year = d.getFullYear();
          }
          if(this.Order.Status == 1){
            this.Status = 'Pending';
          }else if(this.Order.Status == 2){
            this.Status = 'Approved';
          }else if(this.Order.Status == 3){
            this.Status = 'Disapproved';
          }else if(this.Order.Status == 4){
            this.Status = 'Dispatched';
          }else if(this.Order.Status == 5){
            this.Status = 'Delivered';
          }else if(this.Order.Status == 6){
            this.Status = 'Acknowledged';
          }else if(this.Order.Status == 7){
            this.Status = 'Return';
          }else if(this.Order.Status == 8){
            this.Status = 'Transfer';
          }else if(this.Order.Status == 9){
            this.Status = 'Return Approved';
          }

        }
        else {
          this.snackBar.open("Not found any result", "OK", {
            duration: 5000,
            panelClass: ["danger-snackbar"],
            verticalPosition: "top"
          });
        }
        this.spinnerService.hide();
      },
      err => {
        this.snackBar.open("something went error", "OK", {
          duration: 5000,
          panelClass: ["danger-snackbar"],
          verticalPosition: "top"
        });
      }
    );
  }

  ServiceTypeChange() {
    this.search = null;
    this.Order = {
      CustomerDetails : {
        CustomerName: '',
        Mobile:'',
        Email:'',
        Address:'',
        ContactPerson:'',
        ContactPersonMobile:''
      },
      Products: [],
      ScheduleReturnDate: '',
      ServiceType: 'Instrument'
    }
    this.year = null;
    this.month = null;
    this.date = null;
    this.Status = null;
  }

  getOne(id, salespersonid) {
    this.spinnerService.show();
    this.ordersService.getOneOrderByRequestID(id, salespersonid).subscribe(
      data => {
        this.searchinput = false;
        if (data["result"].length > 0 && data["status"] == true) {
          if(data["result"][0].SalesPersonID == this.localStorage.id){
            this.OrderProducts = data["result"][0].Products;
            this.Order = data["result"][0];
            const sdate = this.Order.ScheduleReturnDate;
            var d = new Date(sdate);
            this.month = d.getMonth() + 1;
            if(this.Order.ScheduleReturnDate){
              if(this.month < 10){
                this.month = '0' + this.month;
              }
              this.date = d.getDate();
              if(this.date < 10){
                this.date = '0' + this.date;
              }
              this.year = d.getFullYear();
            }
            if(this.Order.Status == 1){
              this.Status = 'Pending';
            }else if(this.Order.Status == 2){
              this.Status = 'Approved';
            }else if(this.Order.Status == 3){
              //this.searchinput = true;
              this.Status = 'Disapproved';
            }else if(this.Order.Status == 4){
              this.Status = 'Dispatched';
            }else if(this.Order.Status == 5){
              this.Status = 'Delivered';
            }else if(this.Order.Status == 6){
              this.Status = 'Acknowledged';
            }else if(this.Order.Status == 7){
              this.Status = 'Return';
            }else if(this.Order.Status == 8){
              this.Status = 'Transfer';
            }else if(this.Order.Status == 9){
              this.Status = 'Return Approved';
            }
          } else {
            this.snackBar.open("Order not associated with you", "OK", {
              duration: 5000,
              panelClass: ["danger-snackbar"],
              verticalPosition: "top"
            });
            this.Order = {
              CustomerDetails : {
                CustomerName: '',
                Mobile:'',
                Email:'',
                Address:'',
                ContactPerson:'',
                ContactPersonMobile:''
              },
              Products: [],
              ScheduleReturnDate: '',
              ServiceType: 'Instrument'
            }
            this.year = null;
            this.month = null;
            this.date = null;
            this.Status = null;
          }
        }
        else {
          this.snackBar.open("Order not found", "OK", {
            duration: 5000,
            panelClass: ["danger-snackbar"],
            verticalPosition: "top"
          });
          this.Order = {
            CustomerDetails : {
              CustomerName: '',
              Mobile:'',
              Email:'',
              Address:'',
              ContactPerson:'',
              ContactPersonMobile:''
            },
            Products: [],
            ScheduleReturnDate: '',
            ServiceType: 'Instrument'
          }
          this.year = null;
          this.month = null;
          this.date = null;
          this.Status = null;
        }
        this.spinnerService.hide();
      },
      err => {
        this.snackBar.open("something went error", "OK", {
          duration: 5000,
          panelClass: ["danger-snackbar"],
          verticalPosition: "top"
        });
        this.Order = {
          CustomerDetails : {
            CustomerName: '',
            Mobile:'',
            Email:'',
            Address:'',
            ContactPerson:'',
            ContactPersonMobile:''
          },
          Products: [],
          ScheduleReturnDate: '',
          ServiceType: 'Instrument'
        }
      }
    );
  }

  openAck() {
    this.router.navigate(["/orders/ack/"+this.Order._id]);
  }

  openTransfer() {
    this.router.navigate(["/orders/transfer/"+this.Order._id]);
  }

  openReturn() {
    this.router.navigate(["/orders/return/"+this.Order._id]);
  }

  // cancel() {
  //   this.location.back();
  // }

}

@Component({
  selector: 'dialog-overview-example-dialog',
  templateUrl: 'dialog-overview-example-dialog.html',
})
export class DialogOverviewExampleDialog {

  constructor(
    public dialogRef: MatDialogRef<DialogOverviewExampleDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

}

@Component({
  selector: 'order-detail-dialog-report',
  templateUrl: 'order-detail-dialog-report.html',
})
export class OrderDetailDialogReport{
  currentUser: any;
  apiUrl: string;
  orderDetail: any = {};
  dispatchDetail: any;
  orderTimeLineDetail: any;
  acknowledgeDetail: any;
  allStatus: any = ['', 'Pending', 'Approved', 'Disapproved', 'Dispatched', 'Delivered', 'Acknowledged', 'Return request', 'Transfer', 'Returned'];
  constructor(public commonService: CommonService,
              public orderService: OrderService,
              public spinnerService: Ng4LoadingSpinnerService,
              private snackBar: MatSnackBar,
              private notificationService: NotificationService,
              public dialogRef: MatDialogRef<OrderDetailDialogReport>,
              @Inject(MAT_DIALOG_DATA) public data: any) {
      this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
      this.apiUrl = this.commonService.getApiUrl();
      this.orderDetail = data.orderDetail;
      this.getDispatcheOrderDetail(this.orderDetail._id);
      this.getOrderTimeLine(this.orderDetail._id);
      this.getOrderAcknowledgeDetail(this.orderDetail._id);
  }

  onNoClick(): void {
      this.dialogRef.close();
  }

  getDispatcheOrderDetail(OrderID){
      this.spinnerService.show();
      this.orderService.getDispatcheOrderDetail(OrderID).subscribe(data => {
          if(data['status']) {
              this.dispatchDetail = data['result'];
          } else {
              this.snackBar.open(data['message'], 'OK', {
                  duration: 5000,
                  panelClass: ['danger-snackbar'],
                  verticalPosition: 'top'
              });
          }
          this.spinnerService.hide();
      }, err => {
          this.spinnerService.hide();
          this.snackBar.open('something went error', 'OK', {
              duration: 5000,
              panelClass: ['danger-snackbar'],
              verticalPosition: 'top'
          });
      })
  }

  getOrderTimeLine(OrderID){
      this.spinnerService.show();
      this.orderService.getOrderTimeLine(OrderID).subscribe(data => {
          if(data['status']) {
              this.orderTimeLineDetail = data['result'];
          } else {
              this.snackBar.open(data['message'], 'OK', {
                  duration: 5000,
                  panelClass: ['danger-snackbar'],
                  verticalPosition: 'top'
              });
          }
          this.spinnerService.hide();
      }, err => {
          this.spinnerService.hide();
          this.snackBar.open('something went error', 'OK', {
              duration: 5000,
              panelClass: ['danger-snackbar'],
              verticalPosition: 'top'
          });
      })
  }

  getOrderAcknowledgeDetail(OrderID) {
      this.spinnerService.show();
      this.orderService.getOrderAcknowledgeDetail(OrderID).subscribe(data => {
          if(data['status']) {
              this.acknowledgeDetail = data['result'];
          } else {
              this.snackBar.open(data['message'], 'OK', {
                  duration: 5000,
                  panelClass: ['danger-snackbar'],
                  verticalPosition: 'top'
              });
          }
          this.spinnerService.hide();
      }, err => {
          this.spinnerService.hide();
          this.snackBar.open('something went error', 'OK', {
              duration: 5000,
              panelClass: ['danger-snackbar'],
              verticalPosition: 'top'
          });
      })
  }

  onClickDelivered(OrderID){
      let lab = 'Are you sure this order has been successfully delivered?';
      this.notificationService.smartMessageBox({
          title: "Delivered!",
          content: lab,
          buttons: '[No][Yes]'
      }, (ButtonPressed) => {
          if (ButtonPressed === "Yes") {
              this.spinnerService.show();
              this.orderService.orderDelivered(this.currentUser.id, OrderID).subscribe(data => {
                  this.spinnerService.hide();
                  this.snackBar.open('Order Delivered successfully', 'OK', {
                      duration: 5000,
                      panelClass: ['success-snackbar'],
                      verticalPosition: 'top'
                  });
                  this.dialogRef.close(true);
              }, err => {
                  this.spinnerService.hide();
                  this.snackBar.open('something went error', 'OK', {
                      duration: 5000,
                      panelClass: ['danger-snackbar'],
                      verticalPosition: 'top'
                  });
              })
          }
          if (ButtonPressed === "No") {
              this.spinnerService.hide();
          }
      });
  }

  approveOrder(OrderID) {
      let lab = 'Are you sure to approve this order request?';
      this.notificationService.smartMessageBox({
          title: "Approve!",
          content: lab,
          buttons: '[No][Yes]'
      }, (ButtonPressed) => {
          if (ButtonPressed === "Yes") {
              this.spinnerService.show();
              this.orderService.approveOrder(OrderID, this.currentUser.id).subscribe(data => {
                  this.spinnerService.hide();
                  this.snackBar.open('Order Approved successfully', 'OK', {
                      duration: 5000,
                      panelClass: ['success-snackbar'],
                      verticalPosition: 'top'
                  });
                  this.dialogRef.close(true);
              }, err => {
                  this.spinnerService.hide();
                  this.snackBar.open('something went error', 'OK', {
                      duration: 5000,
                      panelClass: ['danger-snackbar'],
                      verticalPosition: 'top'
                  });
              })
          }
          if (ButtonPressed === "No") {
              this.spinnerService.hide();
          }
      });
  }

  disapproveOrder(OrderID) {
      let lab = 'Do you want to disapprove this order request?';
      this.notificationService.smartMessageBox({
          title: "Disapprove!",
          content: lab,
          buttons: '[No][Yes]'
      }, (ButtonPressed) => {
          if (ButtonPressed === "Yes") {
              this.spinnerService.show();
              this.orderService.disapproveOrder(OrderID, this.currentUser.id, '').subscribe(data => {
                  this.spinnerService.hide();
                  this.snackBar.open('Order Disapproved successfully', 'OK', {
                      duration: 5000,
                      panelClass: ['success-snackbar'],
                      verticalPosition: 'top'
                  });
                  this.dialogRef.close(true);
              }, err => {
                  this.spinnerService.hide();
                  this.snackBar.open('something went error', 'OK', {
                      duration: 5000,
                      panelClass: ['danger-snackbar'],
                      verticalPosition: 'top'
                  });
              })
          }
          if (ButtonPressed === "No") {
              this.spinnerService.hide();
          }
      });
  }

  onClickReturAccept(OrderID){
      let lab = 'Are you sure this order has been successfully returned?';
      this.notificationService.smartMessageBox({
          title: "Returned!",
          content: lab,
          buttons: '[No][Yes]'
      }, (ButtonPressed) => {
          if (ButtonPressed === "Yes") {
              this.spinnerService.show();
              this.orderService.orderReturnAccept(OrderID).subscribe(data => {
                  this.spinnerService.hide();
                  this.snackBar.open('Order Returned successfully', 'OK', {
                      duration: 5000,
                      panelClass: ['success-snackbar'],
                      verticalPosition: 'top'
                  });
                  this.dialogRef.close(true);
              }, err => {
                  this.spinnerService.hide();
                  this.snackBar.open('something went error', 'OK', {
                      duration: 5000,
                      panelClass: ['danger-snackbar'],
                      verticalPosition: 'top'
                  });
              })
          }
          if (ButtonPressed === "No") {
              this.spinnerService.hide();
          }
      });
  }

  dismissDialog() {
      this.dialogRef.close(true);
  }

}