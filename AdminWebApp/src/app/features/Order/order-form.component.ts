import {Component, OnInit, ViewChild, AfterViewInit, ElementRef} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {NotificationService} from "@app/core/services";
import {MatSnackBar} from "@angular/material";
import {OrderService} from "@app/features/Order/order.service";
import {ToolkitService} from "@app/features/Tool-kit/tool-kit.service";
import {InventoryService} from "@app/features/Inventory/Inventory.service";
import {UserService} from "@app/features/User/user.service";
import {
    FormBuilder,
    FormGroup,
    FormArray,
    FormControl,
    Validators
} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {Location} from "@angular/common";
import {Observable} from "rxjs";
import {Ng4LoadingSpinnerService} from "ng4-loading-spinner";
import {CommonService} from "@app/core/common/common.service";

import {MatSelect} from "@angular/material";

import {ReplaySubject, Subject} from "rxjs";
import {take, takeUntil} from "rxjs/operators";

import * as XLSX from 'xlsx';  
import * as FileSaver from 'file-saver';
type AOA = any[][];

@Component({
    selector: "project-order-form",
    templateUrl: "./order-form.component.html"
})
export class OrderFormComponent implements OnInit {
    ID: any;
    name: any;
    States: any;
    orderForm: FormGroup;
    showSpinner: boolean = false;
    ProductAdded: boolean = false;
    ImageData: any;
    showForm: boolean = false;
    validationorderFormDetailOptions = {};
    Products2: any = [];
    Products: any = [];
    OrderProducts: any = [];
    Order: any = {};
    localStorage: any;
    days: number = 31;
    requestID: number;

    ProductCode: any;
    Quantity: any;
    ProductConsumable: any = 'No';
    showerror1: boolean = false;
    showerror2: boolean = false;
    showerror3: boolean = false;
    SameAs: boolean = false;
    month: any;
    date: any;
    year: any;
    mobNumberPattern = "^(0)?[0-9]{10}$";
    submitrunning: boolean = false;
    dateerror: boolean = false;
    dateerrormsg: any;

    /** control for the selected bank */
    public customerCtrl: FormControl = new FormControl('0');
    public bankCtrl: FormControl = new FormControl();
    public addressCtrl: FormControl = new FormControl();
    public kitCtrl: FormControl = new FormControl();
    public salespersonCtrl: FormControl = new FormControl();

    /** control for the MatSelect filter keyword */
    public customerFilterCtrl: FormControl = new FormControl();
    public bankFilterCtrl: FormControl = new FormControl();
    public addressFilterCtrl: FormControl = new FormControl();
    public kitFilterCtrl: FormControl = new FormControl();
    public salespersonFilterCtrl: FormControl = new FormControl();

    private banks: any = [];


    /** list of banks filtered by search keyword */
    public filteredCustomers: any = [];
    public filteredBanks: any = [];
    public filteredAddresses: any = [];
    public filteredKits: any = [];
    public filteredSalespersons: any = [];

    allCustomers: any = [];
    allDeliveryAddress: any = [];
    allKits: any = [];
    allSalesPerson: any = [];

    fileUploaded: DataTransfer;
    worksheet: any;
    excelData: AOA = [[1, 2], [3, 4]];
    wopts: XLSX.WritingOptions = { bookType: 'xlsx', type: 'array' };
    allInventories: any = [];
    @ViewChild('myExcelFileInput') myExcelFileInputVariable: ElementRef;

    inCompletedOrder = false;
    constructor(private http: HttpClient,
                private router: Router,
                private fb: FormBuilder,
                private location: Location,
                private route: ActivatedRoute,
                private ordersService: OrderService,
                private notificationService: NotificationService,
                private snackBar: MatSnackBar,
                private spinnerService: Ng4LoadingSpinnerService,
                private toolkitService: ToolkitService,
                private inventoryService: InventoryService,
                private userService: UserService,
                private commService: CommonService) {
        this.getAllCustomers();
        this.getAllDeliveryAddress();
        this.getAllToolKit();
        this.getAllInventories();
        this.getAllSalesperson();
        this.validationorderFormDetailOptions = {
            // Rules for form validation
            rules: {
                CustomerName: {
                    required: true
                },
                Mobile: {
                    required: true
                },
                Attachment: {
                    required: true
                }
            },

            // Messages for form validation
            messages: {
                CustomerName: {
                    required: "Please enter order Name"
                },
                Mobile: {
                    required: "Please select order Type"
                },
                Attachment: {
                    required: "Please select order image"
                }
            },
            submitHandler: this.onSubmit
        };
        this.ID = this.route.params["value"].id;
        if (this.ID !== "-1") {
            this.getOne(this.ID);
        }
        this.showForm = true;

        this.ordersService.getAllProducts().subscribe((data: {}) => {
            this.Products = data["result"];
            this.Products2 = data["result"];
        });

        //this.requestID = this.getLastOrderRequestID();
        this.ordersService.getLastOrderRequestID().subscribe(
            data => {
                if (data["status"]) {
                    this.requestID = data["result"];
                }
            },
            err => {
                console.log(err);
            }
        );

        this.localStorage = JSON.parse(localStorage.getItem("currentUser"));
        this.Order = {
            CustomerDetails: {
                CustomerID: '0',
                CustomerName: '',
                Mobile: '',
                Email: '',
                Address: '',
                ContactPerson: '',
                ContactPersonMobile: ''
            },
            Products: [],
            ScheduleReturnDate: '',
            ServiceType: 'Instrument',
            RequestType: 'Short Term',
            OrderType: 'Kit Component',
            KitStatus: ''
        }

        this.commService.sharedVariable = 'Create Order';
    }


    @ViewChild('singleSelect') singleSelect: MatSelect;

    /** Subject that emits when the component has been destroyed. */
    private _onDestroy = new Subject<void>();

    ngOnInit() {
        // listen for search field value changes
        this.customerFilterCtrl.valueChanges
            .pipe(takeUntil(this._onDestroy))
            .subscribe(() => {
                this.filterCustomers();
            });

        this.bankFilterCtrl.valueChanges
            .pipe(takeUntil(this._onDestroy))
            .subscribe(() => {
                this.filterBanks();
            });

        this.addressFilterCtrl.valueChanges
            .pipe(takeUntil(this._onDestroy))
            .subscribe(() => {
                this.filterAddresses();
            });
            
        this.kitFilterCtrl.valueChanges
            .pipe(takeUntil(this._onDestroy))
            .subscribe(() => {
                this.filterKits();
            });

        this.salespersonFilterCtrl.valueChanges
            .pipe(takeUntil(this._onDestroy))
            .subscribe(() => {
                this.filterSalespersons();
            });
    }

    ngAfterViewInit() {
        //this.setInitialValue();
    }

    ngOnDestroy() {
        this._onDestroy.next();
        this._onDestroy.complete();
    }

    /**
     * Sets the initial value after the filteredBanks are loaded initially
     */
    private setInitialValue() {
        this.filteredBanks
            .pipe(take(1), takeUntil(this._onDestroy))
            .subscribe(() => {
                
            });
    }

    private filterCustomers() {
        if (!this.Products) {
            return;
        }
        
        // get the search keyword
        let search = this.customerFilterCtrl.value;
        if (!search) {
            this.filteredCustomers = this.allCustomers.slice();
            return;
        } else {
            search = search.toLowerCase();
        }
        // filter the customers
        this.filteredCustomers = this.allCustomers.filter(
            item => item.FirstName.toLowerCase().indexOf(search) > -1
        );
    }

    private filterBanks() {
        if (!this.Products) {
            return;
        }
        
        // get the search keyword
        let search = this.bankFilterCtrl.value;
        if(search.length < 3){
            // this.filteredBanks = [];
            return;
        }
        if (!search) {
            this.filteredBanks = this.Products.slice();
            return;
        } else {
            search = search.toLowerCase();
        }
        // filter the banks
        this.filteredBanks = this.Products.filter(
            product => product.Name.toLowerCase().indexOf(search) > -1
                || product.ProductCode.toLowerCase().indexOf(search) > -1
        );
    }

    private filterAddresses() {
        // get the search keyword
        let search = this.addressFilterCtrl.value;
        if (!search) {
            this.filteredAddresses = this.allDeliveryAddress.slice();
            return;
        } else {
            search = search.toLowerCase();
        }
        // filter the banks
        this.filteredAddresses = this.allDeliveryAddress.filter(
            x => x.Address.toLowerCase().indexOf(search) > -1
        );
    }

    private filterKits() {
        // get the search keyword
        let search = this.kitFilterCtrl.value;
        if (!search) {
            this.filteredKits = this.allKits.slice();
            return;
        } else {
            search = search.toLowerCase();
        }
        // filter the kit
        this.filteredKits = this.allKits.filter(
            x => x.Name.toLowerCase().indexOf(search) > -1
            || x.KitCode.toLowerCase().indexOf(search) > -1
        );
    }

    private filterSalespersons() {
        // get the search keyword
        let search = this.salespersonFilterCtrl.value;
        if (!search) {
            this.filteredSalespersons = this.allSalesPerson.slice();
            return;
        } else {
            search = search.toLowerCase();
        }
        // filter the salesperson
        this.filteredSalespersons = this.allSalesPerson.filter(
            x => x.FirstName.toLowerCase().indexOf(search) > -1
            || x.LastName.toLowerCase().indexOf(search) > -1
        );
    }

    uploadedExcelFile(evt) {  
        this.fileUploaded = <DataTransfer>(evt.target);
        this.readExcel()
    }

    readExcel() {  
        if (this.fileUploaded.files.length !== 1) throw new Error('Cannot use multiple files');
        const reader: FileReader = new FileReader();
        reader.onload = (e: any) => {
            /* read workbook */
            const bstr: string = e.target.result;
            const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary' });

            /* grab first sheet */
            const wsname: string = wb.SheetNames[0];
            const ws: XLSX.WorkSheet = wb.Sheets[wsname];

            /* save data */
            this.excelData = <AOA>(XLSX.utils.sheet_to_json(ws, { header: 1 }));
        };
        reader.readAsBinaryString(this.fileUploaded.files[0]);
    }

    addProductFromExcel(){
        if(!this.fileUploaded) {
            this.snackBar.open("Please select excel file", "OK", {
                duration: 5000,
                panelClass: ["danger-snackbar"],
                verticalPosition: "top"
            });
            return;
        }
        if(this.excelData.length < 1){
            this.snackBar.open("Excel Empty", "OK", {
                duration: 5000,
                panelClass: ["danger-snackbar"],
                verticalPosition: "top"
            });
            return;
        }
        let i = 0;
        let flag = true;
        this.excelData.map(p => {
            if(i > 0){
                var index = this.Products2.findIndex(x => x.ProductCode == p[0]);
                if(index != -1){
                    let ProductName = this.Products2[index].Name;
                    let ProductID = this.Products2[index]._id;
                    var pIndex = this.Order.Products.findIndex(x => x.ProductID == ProductID);
                    if(isNaN(p[2])){
                        flag = false;
                        this.snackBar.open("Invalid Product: " + p[0] + " Quantity", "OK", {
                            duration: 5000,
                            panelClass: ["danger-snackbar"],
                            verticalPosition: "top"
                        });
                    } else if(pIndex != -1){
                        this.Order.Products[pIndex].Quantity += p[2];
                    } else {
                        var InvIndex = this.allInventories.findIndex(x => x.ProductCode == this.Products2[index].ProductCode);
                        if(InvIndex == -1){
                            flag = false;
                            this.snackBar.open("Instrument is not in the stock so delivery will be delayed", "OK", {
                                duration: 5000,
                                panelClass: ["warning-snackbar"],
                                verticalPosition: "top"
                            });
                        }
                        this.Order.Products.push({
                            ProductID: ProductID,
                            Name: ProductName,
                            ProductCode: this.Products2[index].ProductCode,
                            Quantity: isNaN(p[2]) ? 0 : p[2],
                            ProductConsumable: this.Products2[index].ProductConsumable ? this.Products2[index].ProductConsumable : 'No',
                            Price: this.Products2[index].Price ? this.Products2[index].Price : 0,
                            Inventory: this.Products2[index].Inventory,
                            BranchCode: this.Products2[index].BranchCode
                        });
                    }
            
                    // this.filteredBanks = this.filteredBanks.filter(function (obj) {
                    //     return obj.ProductCode != this.Products2[index].ProductCode;
                    // });
                } else {
                    flag = false;
                    this.snackBar.open("Product Code: " + p[0] + " Invalid", "OK", {
                        duration: 5000,
                        panelClass: ["danger-snackbar"],
                        verticalPosition: "top"
                    });
                }
            };
            i++;
        })

        //this.bankCtrl = null;
        this.bankCtrl.setValue(null);
    
        this.excelData = [];
        this.fileUploaded = null;
        this.myExcelFileInputVariable.nativeElement.value = "";

        this.ProductCode = null;
        this.Quantity = null;
        if(flag){
            this.snackBar.open("Excel uploaded successfully", "OK", {
                duration: 5000,
                panelClass: ["success-snackbar"],
                verticalPosition: "top"
            });
        }
    }

    ValidateDate() {
        if ((this.date != undefined && this.year != undefined && this.month != undefined) || (this.date != null && this.year != null && this.month != null)) {
            const date2 = this.year + '-' + this.month + '-' + this.date;

            var a = new Date(this.year, this.month, 0).getDate();

            var CurrentDate = new Date();
            const date3 = new Date(date2);
            CurrentDate.setHours(0);
            CurrentDate.setMinutes(0);
            CurrentDate.setSeconds(0);
            date3.setHours(0);
            date3.setMinutes(0);
            date3.setSeconds(0);

            if (a < this.date) {
                this.dateerror = true;
                this.dateerrormsg = 'Invalid Date';
            }

            else if (date3 > CurrentDate) {
                //alert('Given date is greater than the current date.');
                this.dateerror = false;
                this.dateerrormsg = '';
            } else if (CurrentDate.getDate() == date3.getDate() && CurrentDate.getFullYear() == date3.getFullYear() && CurrentDate.getMonth() == date3.getMonth()) {
                this.dateerror = false;
                this.dateerrormsg = '';
            } else {
                this.dateerror = true;
                this.dateerrormsg = 'Please select future date';
                //alert('Given date is not greater than the current date.');
            }
        }

    }

    getProductName(code: number): String {

        for (var i in this.Products2) {
            if (this.Products2[i].ProductCode == code) {
                return this.Products2[i].Name; // {a: 5, b: 6}
            }
        }
    }

    getProductID(code) {

        for (var i in this.Products2) {
            if (this.Products2[i].ProductCode == code) {
                return this.Products2[i]._id; // {a: 5, b: 6}
            }
        }
    }

    async onSubmit() {      
        if(this.Order.Products.length == 0 ) {
            this.snackBar.open("Please Add Products", "OK", {
                duration: 5000,
                panelClass: ["danger-snackbar"],
                verticalPosition: "top"
            });
            return;
        }

        if(this.year && this.month && this.date){
            this.Order.ScheduleReturnDate = this.year + '-' + this.month + '-' + this.date;
        } else if(this.Order.RequestType == 'Short Term'){
            this.dateerror = true;
            this.dateerrormsg = 'Please select Schedule Return Date';
            return;
        }

        if(!this.addressCtrl.value){
            this.snackBar.open("Please select delivery address", "OK", {
                duration: 5000,
                panelClass: ["danger-snackbar"],
                verticalPosition: "top"
            });
            return;
        }

        if (this.Order.Products.length > 0) {
            
            if (this.dateerror == true) {
                return;
            }
            if (this.Order.ServiceType == 'Demo') {
                this.Order.RequestID = 'DR-' + this.requestID;
            } else {
                this.Order.RequestID = 'IR-' + this.requestID;
            }

            if((this.localStorage.RoleID.Description == 'DealerChild' || this.localStorage.RoleID.Description == 'OM Supervisory') && !this.Order.SalesPersonID){
                this.snackBar.open("Please select dealer", "OK", {
                    duration: 5000,
                    panelClass: ["danger-snackbar"],
                    verticalPosition: "top"
                });
                return;
            } else if(!(this.localStorage.RoleID.Description == 'DealerChild' || this.localStorage.RoleID.Description == 'OM Supervisory')) {
                this.Order.SalesPersonID = this.localStorage.id;
            }
            this.submitrunning = true;
            if (this.ID === "-1") {
                if(this.Order.KitStatus == 'Go ahead with incomplete order'){
                    this.Order.Products.map(item => {
                        item.Quantity = item.AvailableQuantity
                    })
                }
                this.createNew();
            }
        }
    }

    async addProduct() {
        this.ProductCode = this.bankCtrl.value;
        let a = this.ProductCode;
        let b = false;
        // await this.Products.forEach(element => {
            
        // });
        
        if (this.ProductCode == undefined || this.ProductCode == null) {
            this.showerror1 = true;
            return;
        } else {
            this.showerror1 = false;
        }
        if (this.Quantity == undefined || this.Quantity == null) {
            this.showerror2 = true;
            return;
        }

        var ind = this.filteredBanks.findIndex(x => x.ProductCode == this.ProductCode);
        var InvIndex = this.allInventories.findIndex(x => x.ProductCode == this.ProductCode);
        // if (this.Quantity > this.filteredBanks[ind].AvailableQuantity) {
        //     this.snackBar.open("Instrument is not in the stock so delivery will be delayed", "OK", {
        //         duration: 5000,
        //         panelClass: ["warning-snackbar"],
        //         verticalPosition: "top"
        //     });
        // } else 
        if(InvIndex == -1){
            this.snackBar.open("Demo is not in the stock so delivery will be delayed", "OK", {
                duration: 5000,
                panelClass: ["warning-snackbar"],
                verticalPosition: "top"
            });
        }

        if (b == false) {
            let ProductName = this.getProductName(a);
            let ProductID = this.getProductID(a);
            this.Order.Products.push({
                ProductID: ProductID,
                Name: ProductName,
                ProductCode: this.ProductCode,
                Quantity: this.Quantity,
		AvailableQuantity: this.filteredBanks[ind].AvailableQuantity,
                ProductConsumable: this.filteredBanks[ind].ProductConsumable ? this.filteredBanks[ind].ProductConsumable : 'No',
                Price: this.filteredBanks[ind].Price ? this.filteredBanks[ind].Price : 0,
                Inventory: this.filteredBanks[ind].Inventory,
                BranchCode: this.filteredBanks[ind].BranchCode,
                MisFranchise: this.filteredBanks[ind].MisFranchise,
                MisBUGroup: this.filteredBanks[ind].MisBUGroup
            });
            //this.bankCtrl = null;
            this.bankCtrl.setValue(null);


            this.ProductCode = null;
            this.Quantity = null;

            this.filteredBanks = this.filteredBanks.filter(function (obj) {
                return obj.ProductCode != a;
            });
        }


    }


    async addKits() {
        // this.Order.Products = [];
        this.inCompletedOrder = false;
        var selectedKit = this.kitCtrl.value;
        let flag = true;
        var index = this.allKits.findIndex(x => x._id == selectedKit);
        if(index != -1){
            this.allKits[index].Inventory.map(item => {
                if(item.Quantity > item.Inventory_ID.AvailableQuantity){
                    flag = false;
                    this.inCompletedOrder = true;
                }

                var prdIndex = this.Order.Products.findIndex(x => x.ProductID == item.Inventory_ID._id && x.KitName == this.allKits[index].Name);
                if(prdIndex == -1){
                    this.Order.Products.push({
                        ProductID: item.Inventory_ID._id,
                        KitName: this.allKits[index].Name,
                        Name: item.Inventory_ID.Name,
                        ProductCode: item.Inventory_ID.ProductCode,
                        Quantity: item.Quantity,
                        AvailableQuantity: item.Inventory_ID.AvailableQuantity,
                        ProductConsumable: 'No',
                        Price: item.Inventory_ID.Price ? item.Inventory_ID.Price : 0,
                        Inventory: item.Inventory_ID.Inventory,
                        BranchCode: item.Inventory_ID.BranchCode,
                        MisFranchise: item.Inventory_ID.MisFranchise,
                        MisBUGroup: item.Inventory_ID.MisBUGroup
                    });
                } else {
                    this.Order.Products[prdIndex].Quantity = parseInt(this.Order.Products[prdIndex].Quantity) + parseInt(item.Quantity);
                }
            });
        } else {
            this.snackBar.open("Invalid tool kit", "OK", {
                duration: 5000,
                panelClass: ["danger-snackbar"],
                verticalPosition: "top"
            });
            return;
        }

        if (!flag) {
            this.Order.KitStatus = 'Go ahead with incomplete order';
            this.snackBar.open("Instrument is not in the stock so delivery will be delayed", "OK", {
                duration: 5000,
                panelClass: ["warning-snackbar"],
                verticalPosition: "top"
            });
        } else {
            this.Order.KitStatus = 'I want complete order';
            this.snackBar.open("Tool kit added successfully", "OK", {
                duration: 5000,
                panelClass: ["success-snackbar"],
                verticalPosition: "top"
            });
        }
        this.kitCtrl.setValue(null);
    }


    modelChanged1() {
        if (this.ProductCode == undefined) {
            this.showerror1 = true;
        } else {
            this.showerror1 = false;
        }

    }

    modelChanged2() {

        if (this.Quantity == undefined) {
            this.showerror2 = true;
        } else {
            this.showerror2 = false;
        }

    }

    createNew() {
        this.showSpinner = true;
        this.ordersService.addOrder(this.Order).subscribe(
            data => {
                if (data["status"]) {
                    this.snackBar.open("Order posted successfully, You will be informed once the order is approved.", "OK", {
                        duration: 5000,
                        panelClass: ["success-snackbar"],
                        verticalPosition: "top"
                    });
                    this.showSpinner = false;
                    this.submitrunning = false;
                    this.location.back();
                }
            },
            err => {
                this.snackBar.open("something went error", "OK", {
                    duration: 5000,
                    panelClass: ["danger-snackbar"],
                    verticalPosition: "top"
                });
            }
        );
    }

    getOne(id) {
        this.spinnerService.show();
        this.ordersService.getOneOrder(id).subscribe(
            data => {
                if (data["status"]) {

                    //this.OrderProducts = data["result"].Products;
                    for (let i = 0; i < data["result"].Products.length; i++) {
                        this.OrderProducts.push({
                            ProductID: data["result"].Products[i]._id,
                            ProductName: data["result"].Products[i].ProductID.Name,
                            ProductQuantity: data["result"].Products[i].ProductQuantity
                        });
                    }
                    this.Order.DeliveryLocation = data['result'].DeliveryLocation;
                    this.Order.CustomerDetails = data['result'].CustomerDetails;
                    this.Order.ServiceType = data['result'].ServiceType;
                    this.Order.ScheduleReturnDate = data['result'].ScheduleReturnDate;
                    this.Order.Consumable = data['result'].Consumable;


                }
                this.spinnerService.hide();
            },
            err => {
                this.snackBar.open("something went error", "OK", {
                    duration: 5000,
                    panelClass: ["danger-snackbar"],
                    verticalPosition: "top"
                });
            }
        );
    }

    onFileChange(event) {
        const reader = new FileReader();

        if (event.target.files && event.target.files.length) {
            const [file] = event.target.files;
            reader.readAsDataURL(file);
            const fileName = event.target.files[0].name;
            const lastIndex = fileName.lastIndexOf(".");
            const extension = fileName.substr(lastIndex + 1);
            //if (extension.toLowerCase() === 'jpg' || extension.toLowerCase() === 'jpeg' || extension.toLowerCase() === 'png') {
            reader.onload = () => {
                this.ImageData = event.target.files[0];
            };
        }
    }

    register() {
        this.router.navigate(["/dashboard"]);
    }

    cancel() {
        this.router.navigate(["/dashboard"]);
    }

    deleteProduct(productid, index) {
        this.Order.Products.splice(index, 1);
        for (let index = 0; index < this.Products2.length; index++) {
            const element = this.Products2[index];

            if (element.ProductCode === productid) {
                this.filteredBanks.push(element);
            }

        }
    }

    checkSameAsAddress(value) {
        if(this.SameAs == true){
            this.Order.DeliveryLocation = this.Order.CustomerDetails.Address;
        }else {
            this.Order.DeliveryLocation = null;
        }
    }

    reset() {
        this.Order = {
            CustomerDetails: {
                CustomerID: '0',
                CustomerName: '',
                Mobile: '',
                Email: '',
                Address: '',
                ContactPerson: '',
                ContactPersonMobile: ''
            },
            Products: [],
            ScheduleReturnDate: '',
            ServiceType: 'Instrument',
            RequestType: 'Short Term',
            OrderType: 'Kit Component',
            KitStatus: ''
        }
        this.ProductCode = '';
        this.Quantity = '';
        this.bankCtrl.setValue(null);
        this.addressCtrl.setValue(null);

    }

    getAllInventories(){
        this.spinnerService.show();
        this.inventoryService.getAllInventorys().subscribe(data => {
            if (data['status']) {
                this.allInventories = data['result'];
            } else {
                this.snackBar.open(data['message'], 'OK', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
            }
            this.spinnerService.hide();
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('something went error', 'OK', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        })
    }

    getAllCustomers() {
        this.spinnerService.show();
        this.ordersService.getAllCustomer().subscribe(data => {
            if (data['status']) {
                this.allCustomers = data['result'];
                this.filteredCustomers = data['result'];
            } else {
                this.snackBar.open(data['message'], 'OK', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
            }
            this.spinnerService.hide();
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('something went error', 'OK', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        })
    }

    getAllDeliveryAddress() {
        this.spinnerService.show();
        this.ordersService.getAllCustomerSalesPersonAddress().subscribe(data => {
            if (data['status']) {
                this.allDeliveryAddress = data['result'];
                this.filteredAddresses = [{
                    Address: this.localStorage.Address
                }];
            } else {
                this.snackBar.open(data['message'], 'OK', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
            }
            this.spinnerService.hide();
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('something went error', 'OK', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        })
    }

    getAllToolKit() {
        this.spinnerService.show();
        this.toolkitService.getAllToolkits().subscribe(data => {
            if (data['status']) {
                this.allKits = data['result'];
                this.filteredKits = data['result'];
            } else {
                this.snackBar.open(data['message'], 'OK', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
            }
            this.spinnerService.hide();
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('something went error', 'OK', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        })
    }

    getAllSalesperson(){
        this.spinnerService.show();
        this.userService.getAllSalesperson().subscribe(data => {
            if (data['status']) {
                this.allSalesPerson = data['result'];
                this.filteredSalespersons = data['result'];
            } else {
                this.snackBar.open(data['message'], 'OK', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
            }
            this.spinnerService.hide();
        }, err => {
            this.spinnerService.hide();
            this.snackBar.open('something went error', 'OK', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        })
    }

    onChangeCustomer(){
        this.Order.CustomerDetails.CustomerID = this.customerCtrl.value;
        var ind = this.allCustomers.findIndex(x => x._id == this.Order.CustomerDetails.CustomerID);
        if(ind != -1) {
            var cName = this.allCustomers[ind].FirstName + ' ';
            cName += this.allCustomers[ind].LastName ? this.allCustomers[ind].LastName : '';            this.Order.CustomerDetails.CustomerName = cName;
            this.Order.CustomerDetails.Mobile = this.allCustomers[ind].Phone;
            this.Order.CustomerDetails.Email = this.allCustomers[ind].Email;
            this.Order.CustomerDetails.Address = this.allCustomers[ind].Address;
            this.Order.CustomerDetails.ContactPerson = cName;
            this.Order.CustomerDetails.ContactPersonMobile = this.allCustomers[ind].Phone;

            if(this.Order.CustomerDetails.Address){
                this.filteredAddresses = [
                    {Address: this.localStorage.Address},
                    {Address: this.Order.CustomerDetails.Address}
                ];
            }
        } else {
            this.Order.CustomerDetails.CustomerName = '';
            this.Order.CustomerDetails.Mobile = '';
            this.Order.CustomerDetails.Email = '';
            this.Order.CustomerDetails.Address = '';
            this.Order.CustomerDetails.ContactPerson = '';
            this.Order.CustomerDetails.ContactPersonMobile = '';

            this.filteredAddresses = [
                {Address: this.localStorage.Address}
            ];
        }
    }

    onBlurCustomerAddress(){
        if(this.Order.CustomerDetails.Address){
            this.filteredAddresses = [
                {Address: this.localStorage.Address},
                {Address: this.Order.CustomerDetails.Address}
            ];
        }
    }

    onChangeAddress(){
        if(this.addressCtrl.value != 1){
            this.Order.DeliveryLocation = this.addressCtrl.value;
        } else {
            this.Order.DeliveryLocation = null;
        }
    }

    onChangeOrderType(){
        if(this.Order.Products.length > 0){
            let lab = 'Are you sure you want to change the order type because it will delete all selected products?';
            this.notificationService.smartMessageBox({
                title: "Order Type!",
                content: lab, 
                buttons: '[No][Yes]'
            }, (ButtonPressed) => {
                if (ButtonPressed === "Yes") {
                    this.Order.Products = [];
                }
                if (ButtonPressed === "No") {
                    if(this.Order.OrderType == 'Kit Component'){
                        this.Order.OrderType = 'Individual Component';
                    } else {
                        this.Order.OrderType = 'Kit Component';
                    }
                }
            });
        }
    }
    onChangeSalesperson(){
        if(this.salespersonCtrl.value){
            this.Order.SalesPersonID = this.salespersonCtrl.value;
            var ind = this.allSalesPerson.findIndex(x => x._id == this.Order.SalesPersonID);
            if(this.Order.CustomerDetails.Address){
                this.filteredAddresses = [
                    {Address: this.localStorage.Address},
                    {Address: this.allSalesPerson[ind].Address},
                    {Address: this.Order.CustomerDetails.Address}
                ];
            } else {
                this.filteredAddresses = [
                    {Address: this.localStorage.Address},
                    {Address: this.allSalesPerson[ind].Address}
                ];
            }
        } else {
            this.Order.SalesPersonID = null;
        }
    }

}
