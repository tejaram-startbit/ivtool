import {Component, OnInit, ViewChild} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {NotificationService} from '@app/core/services';
import {OrderService} from "@app/features/Order/order.service";
import {Observable} from "rxjs";
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {MatSnackBar} from "@angular/material";
import {CommonService} from "@app/core/common/common.service";
import {Ng4LoadingSpinnerService} from "ng4-loading-spinner";
import {routerTransition} from "@app/shared/utils/animations";

declare var $: any;

@Component({
    selector: 'project-order-dashboard',
    templateUrl: './order-dashboard.component.html',
    animations: [routerTransition]

})
export class OrderDashboardComponent implements OnInit {
    orders: any;
    displayedColumns: string[] = [
        // 'SN',
        'RequestID',
        'ScheduleReturnDate',
        'Action'
    ];
    dataSource: MatTableDataSource<any>;
    isLoadingResults: any;
    isRateLimitReached: any;

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    apiUrl: string;
    localStorage: any = {};

    constructor(private http: HttpClient,
                private notificationService: NotificationService,
                private ordersService: OrderService,
                private snackBar: MatSnackBar,
                private commonService: CommonService,
                private spinnerService: Ng4LoadingSpinnerService) {
        this.apiUrl = this.commonService.getApiUrl();
        this.localStorage = JSON.parse(localStorage.getItem('currentUser'));
        this.getAllOrders();
        this.commonService.sharedVariable = 'Welcome '+ this.localStorage.username;

    }

    ngOnInit() {
    }


    applyFilter(filterValue: string) {
        this.dataSource.filter = filterValue.trim().toLowerCase();

        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    }

    getAllOrders() {
        this.spinnerService.show();
        this.ordersService.getAllSalespersonOrders(this.localStorage.id).subscribe((data: {}) => {
            this.orders = data['result'];
            this.dataSource = new MatTableDataSource(data['result']);
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
            this.spinnerService.hide();
        });
    }


    delete(id) {

        let lab = 'Are you sure delete this record?';
        this.notificationService.smartMessageBox({
            title: "Delete!",
            content: lab,
            buttons: '[No][Yes]'
        }, (ButtonPressed) => {
            if (ButtonPressed === "Yes") {
                this.ordersService.deleteOrder(id).subscribe((data: {}) => {
                    if (data['status']) {
                        this.snackBar.open('Record deleted successfully', 'OK', {
                            duration: 5000,
                            panelClass: ['danger-snackbar'],
                            verticalPosition: 'top'
                        });
                        this.getAllOrders();
                    }
                })
            }
            if (ButtonPressed === "No") {

            }
        });
    }

    private handleError(error: any) {
        // In a real world app, we might use a remote logging infrastructure
        // We'd also dig deeper into the error to get a better message
        let errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg); // log to console instead
        return Observable.throw(errMsg);
    }
}