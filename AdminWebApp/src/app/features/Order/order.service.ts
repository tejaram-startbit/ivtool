import {Injectable} from '@angular/core';

import {HttpClient, HttpHeaders, HttpErrorResponse} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {map, catchError, tap} from 'rxjs/operators';

import {CommonService} from "@app/core/common/common.service";

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json'
    })
};

@Injectable({
    providedIn: 'root'
})
export class OrderService {
    url: string;
    producturl: string;
    DocUrl: string;
    userurl: string;
    currentUser: any;
    constructor(private http: HttpClient, private commService: CommonService) {
        this.url = commService.getApiUrl() + '/order/';
        this.producturl = commService.getApiUrl() + '/product/';
        this.userurl = commService.getApiUrl() + '/user/';
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    }

    private extractData(res: Response) {
        let body = res;
        return body || {};
    }

    getAllOrders(): Observable<any> {
        return this.http.get(this.url + 'getAll/').pipe(
            map(this.extractData));
    }

    getAllByLastOneYear(): Observable<any> {
        return this.http.get(this.url + 'getAllByLastOneYear').pipe(
            map(this.extractData));
    }

    getAllLastMonth(): Observable<any> {
        return this.http.get(this.url + 'getAllLastMonth').pipe(
            map(this.extractData));
    }

    getOneOrder(id): Observable<any> {
        return this.http.get(this.url + 'getOne/' + id).pipe(
            map(this.extractData));
    }

    deleteOrder(id): Observable<any> {
        return this.http.delete<any>(this.url + 'delete/' + id, httpOptions).pipe(
            tap(() => console.log(`deleted role w/ id=${id}`)),
            catchError(this.handleError<any>('deleteOrders'))
        );
    }
    
    dispatchedOrder(OrderID, data, excelFile, pdfFile): Observable<any> {
        const formData = new FormData();
        formData.append('data', JSON.stringify(data));
        formData.append('OrderID', OrderID);
        if (excelFile){
            formData.append('excelFile', excelFile);
        }
        if (pdfFile) {
            formData.append('pdfFile', pdfFile);
        }
        return this.http.post<any>(this.url + 'dispatchedOrder', formData).pipe(
            tap(() => console.log(`added role w/ id=${data._id}`)),
            catchError(this.handleError<any>('addOrder'))
        );
    }

    orderDelivered(CurrentUserID, OrderID): Observable<any> {
        return this.http.get(this.url + 'orderDelivered/' + CurrentUserID + '/' + OrderID).pipe(
            map(this.extractData));
    }

    approveOrder(OrderID, ApproverID): Observable<any> {
        return this.http.post<any>(this.url + 'approveOrder/' + OrderID, JSON.stringify({ApproverID: ApproverID, Status: 2}), httpOptions).pipe(
            tap(() => console.log(`approveOrder`)),
            catchError(this.handleError<any>('approveOrder'))
        );
    }

    disapproveOrder(OrderID, ApproverID, ReasonMessage): Observable<any> {
        return this.http.post<any>(this.url + 'disapproveOrder/' + OrderID, JSON.stringify({ApproverID: ApproverID, ReasonMessage: ReasonMessage, Status: 3}), httpOptions).pipe(
            tap(() => console.log(`disapproveOrder`)),
            catchError(this.handleError<any>('disapproveOrder'))
        );
    }

    declineOrder(OrderID, ApproverID, ReasonMessage): Observable<any> {
        return this.http.post<any>(this.url + 'declineOrder/' + OrderID, JSON.stringify({ApproverID: ApproverID, ReasonMessage: ReasonMessage, Status: 10}), httpOptions).pipe(
            tap(() => console.log(`declineOrder`)),
            catchError(this.handleError<any>('declineOrder'))
        );
    }

    updateScheduledReturnDate(OrderID, CurrentUser, ScheduledReturnDate): Observable<any> {
        return this.http.post<any>(this.url + 'updateScheduledReturnDate/' + OrderID, JSON.stringify({CurrentUser: CurrentUser, ScheduledReturnDate: ScheduledReturnDate}), httpOptions).pipe(
            tap(() => console.log(`updateScheduledReturnDate`)),
            catchError(this.handleError<any>('updateScheduledReturnDate'))
        );
    }

    getDispatcheOrderDetail(OrderID): Observable<any> {
        return this.http.get(this.url + 'getDispatcheOrderDetail/' + OrderID).pipe(
            map(this.extractData));
    }

    getOrderTimeLine(OrderID): Observable<any> {
        return this.http.get(this.url + 'getOrderTimeLine/' + OrderID).pipe(
            map(this.extractData));
    }

    getNotificationByUser(UserID): Observable<any> {
        return this.http.get(this.url + 'getNotificationByUser/' + UserID).pipe(
            map(this.extractData));
    }

    readNotificationByUser(NotificationID, UserID): Observable<any> {
        return this.http.post<any>(this.url + 'readNotificationByUser', JSON.stringify({NotificationID: NotificationID, UserID: UserID}), httpOptions).pipe(
            tap(() => console.log(`readNotificationByUser`)),
            catchError(this.handleError<any>('readNotificationByUser'))
        );
    }

    getAllOrdersByRole(Role, UserID): Observable<any> {
        return this.http.post<any>(this.url + 'getAllOrdersByRole/', {Role: Role, UserID: UserID}, httpOptions).pipe(
            tap(() => console.log(`added`)),
            catchError(this.handleError<any>('addRole'))
        );
    }

    orderReturnAccept(OrderID): Observable<any> {
        return this.http.get(this.url + 'orderReturnAccept/' + this.currentUser.id + '/' + OrderID).pipe(
            map(this.extractData));
    }

    getAllSP(): Observable<any> {
        return this.http.get(this.userurl + 'getAllSalesperson').pipe(
            map(this.extractData));
        // return this.http.get(this.url + 'getAllSP/' + salesid).pipe(
        //     map(this.extractData));
    }

    transferedOrder(OrderID, data): Observable<any> {
        return this.http.post<any>(this.url + 'transferOrder/' + OrderID, JSON.stringify(data), httpOptions).pipe(
            tap(() => console.log(`transferOrder`)),
            catchError(this.handleError<any>('transferOrder'))
        );
    }

    transferRequestOrder(OrderID, data): Observable<any> {
        return this.http.post<any>(this.url + 'transferRequestOrder/' + OrderID, JSON.stringify(data), httpOptions).pipe(
            tap(() => console.log(`transferRequestOrder`)),
            catchError(this.handleError<any>('transferRequestOrder'))
        );
    }

    getOrderAcknowledgeDetail(OrderID): Observable<any> {
        return this.http.get(this.url + 'getOrderAcknowledgeDetail/' + OrderID).pipe(
            map(this.extractData));
    }

    getTransferRequest(OrderID): Observable<any> {
        return this.http.get(this.url + 'getTransferRequest/' + OrderID).pipe(
            map(this.extractData));
    }

    authenticationLogin(Username, Password): Observable<any> {
        return this.http.post<any>(this.url + 'authenticationLogin/', {Username: Username, Password: Password}, httpOptions).pipe(
            tap(() => console.log(`authenticationLogin`)),
            catchError(this.handleError<any>('authenticationLogin'))
        );
    }

    updateProductDetail(OrderID, data, currentUser): Observable<any> {
        return this.http.post<any>(this.url + 'updateProductDetail/' + OrderID, {data: data, currentUser: currentUser}, httpOptions).pipe(
            tap(() => console.log(`updateProductDetail`)),
            catchError(this.handleError<any>('updateProductDetail'))
        );
    }

    updateDispatchOrder(OrderID, data, files): Observable<any> {
        const formData = new FormData();
        formData.append('data', JSON.stringify(data));
        formData.append('OrderID', OrderID);
        for (let file of files){
            formData.append('file', file);
        }
        return this.http.post<any>(this.url + 'updateFinalProductDetail', formData).pipe(
            tap(() => console.log(`added role w/ id=${data._id}`)),
            catchError(this.handleError<any>('addOrder'))
        );
    }

    transferOrderRequest(OrderID, currentUser): Observable<any> {
        return this.http.get(this.url + 'getOrderAcknowledgeDetail/' + currentUser + '/' + OrderID).pipe(
            map(this.extractData));
    }

    getAllSalespersonOrders(id): Observable<any> {
        return this.http.get(this.url + 'sales/getAll/' + id).pipe(
            map(this.extractData));
        // return this.http.get(this.url + 'getAll/').pipe(
        //         map(this.extractData));
    }







    getAllProducts(): Observable<any> {
        return this.http.get(this.producturl + 'getAll/').pipe(
            map(this.extractData));
    }

    getLastOrderRequestID(): Observable<any> {
        return this.http.get(this.url + 'getLastOrderRequestID').pipe(
            map(this.extractData));
    }
    getAllCustomerNSP(): Observable<any> {
        return this.http.get(this.url + 'getAllCustomerNSP/').pipe(
            map(this.extractData));
    }

    getOneOrderByRequestID(id, salespersonid): Observable<any> {
        return this.http.get(this.url + 'getOneOrderByRequestID/' + id + '/' + salespersonid, httpOptions).pipe(
            map(this.extractData));
    }

    addOrder(data): Observable<any> {
        const formData = new FormData();
        formData.append('data', JSON.stringify(data));
        return this.http.post<any>(this.url + 'create', formData).pipe(
            tap(() => console.log(`added role w/ id=${data._id}`)),
            catchError(this.handleError<any>('addorder'))
        );
    }

    updateOrder(data, id): Observable<any> {
        const formData = new FormData();
        formData.append('data', JSON.stringify(data));
        return this.http.post<any>(this.url + 'update/' + id, formData ).pipe(
            tap(() => console.log(`added role w/ id=${data._id}`)),
            catchError(this.handleError<any>('addorder'))
        );
    }

    addTransfer(data, CustomerID, currentUserID): Observable<any> {
        const formData = new FormData();
        formData.append('data', JSON.stringify(data));
        data.currentUserID = currentUserID;
        data.CustomerID = CustomerID;
        return this.http.post<any>(this.url + 'transferRequestOrder/' + data._id, data, httpOptions).pipe(
            tap(() => console.log(`added role w/ id=${data._id}`)),
            catchError(this.handleError<any>('addorder'))
        );
    }

    returnOrder(data, ReturnProducts, id): Observable<any> {

        return this.http.post<any>(this.url + 'returnOrder/' + id, {data: data, ReturnProducts: ReturnProducts}, httpOptions).pipe(
            tap(() => console.log(`added role w/ id=${data._id}`)),
            catchError(this.handleError<any>('addorder'))
        );
    }


    addAcknowledge(data, file, id): Observable<any> {
        const formData = new FormData();
        formData.append('data', JSON.stringify(data));
        formData.append('orderid', id);
        if(file){
            formData.append('file', file);
        }

        return this.http.post<any>(this.url + 'acknoledgeCreate', formData).pipe(
            tap(() => console.log(`added role w/ id=${data._id}`)),
            catchError(this.handleError<any>('addorder'))
        );
    }

    updateMyOrder(id, data, file): Observable<any> {
        const formData = new FormData();
        formData.append('data', JSON.stringify(data));
        if(file){
            formData.append('file', file);
        }
        return this.http.post<any>(this.url + 'update/' + id, formData).pipe(
            tap(() => console.log(`updated role w/ id=${data._id}`)),
            catchError(this.handleError<any>('updateorders'))
        );
    }

    getOrderImage(url): Observable<any> {
        return this.http.get(this.commService.getApiUrl() + url, {responseType: 'blob' as 'json'}).pipe(
            map(this.extractData));
    }

    getAllCustomer(): Observable<any> {
        return this.http.get(this.userurl + 'getAllCustomer').pipe(
            map(this.extractData));
    }

    getAllCustomerSalesPersonAddress(): Observable<any> {
        return this.http.get(this.userurl + 'getAllCustomerSalesPersonAddress').pipe(
            map(this.extractData));
    }

    getOrderReturnData(id): Observable<any> {
        return this.http.get(this.url + 'getOrderReturnData/' + id).pipe(
            map(this.extractData));
    }

    readAllNotification(NotificationArr, UserID): Observable<any> {
        return this.http.post<any>(this.url + 'readAllNotification/' + UserID, JSON.stringify({data: NotificationArr}), httpOptions).pipe(
            tap(() => console.log(`readAllNotification`)),
            catchError(this.handleError<any>('readAllNotification'))
        );
    }

    private handleError<T>(operation = 'operation', result?: any) {
        return (error: any): Observable<any> => {

            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // TODO: better job of transforming error for home consumption
            console.log(`${operation} failed: ${error.message}`);
            const errorData = {
                status: false,
                message: 'Server Error'
            };
            // Let the app keep running by returning an empty result.
            return of(errorData);
        };
    }
}
