import { Component, OnInit, ViewChild, AfterViewInit, Inject } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { NotificationService } from "@app/core/services";
import { MatSnackBar } from "@angular/material";
import { OrderService } from "@app/features/Order/order.service";
import { FormBuilder, FormGroup, Validators, FormControl } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { Location } from "@angular/common";
import { Observable } from "rxjs";
import { Ng4LoadingSpinnerService } from "ng4-loading-spinner";

import { MatSelect } from "@angular/material";

import { ReplaySubject, Subject } from "rxjs";
import { take, takeUntil } from "rxjs/operators";
import {CommonService} from "@app/core/common/common.service";
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {UserService} from "@app/features/User/user.service";


export interface DialogData {
  animal: string;
  name: string;
}

@Component({
  selector: "project-order-transferform",
  templateUrl: "./order-transferform.component.html"
})
export class OrderTransferformComponent implements OnInit {
  ID: any;
  name: any;
  States: any;
  AckForm: FormGroup;
  showSpinner: boolean = false;
  ProductAdded: boolean = false;
  ImageData: any;
  showForm: boolean = false;
  validationAckFormDetailOptions = {};
  Products2: any = [];
  Products: any = [];
  OrderProducts: any = [];
  Order: any = {};
  localStorage: any;
  search: any;
  ServiceType: any = 'Instrument';
  month: any;
  date: any;
  year: any;
  dmonth: any;
  ddate: any;
  dyear: any;
  Status : any;
  animal: string;
  searchinput: boolean = false;
  allUsers: any = [];
  CustomerID: any;
  mobNumberPattern = "^(0)?[0-9]{10}$";

    public addressCtrl: FormControl = new FormControl();
    public addressFilterCtrl: FormControl = new FormControl();
    public filteredAddresses: any = [];
    allDeliveryLocationes: any = [];

    public customerCtrl: FormControl = new FormControl(1);
    public customerFilterCtrl: FormControl = new FormControl();
    public filteredCustomers: any = [];
  constructor(
    private http: HttpClient,
    private router: Router,
    private fb: FormBuilder,
    private location: Location,
    private route: ActivatedRoute,
    private ordersService: OrderService,
    private notificationService: NotificationService,
    private snackBar: MatSnackBar,
    private spinnerService: Ng4LoadingSpinnerService,
    private commService: CommonService,
    public dialog: MatDialog,
    private userService: UserService
  ) {
      this.getAllDeliveryLocationes();

      this.AckForm = fb.group({
        Remarks: ["", [Validators.required]],
        Picture: ["", [Validators.required]]
      });
      this.validationAckFormDetailOptions = {
        // Rules for form validation
        rules: {
          Picture: {
            required: true
          },
          Remarks: {
            required: true
          }
        },

        // Messages for form validation
        messages: {
          Picture: {
            required: "Please select image"
          },
          Remarks: {
            required: "Please enter your remarks"
          }
        },
        submitHandler: this.onSubmit
      };

    

    // this.ordersService.getAllProducts().subscribe((data: {}) => {
    //   for (let i = 0; i < data["result"].length; i++) {
    //     this.Products2.push({
    //       ProductID: data["result"][i]._id,
    //       ProductName: data["result"][i].Name,
    //       ProductQuantity: 0
    //     });
    //   }
    // });

    this.localStorage = JSON.parse(localStorage.getItem("currentUser"));
    this.commService.sharedVariable = 'Transfer';
    this.Order = {
      CustomerDetails : {
        CustomerName: '',
        Mobile:'',
        Email:'',
        Address:'',
        ContactPerson:'',
        ContactPersonMobile:''
      },
      Products: [],
      ScheduleReturnDate: '',
      ServiceType: 'Instrument'
    }
    this.getAllCustomerNSP();
    // this.ID = this.route.params["value"].id;
    // if (this.ID !== "1") {
    //   this.getOne(this.ID, this.localStorage.id);
    // }
  }

  onSubmit() {
    console.log('\n', 'submit handler for validated form', '\n\n')
  }

  private _onDestroy = new Subject<void>();

  ngOnInit() {
    this.addressFilterCtrl.valueChanges
    .pipe(takeUntil(this._onDestroy))
    .subscribe(() => {
        this.filterAddresses();
    });

    this.customerFilterCtrl.valueChanges
        .pipe(takeUntil(this._onDestroy))
        .subscribe(() => {
            this.filterCustomers();
        });
  }

  private filterAddresses() {
    // get the search keyword
    let search = this.addressFilterCtrl.value;
    if (!search) {
        this.filteredAddresses = this.allDeliveryLocationes.slice();
        return;
    } else {
        search = search.toLowerCase();
    }
    // filter the banks
    this.filteredAddresses = this.allDeliveryLocationes.filter(
        x => x.Address.toLowerCase().indexOf(search) > -1
    );
  }

  private filterCustomers() {
    // get the search keyword
    let search = this.customerFilterCtrl.value;
    if (!search) {
        this.filteredCustomers = this.allUsers.slice();
        return;
    } else {
        search = search.toLowerCase();
    }
    // filter the banks
    this.filteredCustomers = this.allUsers.filter(
        x => x.FirstName.toLowerCase().indexOf(search) > -1 ||
        x.LastName.toLowerCase().indexOf(search) > -1
    );
}

  getAllDeliveryLocationes(){
    this.userService.getAllCustomerSalesPersonAddress().subscribe(data => {
        if (data['status']) {
            this.allDeliveryLocationes = data['result'];
            this.filteredAddresses = data['result'];
        }
    }, err => {
        this.snackBar.open('something went error', 'OK', {
            duration: 5000,
            panelClass: ['danger-snackbar'],
            verticalPosition: 'top'
        });
    });
}

onChangeAddress(){
  if(this.addressCtrl.value != 1){
          this.Order.DeliveryLocation = this.addressCtrl.value;
  } else {
    this.Order.DeliveryLocation = '';
  }
}

onChangeCustomers(){
  this.CustomerID = this.customerCtrl.value;
}

  getAllCustomerNSP(){
    const salesid = this.localStorage.id;
    this.ordersService.getAllSP().subscribe(data => {
        if (data['status']) {
            this.allUsers = data['result'];
            this.filteredCustomers = data['result'];
            // this.allUsers = this.allUsers.filter(function( obj ) {
            //   return obj._id != salesid;
            // });
            //getAllCustomerNSP
        }
    }, err => {
            console.log(err);
    })
}


  searchfun(){
    if(this.search){
      this.searchinput = true;
      if(this.ServiceType == 'Demo'){
        this.getOne('DR-'+this.search, this.localStorage.id);
      }else {
        this.getOne('IR-'+this.search, this.localStorage.id);
      }
    }
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(DialogOverviewExample4Dialog, {
      width: '250px',
      data: {name: this.name, animal: this.animal}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.animal = result;
    });
  }

  onSubmitForm() {
      if (this.CustomerID == undefined) {
        this.snackBar.open('Please select Dealer', 'OK', {
          duration: 5000,
          panelClass: ['danger-snackbar'],
          verticalPosition: 'top'
        });
          return 0;
      }

      if (!this.Order.DeliveryLocation) {
        this.snackBar.open('Please select delivery location', 'OK', {
          duration: 5000,
          panelClass: ['danger-snackbar'],
          verticalPosition: 'top'
        });
          return 0;
      }

      var flag = true;
      this.Order.Products.map(x => {
        if(x.value){
          flag = false;
        }
      })

      if(flag){
        this.snackBar.open('Please select at least one product for transfer', 'OK', {
          duration: 5000,
          panelClass: ['danger-snackbar'],
          verticalPosition: 'top'
        });
        return;
      }
      
      this.showSpinner = true;
      this.ordersService.addTransfer(this.Order, this.CustomerID, this.localStorage.id).subscribe(data => {
          if (data['status']) {
              const dialogRef = this.dialog.open(DialogOverviewExample8Dialog, {
                width: '250px',
                data: {name: this.name, animal: this.animal}
              });
          
              dialogRef.afterClosed().subscribe(result => {
                this.animal = result;
              });
          } else {
              this.snackBar.open(data['message'], 'OK', {
                  duration: 5000,
                  panelClass: ['danger-snackbar'],
                  verticalPosition: 'top'
              });
          }
          this.showSpinner = false;
      }, err => {
          this.snackBar.open('something went error', 'OK', {
              duration: 5000,
              panelClass: ['danger-snackbar'],
              verticalPosition: 'top'
          });
      });
  }


  onFileChange(event) {
    const reader = new FileReader();

    if (event.target.files && event.target.files.length) {
        const [file] = event.target.files;
        reader.readAsDataURL(file);
        const fileName = event.target.files[0].name;
        const lastIndex = fileName.lastIndexOf('.');
        const extension = fileName.substr(lastIndex + 1);
        if (extension.toLowerCase() === 'jpg' || extension.toLowerCase() === 'jpeg' || extension.toLowerCase() === 'png') {
            reader.onload = () => {
                this.ImageData = event.target.files[0];
            };
        } else {
            this.snackBar.open('Invalid selected file', 'OK', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        }
    }
}

focusOutFunction(e){
  e.preventDefault();
  if(this.search){
    this.searchinput = true;
    if(this.ServiceType == 'Demo'){
      this.getOne('DR-'+this.search, this.localStorage.id);
    }else {
      this.getOne('IR-'+this.search, this.localStorage.id);
    }
  }
}
ServiceTypeChange() {
  this.search = null;
  this.Order = {
    CustomerDetails : {
      CustomerName: '',
      Mobile:'',
      Email:'',
      Address:'',
      ContactPerson:'',
      ContactPersonMobile:''
    },
    Products: [],
    ScheduleReturnDate: '',
    ServiceType: 'Instrument'
  }
  this.year = null;
  this.month = null;
  this.date = null;
  this.Status = null;
}

update(){
 if(this.month == undefined && this.date == undefined && this.year == undefined){
    return;
 }

 this.Order.ScheduleReturnDate = this.year + '-' + this.month + '-' + this.date;

  this.ordersService.updateOrder(this.Order , this.Order._id).subscribe(
    data => {
      if (data["status"]) {
        this.snackBar.open("Record created successfully", "OK", {
          duration: 5000,
          panelClass: ["success-snackbar"],
          verticalPosition: "top"
        });
        this.showSpinner = false;
        this.router.navigate(["/dashboard"]);
      }
    },
    err => {
      this.snackBar.open("something went error", "OK", {
        duration: 5000,
        panelClass: ["danger-snackbar"],
        verticalPosition: "top"
      });
    }
  );

}

getOneOrder(id){
    this.spinnerService.show();
    this.ordersService.getOneOrder(id).subscribe(
      data => {
        if (data["result"].length > 0 && data["status"] == true) {
          this.OrderProducts = data["result"][0].Products;
         
          this.Order = data["result"][0];
          const sdate = this.Order.ScheduleReturnDate;
          var d = new Date(sdate);
          this.month = d.getMonth() + 1;
          if(this.Order.ScheduleReturnDate){
            if(this.month < 10){
              this.month = '0' + this.month;
            }

            this.date = d.getDate();
            if(this.date < 10){
              this.date = '0' + this.date;
            }
            this.year = d.getFullYear();
          }
          if(this.Order.Status == 1){
            this.Status = 'Pending';
          }else if(this.Order.Status == 2){
            this.Status = 'Approved';
          }else if(this.Order.Status == 3){
            this.Status = 'Disapproved';
          }else if(this.Order.Status == 4){
            this.Status = 'Dispatched';
          }else if(this.Order.Status == 5){
            this.Status = 'Delivered';
          }else if(this.Order.Status == 6){
            this.Status = 'Acknowledged';
          }else if(this.Order.Status == 7){
            this.Status = 'Return';
          }else if(this.Order.Status == 8){
            this.Status = 'Transfer';
          }else if(this.Order.Status == 9){
            this.Status = 'Return Approved';
          }

        }
        else {
          this.snackBar.open("Not found any result", "OK", {
            duration: 5000,
            panelClass: ["danger-snackbar"],
            verticalPosition: "top"
          });
        }
        this.spinnerService.hide();
      },
      err => {
        this.snackBar.open("something went error", "OK", {
          duration: 5000,
          panelClass: ["danger-snackbar"],
          verticalPosition: "top"
        });
      }
    );
  }

  getOne(id, salespersonid) {
    this.spinnerService.show();
    this.ordersService.getOneOrderByRequestID(id, salespersonid).subscribe(
      data => {
        this.searchinput = false;
        if (data["result"].length > 0 && data["status"] == true) {
          //if(data["result"][0].Status == 6 || data["result"][0].Status == 8){
          if(data["result"][0].SalesPersonID == this.localStorage.id){
            if(data["result"][0].Status != 9){

              this.Order = data["result"][0];

              if(this.Order.Status != 6){
                this.snackBar.open("Order is not transferable now as it is not acknowledged", "OK", {
                  duration: 5000,
                  panelClass: ["danger-snackbar"],
                  verticalPosition: "top"
                });
              }

              this.Order.Products.map(x => {
                x.DefaultQuantity = x.Quantity;
                x.value = true;
              })
              this.CustomerID = data['result'][0].SalesPersonID;
              this.customerCtrl.setValue(this.CustomerID);
              var ind = this.allDeliveryLocationes.findIndex(x => x.Address == this.Order.DeliveryLocation);
              if(ind != -1){
                  this.addressCtrl.setValue(this.Order.DeliveryLocation);
              } else {
                  this.addressCtrl.setValue('1')
              }

              this.OrderProducts = data["result"][0].Products;
            
              const sdate = this.Order.ScheduleReturnDate;
              var d = new Date(sdate);
              if(this.Order.ScheduleReturnDate){
                this.month = d.getMonth() + 1;
                if(this.month < 10){
                  this.month = '0' + this.month;
                }

                this.date = d.getDate();
                if(this.date < 10){
                  this.date = '0' + this.date;
                }
                this.year = d.getFullYear();
              }
              if(this.Order.Status == 1){
                this.Status = 'Pending';
              }else if(this.Order.Status == 2){
                this.Status = 'Approved';
              }else if(this.Order.Status == 3){
                this.searchinput = true;
                this.Status = 'Disapproved';
              }else if(this.Order.Status == 4){
                this.Status = 'Dispatched';
              }else if(this.Order.Status == 5){
                this.Status = 'Delivered';
              }else if(this.Order.Status == 6){
                this.Status = 'Acknowledged';
              }else if(this.Order.Status == 7){
                this.Status = 'Return';
              }else if(this.Order.Status == 8){
                this.Status = 'Transfer';
              }else if(this.Order.Status == 9){
                this.Status = 'Return Approved';
              }
            }else {
              this.snackBar.open("Order is not transferable now as it is Retured", "OK", {
                duration: 5000,
                panelClass: ["danger-snackbar"],
                verticalPosition: "top"
              });
            }
          } else{
            this.snackBar.open("Order not associated with you", "OK", {
              duration: 5000,
              panelClass: ["danger-snackbar"],
              verticalPosition: "top"
            });
            this.Order = {
              CustomerDetails : {
                CustomerName: '',
                Mobile:'',
                Email:'',
                Address:'',
                ContactPerson:'',
                ContactPersonMobile:''
              },
              Products: [],
              ScheduleReturnDate: '',
              ServiceType: 'Instrument'
            }
            this.year = null;
            this.month = null;
            this.date = null;
            this.Status = null;
          }

        }
        else {
          this.snackBar.open("Order not found", "OK", {
            duration: 5000,
            panelClass: ["danger-snackbar"],
            verticalPosition: "top"
          });
          this.Order = {
            CustomerDetails : {
              CustomerName: '',
              Mobile:'',
              Email:'',
              Address:'',
              ContactPerson:'',
              ContactPersonMobile:''
            },
            Products: [],
            ScheduleReturnDate: '',
            ServiceType: 'Instrument'
          }
        }
        this.spinnerService.hide();
      },
      err => {
        this.snackBar.open("something went error", "OK", {
          duration: 5000,
          panelClass: ["danger-snackbar"],
          verticalPosition: "top"
        });
        this.Order = {
          CustomerDetails : {
            CustomerName: '',
            Mobile:'',
            Email:'',
            Address:'',
            ContactPerson:'',
            ContactPersonMobile:''
          },
          Products: [],
          ScheduleReturnDate: '',
          ServiceType: 'Instrument'
        }
      }
    );
  }

  openAck() {
    this.router.navigate(["/orders/ack/"+this.Order._id]);
  }

  openTransfer() {
    this.router.navigate(["/orders/transfer/"+this.Order._id]);
  }

  openReturn() {
    this.router.navigate(["/orders/return/"+this.Order._id]);
  }

  onChangeQuantity(ProductCode){
    var index = this.Order.Products.findIndex(x => x.ProductCode == ProductCode);
    if(index != -1 && this.Order.Products[index].DefaultQuantity < this.Order.Products[index].Quantity){      
      this.Order.Products[index].Quantity = this.Order.Products[index].DefaultQuantity;
      this.snackBar.open("You can transfer max " + this.Order.Products[index].DefaultQuantity + " Products", "OK", {
        duration: 5000,
        panelClass: ["danger-snackbar"],
        verticalPosition: "top"
      });
    } else if(isNaN(this.Order.Products[index].Quantity)){
      this.snackBar.open("Invalid product quantity", "OK", {
        duration: 5000,
        panelClass: ["danger-snackbar"],
        verticalPosition: "top"
      });
      this.Order.Products[index].Quantity = this.Order.Products[index].DefaultQuantity;
    }
  }

  onChangeChackbox(ProductCode){
    var index = this.Order.Products.findIndex(x => x.ProductCode == ProductCode);
    if(!(index != -1 && this.Order.Products[index].value)){
      this.Order.Products[index].Quantity = this.Order.Products[index].DefaultQuantity;
    }
  }

  // cancel() {
  //   this.location.back();
  // }

}

@Component({
  selector: 'dialog-overview-example4-dialog',
  templateUrl: 'dialog-overview-example-dialog.html',
})
export class DialogOverviewExample4Dialog {

  constructor(
    public dialogRef: MatDialogRef<DialogOverviewExample4Dialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

}

@Component({
  selector: 'dialog-overview-example1-dialog',
  templateUrl: 'dialog-overview-example1-dialog.html',
})
export class DialogOverviewExample8Dialog {

  constructor( private router: Router,
    public dialogRef: MatDialogRef<DialogOverviewExample8Dialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {}

  ok(): void {
    this.dialogRef.close();
    this.router.navigate(["/dashboard"]);
  }

}