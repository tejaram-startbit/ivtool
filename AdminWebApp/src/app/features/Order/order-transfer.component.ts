import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {NotificationService} from '@app/core/services';
import {MatSnackBar} from "@angular/material";
import {OrderService} from "@app/features/Order/order.service";
import {FormBuilder, FormGroup, Validators, FormControl} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {Location} from "@angular/common";
import {Observable} from "rxjs";
import {Ng4LoadingSpinnerService} from "ng4-loading-spinner";
import {UserService} from "@app/features/User/user.service";

import {ReplaySubject, Subject} from "rxjs";
import {take, takeUntil} from "rxjs/operators";

@Component({
    selector: 'project-order-transfer',
    templateUrl: './order-transfer.component.html',
})
export class OrderTransferComponent implements OnInit {
    ID: any;
    name: any;
    States: any;
    OrderTransferForm: FormGroup;
    showSpinner: boolean = false;
    selectedPdfFile: any;
    selectedExcelFile: any;
    showTransferForm: boolean = false;
    validationOrderTransferFormDetailOptions = {};
    currentUser: any;
    orderDetail: any;
    allSalesPerson: any = [];
    allDeliveryLocationes: any = [];
    productsForTransfer = [];

    public addressCtrl: FormControl = new FormControl(1);
    public addressFilterCtrl: FormControl = new FormControl();
    public filteredAddresses: any = [];

    public customerCtrl: FormControl = new FormControl(1);
    public customerFilterCtrl: FormControl = new FormControl();
    public filteredCustomers: any = [];

    constructor(private http: HttpClient,
                private router: Router,
                private fb: FormBuilder,
                private location: Location,
                private route: ActivatedRoute,
                private orderService: OrderService,
                private notificationService: NotificationService,
                private snackBar: MatSnackBar,
                private userService: UserService,
                private spinnerService: Ng4LoadingSpinnerService) {
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.OrderTransferForm = fb.group({
            CustomerID: ['0', [Validators.required]],
            CustomerName: [''],
            Mobile: [''],
            Email: [''],
            Address: [''],
            ContactPerson: [''],
            ContactPersonMobile: [''],
            SelectDeliveryLocation: [''],
            DeliveryLocation: ['0', [Validators.required]]
        });

        this.getAllSalesPerson();
        this.getAllDeliveryLocationes();

        this.validationOrderTransferFormDetailOptions = {
            // Rules for form validation
            rules: {
                CustomerID: {
                    required: true
                },
                DeliveryLocation: {
                    required: true
                }
            },

            // Messages for form validation
            messages: {
                CustomerID: {
                    required: 'dealer is required'
                },
                DeliveryLocation: {
                    required: 'please select delivery address'
                }
            },
            submitHandler: this.onSubmit

        };
        this.ID = this.route.params['value'].id;
        this.showTransferForm = true;
    }

    onSubmit() {
        console.log('\n', 'submit handler for validated form', '\n\n')
    }

    private _onDestroy = new Subject<void>();

    ngOnInit() {
        this.addressFilterCtrl.valueChanges
        .pipe(takeUntil(this._onDestroy))
        .subscribe(() => {
            this.filterAddresses();
        });

        this.customerFilterCtrl.valueChanges
        .pipe(takeUntil(this._onDestroy))
        .subscribe(() => {
            this.filterCustomers();
        });
    }

    private filterAddresses() {
        // get the search keyword
        let search = this.addressFilterCtrl.value;
        if (!search) {
            this.filteredAddresses = this.allDeliveryLocationes.slice();
            return;
        } else {
            search = search.toLowerCase();
        }
        // filter the banks
        this.filteredAddresses = this.allDeliveryLocationes.filter(
            x => x.Address.toLowerCase().indexOf(search) > -1
        );
    }

    private filterCustomers() {
        // get the search keyword
        let search = this.customerFilterCtrl.value;
        if (!search) {
            this.filteredCustomers = this.allSalesPerson.slice();
            return;
        } else {
            search = search.toLowerCase();
        }
        // filter the banks
        this.filteredCustomers = this.allSalesPerson.filter(
            x => x.FirstName.toLowerCase().indexOf(search) > -1 ||
            x.LastName.toLowerCase().indexOf(search) > -1
        );
    }

    getOrderDetail(){
        this.spinnerService.show();
        this.orderService.getOneOrder(this.ID).subscribe(data => {
            if (data['status']) {
                this.orderDetail = data['result'];
                if(this.orderDetail){
                    this.productsForTransfer = this.orderDetail.Products.filter(x => x.ProductConsumable == 'No');

                    this.OrderTransferForm.patchValue({
                        CustomerID: this.orderDetail.SalesPersonID._id,
                        DeliveryLocation: this.orderDetail.DeliveryLocation ? this.orderDetail.DeliveryLocation : '0',
                        CustomerName: this.orderDetail.CustomerDetails.CustomerName ? this.orderDetail.CustomerDetails.CustomerName : '',
                        Mobile: this.orderDetail.CustomerDetails.Mobile ? this.orderDetail.CustomerDetails.Mobile : '',
                        Email: this.orderDetail.CustomerDetails.Email ? this.orderDetail.CustomerDetails.Email : '',
                        Address: this.orderDetail.CustomerDetails.Address ? this.orderDetail.CustomerDetails.Address : '',
                        ContactPerson: this.orderDetail.CustomerDetails.ContactPerson ? this.orderDetail.CustomerDetails.ContactPerson : '',
                        ContactPersonMobile: this.orderDetail.CustomerDetails.ContactPersonMobile ? this.orderDetail.CustomerDetails.ContactPersonMobile : ''
                    });
                    var ind = this.allDeliveryLocationes.findIndex(x => x.Address == this.orderDetail.DeliveryLocation);
                    if(ind != -1){
                        this.addressCtrl.setValue(this.orderDetail.DeliveryLocation);
                    } else {
                        this.addressCtrl.setValue('1')
                    }

                    this.customerCtrl.setValue(this.orderDetail.SalesPersonID._id);
                }
            }
            this.spinnerService.hide();
        }, err => {
            this.snackBar.open('something went error', 'OK', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        });
    }

    getAllSalesPerson(){
        this.userService.getAllSalesperson().subscribe(data => {
            if (data['status']) {
                this.allSalesPerson = data['result'];
                this.filteredCustomers = data['result'];
            }
        }, err => {
            this.snackBar.open('something went error', 'OK', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        });
    }

    getAllDeliveryLocationes(){
        this.userService.getAllCustomerSalesPersonAddress().subscribe(data => {
            if (data['status']) {
                this.allDeliveryLocationes = data['result'];
                this.filteredAddresses = data['result'];
                this.getOrderDetail();
            }
        }, err => {
            this.snackBar.open('something went error', 'OK', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        });
    }

    onSubmitTransferForm() {
        if (this.OrderTransferForm.invalid) {
            return 0;
        }

        if(!this.OrderTransferForm.value.CustomerID && this.OrderTransferForm.value.CustomerID == '0'){
            console.log('cst')
            return;
        }
        if(!this.OrderTransferForm.value.DeliveryLocation && this.OrderTransferForm.value.DeliveryLocation == '0'){
            console.log('dlv')
            return;
        }
        this.createNew();
    }

    createNew() {
        if(this.OrderTransferForm.value.CustomerID == '0'){
            return;
        }
        this.showSpinner = true;
        this.OrderTransferForm.value.currentUserID = this.currentUser.id;
        this.OrderTransferForm.value.CustomerDetails = {
            CustomerName: this.OrderTransferForm.value.CustomerName,
            Mobile: this.OrderTransferForm.value.Mobile,
            Email: this.OrderTransferForm.value.Email,
            Address: this.OrderTransferForm.value.Address,
            ContactPerson: this.OrderTransferForm.value.ContactPerson,
            ContactPersonMobile: this.OrderTransferForm.value.ContactPersonMobile
        };
        this.orderService.transferedOrder(this.ID, this.OrderTransferForm.value).subscribe(data => {
            if (data['status']) {
                this.snackBar.open('Order transfered successfully', 'OK', {
                    duration: 5000,
                    panelClass: ['success-snackbar'],
                    verticalPosition: 'top'
                });
                this.router.navigate(["/dashboard"]);
            } else {
                this.showSpinner = false;
                this.snackBar.open(data['message'], 'OK', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
            }
        }, err => {
            this.showSpinner = false;
            this.snackBar.open('something went error', 'OK', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        });
    }

    cancel() {
        this.router.navigate(["/dashboard"]);
    }

    onChangeAddress(){
        if(this.addressCtrl.value != 1){
            this.OrderTransferForm.patchValue({
                DeliveryLocation: this.addressCtrl.value
            })
        } else {
            this.OrderTransferForm.patchValue({
                DeliveryLocation: ''
            })
        }
    }

    onChangeCustomers(){
        if(this.customerCtrl.value != 1){
            this.OrderTransferForm.patchValue({
                CustomerID: this.customerCtrl.value
            })
        } else {
            this.OrderTransferForm.patchValue({
                CustomerID: ''
            })
        }
    }
}