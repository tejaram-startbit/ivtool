import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {NotificationService} from '@app/core/services';
import {MatSnackBar} from "@angular/material";
import {OrderService} from "@app/features/Order/order.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {Location} from "@angular/common";
import {Observable} from "rxjs";
import {Ng4LoadingSpinnerService} from "ng4-loading-spinner";

@Component({
    selector: 'project-order-update',
    templateUrl: './order-update.component.html',
})
export class OrderUpdateComponent implements OnInit {
    ID: any;
    name: any;
    States: any;
    OrderUpdateForm: FormGroup;
    showSpinner: boolean = false;
    showUpdateForm: boolean = false;
    validationOrderUpdateFormDetailOptions = {};
    currentUser: any;
    currentDate = new Date();

    selectedDocuments: any = [];

    constructor(private http: HttpClient,
                private router: Router,
                private fb: FormBuilder,
                private location: Location,
                private route: ActivatedRoute,
                private orderService: OrderService,
                private notificationService: NotificationService,
                private snackBar: MatSnackBar,
                private spinnerService: Ng4LoadingSpinnerService) {
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.OrderUpdateForm = fb.group({
            Invoice_Date: ['0', [Validators.required]],
            JDE_Invoice_No: ['', [Validators.required]],
            Documents: ['', [Validators.required]]
        });
        this.validationOrderUpdateFormDetailOptions = {
            // Rules for form validation
            rules: {
                Invoice_Date: {
                    required: true
                },
                JDE_Invoice_No: {
                    required: true
                },
                Documents: {
                    required: true
                }
            },

            // Messages for form validation
            messages: {
                Invoice_Date: {
                    required: 'Invoice date required'
                },
                JDE_Invoice_No: {
                    required: 'JDE invoice no required'
                },
                Documents: {
                    required: 'select product attachment'
                }
            },
            submitHandler: this.onSubmit

        };
        this.ID = this.route.params['value'].id;
        this.showUpdateForm = true;
    }

    omit_special_char(event)
    {
        var k;
        k = event.charCode;  //         k = event.keyCode;  (Both can be used)
        return((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57));
    }

    onSubmit() {
        console.log('\n', 'submit handler for validated form', '\n\n')
    }
    cancel() {
        this.router.navigate(["/dashboard"]);
    }


    ngOnInit() {

    }

    onSubmitUpdateForm() {
        if (this.OrderUpdateForm.invalid) {
            return 0;
        }
        if(!this.selectedDocuments && this.selectedDocuments.length == 0) {
            this.snackBar.open('Please select at least one attachment', 'OK', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
            return 0;
        }
        this.createNew();
    }

    createNew() {
        this.showSpinner = true;
        this.OrderUpdateForm.value.OrderManagementID = this.currentUser.id;
        this.orderService.updateDispatchOrder(this.ID, this.OrderUpdateForm.value, this.selectedDocuments).subscribe(data => {
            if (data['status']) {
                this.snackBar.open('Order Updated successfully', 'OK', {
                    duration: 5000,
                    panelClass: ['success-snackbar'],
                    verticalPosition: 'top'
                });
                this.router.navigate(["/dashboard"]);
            }
        }, err => {
            this.showSpinner = false;
            this.snackBar.open('something went error', 'OK', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        });

    }

    onFileChange(event) {
        if (event.target.files && event.target.files.length) {
            let files = event.target.files;
            if (files) {
                this.selectedDocuments = [];
                for (let file of files) {
                    let reader = new FileReader();
                    const fileName = file.name;
                    const lastIndex = fileName.lastIndexOf('.');
                    const extension = fileName.substr(lastIndex + 1);
                    reader.onload = (e: any) => {
                        if(this.selectedDocuments.length >= 10){
                            this.snackBar.open('You can only upload a maximum of 10 files', 'OK', {
                                duration: 5000,
                                panelClass: ['danger-snackbar'],
                                verticalPosition: 'top'
                            });
                        } else {
                            this.selectedDocuments.push(file);
                        }
                    }
                    reader.readAsDataURL(file);
                }
            }
        }
    }
}