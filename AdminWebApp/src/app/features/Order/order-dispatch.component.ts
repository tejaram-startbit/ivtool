import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {NotificationService} from '@app/core/services';
import {MatSnackBar} from "@angular/material";
import {OrderService} from "@app/features/Order/order.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {Location} from "@angular/common";
import {Observable} from "rxjs";
import {Ng4LoadingSpinnerService} from "ng4-loading-spinner";

@Component({
    selector: 'project-order-dispatch',
    templateUrl: './order-dispatch.component.html',
})
export class OrderDispatchComponent implements OnInit {
    ID: any;
    name: any;
    States: any;
    OrderDispatchForm: FormGroup;
    showSpinner: boolean = false;
    selectedPdfFile: any;
    selectedExcelFile: any;
    showDispatchForm: boolean = false;
    validationOrderDispatchFormDetailOptions = {};
    currentUser: any;
    currentDate = new Date();

    constructor(private http: HttpClient,
                private router: Router,
                private fb: FormBuilder,
                private location: Location,
                private route: ActivatedRoute,
                private orderService: OrderService,
                private notificationService: NotificationService,
                private snackBar: MatSnackBar,
                private spinnerService: Ng4LoadingSpinnerService) {
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.OrderDispatchForm = fb.group({
            HAWB_No: ['', [Validators.required]],
            HAWB_Date: ['0', [Validators.required]],
            ETA: ['', [Validators.required]],
            ProductDetailAttachment: ['', [Validators.required]],
            ProductInstructionPdf: ['', [Validators.required]]
        });
        this.validationOrderDispatchFormDetailOptions = {
            // Rules for form validation
            rules: {
                HAWB_No: {
                    required: true
                },
                HAWB_Date: {
                    required: true
                },
                ETA: {
                    required: true
                },
                ProductDetailAttachment: {
                    required: true
                },
                ProductInstructionPdf: {
                    required: true
                }
            },

            // Messages for form validation
            messages: {
                HAWB_No: {
                    required: 'HAWB no required'
                },
                HAWB_Date: {
                    required: 'HAWB date required'
                },
                ETA: {
                    required: 'ETA no required'
                },
                ProductDetailAttachment: {
                    required: 'selecte product detail attachment'
                },
                ProductInstructionPdf: {
                    required: 'select product instruction pdf'
                }
            },
            submitHandler: this.onSubmit

        };
        this.ID = this.route.params['value'].id;
        this.showDispatchForm = true;
    }

    omit_special_char(event)
    {
        var k;
        k = event.charCode;  //         k = event.keyCode;  (Both can be used)
        return((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57));
    }

    onSubmit() {
        console.log('\n', 'submit handler for validated form', '\n\n')
    }

    ngOnInit() {

    }

    onSubmitDispatchForm() {
        if (this.OrderDispatchForm.invalid) {
            return 0;
        }
        if(!this.selectedExcelFile) {
            this.snackBar.open('Please select Product Detail excel Attachment', 'OK', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
            return 0;
        }
        if(!this.selectedPdfFile) {
            this.snackBar.open('Please select Product Instruction Pdf', 'OK', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
            return 0;
        }
        this.createNew();
    }

    createNew() {
        this.showSpinner = true;
        this.OrderDispatchForm.value.OrderManagementID = this.currentUser.id;
        this.orderService.dispatchedOrder(this.ID, this.OrderDispatchForm.value, this.selectedExcelFile, this.selectedPdfFile).subscribe(data => {
            if (data['status']) {
                this.snackBar.open('Order Dispatched successfully', 'OK', {
                    duration: 5000,
                    panelClass: ['success-snackbar'],
                    verticalPosition: 'top'
                });
                this.router.navigate(["/dashboard"]);
            }
        }, err => {
            this.showSpinner = false;
            this.snackBar.open('something went error', 'OK', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        });

    }

    onPdfFileChange(event) {
        const reader = new FileReader();

        if (event.target.files && event.target.files.length) {
            const [file] = event.target.files;
            reader.readAsDataURL(file);
            const fileName = event.target.files[0].name;
            const lastIndex = fileName.lastIndexOf('.');
            const extension = fileName.substr(lastIndex + 1);
            if (extension.toLowerCase() === 'pdf') {
                reader.onload = () => {
                    this.selectedPdfFile = event.target.files[0];
                };
            } else {
                this.snackBar.open('Invalid selected file', 'OK', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
            }
        }
    }

    cancel() {
        this.router.navigate(["/dashboard"]);
    }

    onExcelFileChange(args) {
        const reader = new FileReader();
        const self = this, file = args.srcElement && args.srcElement.files && args.srcElement.files[0];
        if(file) {
            const fileName = file.name;
            const lastIndex = fileName.lastIndexOf('.');
            const extension = fileName.substr(lastIndex + 1);
            if(extension == 'xlsx' || extension == 'xls'){
                this.selectedExcelFile = file;
            } else {
                this.snackBar.open('Invalid selected file', 'OK', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
            }
        }
    }
}