import {NgModule} from "@angular/core";

import {routing} from "@app/features/Order/order.routing";
import {SharedModule} from "@app/shared/shared.module";
import {SmartadminDatatableModule} from '@app/shared/ui/datatable/smartadmin-datatable.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {Ng4LoadingSpinnerModule} from "ng4-loading-spinner";

import {SmartadminInputModule} from '@app/shared/forms/input/smartadmin-input.module';
import {SmartadminWizardsModule} from "@app/shared/forms/wizards/smartadmin-wizards.module";
import {NgMultiSelectDropDownModule} from "ng-multiselect-dropdown";
import {MatStepperModule, MatIconModule} from "@angular/material";
import {MaterialModuleModule} from "@app/core/common/material-module/material-module.module";
import {SmartadminValidationModule} from "@app/shared/forms/validation/smartadmin-validation.module";
import {TreeviewModule} from 'ngx-treeview';
import {SmartadminEditorsModule} from "@app/shared/forms/editors/smartadmin-editors.module";

import {OrderComponent} from "@app/features/Order/order.component";
import {OrderDispatchComponent} from "@app/features/Order/order-dispatch.component";
import {OrderFormComponent} from "@app/features/Order/order-form.component";
import {OrderTransferComponent} from "@app/features/Order/order-transfer.component";
import {OrderUpdateComponent} from "@app/features/Order/order-update.component";
import {
    DialogOverviewExampleDialog, OrderDetailDialogReport,
    OrderViewComponent
} from "@app/features/Order/order-view.component";
import {DialogOverviewExample1Dialog, OrderAckComponent} from "@app/features/Order/order-ack.component";
import {DialogOverviewExample2Dialog} from "@app/features/Order/order-transfer-app.component";
import {DialogOverviewExample3Dialog, OrderReturnComponent} from "@app/features/Order/order-return.component";
import {
    DialogOverviewExample4Dialog,
    DialogOverviewExample8Dialog,
    OrderTransferformComponent
} from "@app/features/Order/order-transferform.component";
import {OrderTransferAppComponent} from "@app/features/Order/order-transfer-app.component";
import {OrderDashboardComponent} from "@app/features/Order/order-dashboard.component";
import {DialogOverviewExample5Dialog, OrderReturnformComponent} from "@app/features/Order/order-returnform.component";

import { MatSelectSearchModule } from "@app/core/common/material-module/mat-select-search/mat-select-search.module";

@NgModule({
    declarations: [
        OrderComponent,
        OrderDispatchComponent,
        OrderFormComponent,
        OrderTransferComponent,
        OrderUpdateComponent,

        OrderViewComponent,
        DialogOverviewExampleDialog,
        DialogOverviewExample1Dialog,
        DialogOverviewExample2Dialog,
        DialogOverviewExample3Dialog,
        DialogOverviewExample8Dialog,
        OrderDetailDialogReport,
        OrderAckComponent,
        OrderTransferAppComponent,
        OrderReturnComponent,
        OrderDashboardComponent,
        OrderReturnformComponent,
        OrderTransferformComponent,
        DialogOverviewExample4Dialog,
        DialogOverviewExample5Dialog
    ],
    imports: [
        SharedModule,
        routing,
        SmartadminDatatableModule,
        FormsModule,
        ReactiveFormsModule,
        SmartadminInputModule,
        SmartadminWizardsModule,
        NgMultiSelectDropDownModule.forRoot(),
        MaterialModuleModule,
        SmartadminValidationModule,
        TreeviewModule.forRoot(),
        SmartadminEditorsModule,
        Ng4LoadingSpinnerModule.forRoot(),
        // FileUploadModule,
        // OwlDateTimeModule,
        // OwlNativeDateTimeModule,
        MatSelectSearchModule,
        // SelectDropDownModule
    ],
    providers: [],
    exports: [
        FormsModule,
        ReactiveFormsModule,
        MatStepperModule,
        MatIconModule
    ],
    entryComponents: [
        OrderDetailDialogReport,
        DialogOverviewExample8Dialog,
        DialogOverviewExampleDialog,
        DialogOverviewExample1Dialog,
        DialogOverviewExample2Dialog,
        DialogOverviewExample3Dialog,
        DialogOverviewExample4Dialog,
        DialogOverviewExample5Dialog
    ]
})
export class OrderModule {

}
