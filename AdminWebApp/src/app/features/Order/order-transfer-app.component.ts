import { Component, OnInit, ViewChild, AfterViewInit, Inject } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { NotificationService } from "@app/core/services";
import { MatSnackBar } from "@angular/material";
import { OrderService } from "@app/features/Order/order.service";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { Location } from "@angular/common";
import { Observable } from "rxjs";
import { Ng4LoadingSpinnerService } from "ng4-loading-spinner";

import { MatSelect } from "@angular/material";

import { ReplaySubject, Subject } from "rxjs";
import { take, takeUntil } from "rxjs/operators";
import {CommonService} from "@app/core/common/common.service";
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

export interface DialogData {
  animal: string;
  name: string;
}

@Component({
  selector: "project-order-transfer",
  templateUrl: "./order-transfer-app.component.html"
})
export class OrderTransferAppComponent implements OnInit {
  ID: any;
  name: any;
  States: any;
  AckForm: FormGroup;
  showSpinner: boolean = false;
  ProductAdded: boolean = false;
  ImageData: any;
  showForm: boolean = false;
  validationAckFormDetailOptions = {};
  Products2: any = [];
  Products: any = [];
  OrderProducts: any = [];
  Order: any = {};
  localStorage: any;
  search: any;
  ServiceType: any;
  month: any;
  date: any;
  year: any;
  dmonth: any;
  ddate: any;
  dyear: any;
  Status : any;
  animal: string;
  allUsers: any = [];
  RequestID: any;
  associatedSalesid : any;
  submitrunning: boolean = false;

  constructor(
    private http: HttpClient,
    private router: Router,
    private fb: FormBuilder,
    private location: Location,
    private route: ActivatedRoute,
    private ordersService: OrderService,
    private notificationService: NotificationService,
    private snackBar: MatSnackBar,
    private spinnerService: Ng4LoadingSpinnerService,
    private commService: CommonService,
    public dialog: MatDialog
  ) {
    this.AckForm = fb.group({
      CustomerID: ["", [Validators.required]]
      
    });
    this.validationAckFormDetailOptions = {
      // Rules for form validation
      rules: {
        CustomerID: {
          required: true
        }
      },

      // Messages for form validation
      messages: {
        CustomerID: {
          required: "Please select Dealer"
        }
      },
      submitHandler: this.onSubmit
    };

    this.commService.sharedVariable = 'Transfer';

    
    this.ID = this.route.params["value"].id;
    this.localStorage = JSON.parse(localStorage.getItem("currentUser"));
    this.ordersService.getOneOrder(this.ID).subscribe((data: {}) => {
      this.RequestID = data['result'].RequestID;
      this.associatedSalesid = data['result'].SalesPersonID._id;
      this.getAllCustomerNSP();
    });
    

  }

  onSubmit() {
    console.log('\n', 'submit handler for validated form', '\n\n')
  }

  ngOnInit() {

  }

  getAllCustomerNSP(){
    const salesid = this.localStorage.id;
    this.ordersService.getAllSP().subscribe(data => {
        if (data['status']) {
            this.allUsers = data['result'];

            // this.allUsers = this.allUsers.filter(function( obj ) {
            //   return obj._id != salesid;
            // });
            //getAllCustomerNSP
        }
    }, err => {
            console.log(err);
    })
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(DialogOverviewExample2Dialog, {
      width: '250px',
      data: {name: this.name, animal: this.animal}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.animal = result;
    });
  }

  onSubmitForm() {
      if (this.AckForm.invalid) {
          return 0;
      }
      
      this.showSpinner = true;
      this.ordersService.addTransfer(this.AckForm.value, this.ID, this.localStorage.id).subscribe(data => {
          if (data['status']) {
              const dialogRef = this.dialog.open(DialogOverviewExample2Dialog, {
                width: '250px',
                data: {name: this.name, animal: this.animal}
              });
          
              dialogRef.afterClosed().subscribe(result => {
                console.log('The dialog was closed');
                this.animal = result;
              });
              // this.snackBar.open('Acknowledge submitted successfully', 'OK', {
              //     duration: 5000,
              //     panelClass: ['success-snackbar'],
              //     verticalPosition: 'top'
              // });
              this.showSpinner = false;
              //this.location.back();
          }
      }, err => {
          this.snackBar.open('something went error', 'OK', {
              duration: 5000,
              panelClass: ['danger-snackbar'],
              verticalPosition: 'top'
          });
      });
  }

  onFileChange(event) {
    const reader = new FileReader();

    if (event.target.files && event.target.files.length) {
        const [file] = event.target.files;
        reader.readAsDataURL(file);
        const fileName = event.target.files[0].name;
        const lastIndex = fileName.lastIndexOf('.');
        const extension = fileName.substr(lastIndex + 1);
        if (extension.toLowerCase() === 'jpg' || extension.toLowerCase() === 'jpeg' || extension.toLowerCase() === 'png') {
            reader.onload = () => {
                this.ImageData = event.target.files[0];
            };
        } else {
            this.snackBar.open('Invalid selected file', 'OK', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        }
    }
}
  

}

@Component({
  selector: 'dialog-overview-example1-dialog',
  templateUrl: 'dialog-overview-example1-dialog.html',
})
export class DialogOverviewExample2Dialog {

  constructor( private router: Router,
    private location: Location,
    public dialogRef: MatDialogRef<DialogOverviewExample2Dialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {}

  ok(): void {
    this.dialogRef.close();
    this.router.navigate(["/dashboard"]);
  }

}