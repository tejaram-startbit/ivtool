import {RouterModule, Routes} from "@angular/router";
import {OrderComponent} from "@app/features/Order/order.component";
import {OrderDispatchComponent} from "@app/features/Order/order-dispatch.component";
import {OrderFormComponent} from "@app/features/Order/order-form.component";
import {OrderTransferComponent} from "@app/features/Order/order-transfer.component";
import {OrderUpdateComponent} from "@app/features/Order/order-update.component";
import {OrderViewComponent} from "@app/features/Order/order-view.component";
import {OrderAckComponent} from "@app/features/Order/order-ack.component";
import {OrderTransferAppComponent} from "@app/features/Order/order-transfer-app.component";
import {OrderReturnComponent} from "@app/features/Order/order-return.component";
import {OrderDashboardComponent} from "@app/features/Order/order-dashboard.component";
import {OrderReturnformComponent} from "@app/features/Order/order-returnform.component";
import {OrderTransferformComponent} from "@app/features/Order/order-transferform.component";

export const routes: Routes = [

    {
        path: '',
        redirectTo: 'list'
    },
    {
        path: 'list',
        component: OrderComponent
    },
    {
        path: 'dispatch/:id',
        component: OrderDispatchComponent
    },
    {
        path: 'transfer-order/:id',
        component: OrderTransferComponent
    },
    {
        path: 'update/:id',
        component: OrderUpdateComponent
    },
    //app link
    {
        path: 'form/:id',
        component: OrderFormComponent
    },
    {
        path: 'view/:id',
        component: OrderViewComponent
    },
    {
        path: 'ack/:id',
        component: OrderAckComponent
    },
    {
        path: 'transfer/:id',
        component: OrderTransferAppComponent
    },
    {
        path: 'return/:id',
        component: OrderReturnComponent
    },
    {
        path: 'dashboard',
        component: OrderDashboardComponent
    },
    {
        path: 'returnform',
        component: OrderReturnformComponent
    },
    {
        path: 'transferform',
        component: OrderTransferformComponent
    },
];

export const routing = RouterModule.forChild(routes);
