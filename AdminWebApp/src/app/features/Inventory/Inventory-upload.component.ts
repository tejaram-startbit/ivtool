import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {NotificationService} from '@app/core/services';
import {MatSnackBar} from "@angular/material";
import {InventoryService} from "@app/features/Inventory/Inventory.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {Location} from "@angular/common";
import {Observable} from "rxjs";
import {Ng4LoadingSpinnerService} from "ng4-loading-spinner";
import {RoleService} from "@app/features/Role/role.service";


@Component({
    selector: 'project-inventory-upload',
    templateUrl: './Inventory-upload.component.html',
})
export class InventoryUploadComponent implements OnInit {
    name: any;
    States: any;
    showSpinner: boolean = false;
    selectedExcelFile: any;

    constructor(private http: HttpClient,
                private router: Router,
                private fb: FormBuilder,
                private location: Location,
                private route: ActivatedRoute,
                private inventoryService: InventoryService,
                private notificationService: NotificationService,
                private snackBar: MatSnackBar,
                private spinnerService: Ng4LoadingSpinnerService,
                private roleServece: RoleService) {

    }

    ngOnInit() {

    }

    cancel() {
        this.location.back();
    }

    onExcelFileChange(args) {
        const reader = new FileReader();
        const self = this, file = args.srcElement && args.srcElement.files && args.srcElement.files[0];
        if(file) {
            const fileName = file.name;
            const lastIndex = fileName.lastIndexOf('.');
            const extension = fileName.substr(lastIndex + 1);
            if(extension == 'xlsx' || extension == 'xls'){
                this.selectedExcelFile = file;
            } else {
                this.snackBar.open('Invalid selected file', 'error', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
            }
        }
    }

    uploadExcel(){
        if(this.selectedExcelFile) {
            this.showSpinner = true;
            this.spinnerService.show();
            this.inventoryService.uploadExcel(this.selectedExcelFile).subscribe(data => {
                if (data['status']) {
                    this.snackBar.open('Successfully uploaded excel', 'success', {
                        duration: 5000,
                        panelClass: ['success-snackbar'],
                        verticalPosition: 'top'
                    });
                    this.location.back();
                }
                this.spinnerService.hide();
            }, err => {
                this.showSpinner = false;
                this.spinnerService.hide();
                this.snackBar.open('something went error', 'error', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
            });
        } else {
            this.snackBar.open('Please select excel file', 'error', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        }
    }
}