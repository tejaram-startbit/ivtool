import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {NotificationService} from '@app/core/services';
import {MatSnackBar} from "@angular/material";
import {InventoryService} from "@app/features/Inventory/Inventory.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {Location} from "@angular/common";
import {Observable} from "rxjs";
import {Ng4LoadingSpinnerService} from "ng4-loading-spinner";
import {RoleService} from "@app/features/Role/role.service";
import { UserService } from '@app/features/User/user.service';


@Component({
    selector: 'project-inventory-form',
    templateUrl: './Inventory-form.component.html',
})
export class InventoryFormComponent implements OnInit {
    ID: any;
    name: any;
    States: any;
    InventoryForm: FormGroup;
    showSpinner: boolean = false;
    ImageData: any;
    selectedExcelFile: any;
    showForm: boolean = false;
    validationInventoryFormDetailOptions = {};

    AllMisFranchise = [];
    AllMisBUGroup = [];

    constructor(private http: HttpClient,
                private router: Router,
                private fb: FormBuilder,
                private location: Location,
                private route: ActivatedRoute,
                private inventoryService: InventoryService,
                private userService : UserService,
                private notificationService: NotificationService,
                private snackBar: MatSnackBar,
                private spinnerService: Ng4LoadingSpinnerService,
                private roleServece: RoleService) {

        this.InventoryForm = fb.group({
            Name: ['', [Validators.required]],
            ProductCode: ['', [Validators.required]],
            Type: ['Instrument', [Validators.required]],
            Image: [''],
            Description: [''],
            Price: ['', [Validators.required]],
            TotalQuantity: ['', [Validators.required]],
            ProductConsumable: ['No', [Validators.required]],
            Inventory: ['', [Validators.required]],
            BranchCode: ['', [Validators.required]],
            MisFranchise: ['', [Validators.required]],
            MisBUGroup: ['', [Validators.required]],
            Location: ['']
        });
        this.validationInventoryFormDetailOptions = {
            // Rules for form validation
            rules: {
                Name: {
                    required: true
                },
                ProductCode: {
                    required: true
                },
                Type: {
                    required: true
                },
                Image: {
                    required: true
                },
                TotalQuantity: {
                    required: true
                },
                ProductConsumable: {
                    required: true
                },
                Inventory: {
                    required: true
                },
                BranchCode: {
                    required: true
                },
                MisFranchise:{
                    required : true
                },
                MisBUGroup:{
                    required : true
                }
            },

            // Messages for form validation
            messages: {
                Name: {
                    required: 'Please enter Inventory Name'
                },
                ProductCode: {
                    required: 'Please enter Inventory Code'
                },
                Type: {
                    required: 'Please select inventory Type'
                },
                TotalQuantity: {
                    required: 'Quantity is required'
                },
                ProductConsumable: {
                    required: 'Inventory Consumable is required'
                },
                Inventory: {
                    required: 'Inventory is required'
                },
                BranchCode: {
                    required: 'Branch Code is required'
                },
                MisFranchise:{
                    required : 'Please enter MIS Franchise'
                },
                MisBUGroup:{
                    required : 'Please enter MIS BU Group'
                }
            },
            submitHandler: this.onSubmit

        };
        this.ID = this.route.params['value'].id;
        if (this.ID !== '-1') {
            this.getOne(this.ID);
        }
        this.showForm = true;
    }

    onSubmit() {
        console.log('\n', 'submit handler for validated form', '\n\n')
    }

    ngOnInit() {
        this.userService.getAllApprover().subscribe((data:{})=>{
            const map = new Map();
             for (const item of data['result']) {
                 if(!map.has(item.MisBUGroup)){
                     map.set(item.MisBUGroup, true);    // set any value to Map
                     this.AllMisBUGroup.push({
                         Name: item.MisBUGroup
                     });
                 }
             } 
             for (const item of data['result']) {
                 if(!map.has(item.MisFranchise)){
                     map.set(item.MisFranchise, true);    // set any value to Map
                     this.AllMisFranchise.push({
                         Name: item.MisFranchise
                     });
                 }
             } 
         })
    }

    onSubmitForm() {
        if (this.InventoryForm.invalid) {
            return 0;
        }
        if (this.ID === '-1') {
            this.createNew();
        } else {
            this.editExisting();
        }
    }

    createNew() {
        this.showSpinner = true;
        this.inventoryService.addInventory(this.InventoryForm.value , this.ImageData).subscribe(data => {
            if (data['status']) {
                this.snackBar.open('Record created successfully', 'OK', {
                    duration: 5000,
                    panelClass: ['success-snackbar'],
                    verticalPosition: 'top'
                });
                this.location.back();
            }
        }, err => {
            this.snackBar.open('something went error', 'OK', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        });

    }

    editExisting() {
        this.notificationService.smartMessageBox({
            title: "Update!",
            content: "Are you sure update this record?",
            buttons: '[No][Yes]'
        }, (ButtonPressed) => {
            if (ButtonPressed === "Yes") {
                this.showSpinner = true;
                this.inventoryService.updateMyInventory(this.ID, this.InventoryForm.value , this.ImageData).subscribe(data => {
                    if (data['status']) {
                        this.snackBar.open('Record updated successfully', 'OK', {
                            duration: 5000,
                            panelClass: ['success-snackbar'],
                            verticalPosition: 'top'
                        });
                        this.location.back();
                    }
                }, err => {
                    this.snackBar.open('something went error', 'OK', {
                        duration: 5000,
                        panelClass: ['danger-snackbar'],
                        verticalPosition: 'top'
                    });
                });
            }
            if (ButtonPressed === "No") {

            }

        });

    }

    getOne(id) {
        this.spinnerService.show();
        this.inventoryService.getOneInventory(id).subscribe(data => {
            if (data['status']) {
                this.InventoryForm = this.fb.group({
                    Name: [data['result'].Name, [Validators.required]],
                    ProductCode: [data['result'].ProductCode, [Validators.required]],
                    Type: [data['result'].Type, [Validators.required]],
                    Image: [''],
                    Description: [data['result'].Description,],
                    Price: [data['result'].Price],
                    TotalQuantity: [data['result'].AvailableQuantity],
                    ProductConsumable: [data['result'].ProductConsumable ? data['result'].ProductConsumable : '0', [Validators.required]],
                    Inventory: [data['result'].Inventory, [Validators.required]],
                    BranchCode: [data['result'].BranchCode, [Validators.required]],
                    MisFranchise: [data['result'].MisFranchise ? data['result'].MisFranchise : '', [Validators.required]],
                    MisBUGroup: [data['result'].MisBUGroup  ? data['result'].MisBUGroup : '', [Validators.required]],
                    Location: [data['result'].Location  ? data['result'].Location : '']
                });
            }
            this.spinnerService.hide();
        }, err => {
            this.snackBar.open('something went error', 'OK', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        });
    }

    onFileChange(event) {
        const reader = new FileReader();

        if (event.target.files && event.target.files.length) {
            const [file] = event.target.files;
            reader.readAsDataURL(file);
            const fileName = event.target.files[0].name;
            const lastIndex = fileName.lastIndexOf('.');
            const extension = fileName.substr(lastIndex + 1);
            if (extension.toLowerCase() === 'jpg' || extension.toLowerCase() === 'jpeg' || extension.toLowerCase() === 'png') {
                reader.onload = () => {
                    this.ImageData = event.target.files[0];
                };
            } else {
                this.snackBar.open('Invalid selected file', 'OK', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
            }
        }
    }

    register() {
        this.router.navigate(['/inventory/list']);
    }

    cancel() {
        this.location.back();
    }

    onExcelFileChange(args) {
        const reader = new FileReader();
        const self = this, file = args.srcElement && args.srcElement.files && args.srcElement.files[0];
        if(file) {
            const fileName = file.name;
            const lastIndex = fileName.lastIndexOf('.');
            const extension = fileName.substr(lastIndex + 1);
            if(extension == 'xlsx' || extension == 'xls'){
                this.selectedExcelFile = file;
            } else {
                this.snackBar.open('Invalid selected file', 'OK', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
            }
        }
    }

    uploadExcel(){
        if(this.selectedExcelFile) {
            this.showSpinner = true;
            this.inventoryService.uploadExcel(this.selectedExcelFile).subscribe(data => {
                if (data['status']) {
                    this.snackBar.open('Successfully uploaded excel', 'OK', {
                        duration: 5000,
                        panelClass: ['success-snackbar'],
                        verticalPosition: 'top'
                    });
                    this.location.back();
                }
            }, err => {
                this.showSpinner = false;
                this.snackBar.open('something went error', 'OK', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
            });
        } else {
            this.snackBar.open('Please select excel file', 'OK', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        }
    }
}