import {RouterModule, Routes} from "@angular/router";
import {InventoryComponent} from "@app/features/Inventory/Inventory.component";
import {InventoryFormComponent} from "@app/features/Inventory/Inventory-form.component";
import {InventoryUploadComponent} from "@app/features/Inventory/Inventory-upload.component";

export const routes: Routes = [

    {
        path: '',
        redirectTo: 'list'
    },
    {
        path: 'list',
        component: InventoryComponent
    },
    {
        path: 'form/:id',
        component: InventoryFormComponent
    },
    {
        path: 'upload',
        component: InventoryUploadComponent
    }
];

export const routing = RouterModule.forChild(routes);
