import {RouterModule, Routes} from "@angular/router";
import {ProductComponent} from "@app/features/Product/product.component";
import {ProductFormComponent} from "@app/features/Product/product-form.component";
import {ProductUploadComponent} from "@app/features/Product/product-upload.component";

export const routes: Routes = [

    {
        path: '',
        redirectTo: 'list'
    },
    {
        path: 'list',
        component: ProductComponent
    },
    {
        path: 'form/:id',
        component: ProductFormComponent
    },
    {
        path: 'upload',
        component: ProductUploadComponent
    }
];

export const routing = RouterModule.forChild(routes);
