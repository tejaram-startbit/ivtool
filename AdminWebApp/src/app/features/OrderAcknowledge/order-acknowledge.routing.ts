import {RouterModule, Routes} from "@angular/router";
import {OrderAcknowledgeComponent} from "@app/features/OrderAcknowledge/order-acknowledge.component";
import {OrderAckformComponent} from "@app/features/OrderAcknowledge/order-ackform.component";


export const routes: Routes = [
    {
        path: ':UserID/:OrderID/:Token',
        component: OrderAcknowledgeComponent
    },
    {
        path : 'ackform/:id',
        component : OrderAckformComponent
    }
];

export const routing = RouterModule.forChild(routes);
