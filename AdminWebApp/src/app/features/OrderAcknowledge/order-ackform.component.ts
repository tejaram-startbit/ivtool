import { Component, OnInit, ViewChild, AfterViewInit, Inject } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { NotificationService } from "@app/core/services";
import { MatSnackBar } from "@angular/material";
import { OrderService } from "@app/features/Order/order.service";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { Location } from "@angular/common";
import { Observable } from "rxjs";
import { Ng4LoadingSpinnerService } from "ng4-loading-spinner";

import { MatSelect } from "@angular/material";

import { ReplaySubject, Subject } from "rxjs";
import { take, takeUntil } from "rxjs/operators";
import {CommonService} from "@app/core/common/common.service";
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

export interface DialogData {
  animal: string;
  name: string;
}

@Component({
  selector: "project-order-ackform",
  templateUrl: "./order-ackform.component.html"
})
export class OrderAckformComponent implements OnInit {
  ID: any;
  name: any;
  States: any;
  AckForm: FormGroup;
  showSpinner: boolean = false;
  ProductAdded: boolean = false;
  ImageData: any;
  showForm: boolean = false;
  validationAckFormDetailOptions = {};
  Products2: any = [];
  Products: any = [];
  OrderProducts: any = [];
  Order: any = {};
  localStorage: any;
  search: any;
  ServiceType: any;
  month: any;
  date: any;
  year: any;
  dmonth: any;
  ddate: any;
  dyear: any;
  Status : any;
  animal: string;

  orderDetail: any;
  showThankYouMessage = false;
  OrderFound = false;

  constructor(
    private http: HttpClient,
    private router: Router,
    private fb: FormBuilder,
    private location: Location,
    private route: ActivatedRoute,
    private ordersService: OrderService,
    private notificationService: NotificationService,
    private snackBar: MatSnackBar,
    private spinnerService: Ng4LoadingSpinnerService,
    private commService: CommonService,
    private orderService : OrderService,
    public dialog: MatDialog
  ) {
    this.AckForm = fb.group({
      Remarks: ["", [Validators.required]],
      Picture: ["", ]
    });
    this.validationAckFormDetailOptions = {
      // Rules for form validation
      rules: {
        // Picture: {
        //   required: false
        // },
        Remarks: {
          required: true
        }
      },

      // Messages for form validation
      messages: {
        // Picture: {
        //   required: "Please select image"
        // },
        Remarks: {
          required: "Please enter your remarks"
        }
      },
      submitHandler: this.onSubmit
    };

    

    this.ordersService.getAllProducts().subscribe((data: {}) => {
      //this.Products2 = data['result'];
      for (let i = 0; i < data["result"].length; i++) {
        this.Products2.push({
          ProductID: data["result"][i]._id,
          ProductName: data["result"][i].Name,
          ProductQuantity: 0
          // ProductIncluded: false
        });
      }
    });
    this.ID = this.route.params["value"].id;
    this.getOrderDetail(this.ID);
    this.localStorage = JSON.parse(localStorage.getItem("currentUser"));
    this.commService.sharedVariable = 'Acknowledge';
    this.Order = {
      CustomerDetails : {
        CustomerName: '',
        Mobile:'',
        Email:'',
        Address:'',
        ContactPerson:'',
        ContactPersonMobile:''
      },
      Products: [],
      ScheduleReturnDate: '',
      ServiceType: 'Instrument'
    }
  }

  onSubmit() {
    console.log('\n', 'submit handler for validated form', '\n\n')
  }

  ngOnInit() {

  }

  openDialog(): void {
    const dialogRef = this.dialog.open(DialogOverviewExample1Dialog, {
      width: '250px',
      data: {name: this.name, animal: this.animal}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.animal = result;
    });
  }

  getOrderDetail(OrderID){
    this.showSpinner = true;
    console.log('dsfsdfds')
    this.orderService.getOneOrder(OrderID).subscribe(data => {
        if (data['status']) {
            this.orderDetail = data['result'];
            
            if(this.orderDetail) {
              
            } else {
                // this.userMessage = 'Invalid link';
                // this.linkVisible = true;
                this.snackBar.open('Invalid link', 'OK', {
                  duration: 5000,
                  panelClass: ['danger-snackbar'],
                  verticalPosition: 'top'
              });
            }
        }
        this.showSpinner = false;
    }, err => {
      this.showSpinner = false;
        this.snackBar.open('something went error', 'OK', {
            duration: 5000,
            panelClass: ['danger-snackbar'],
            verticalPosition: 'top'
        });
    })
}

  onSubmitForm() {
      if (this.AckForm.invalid) {
          return 0;
      }
      this.showSpinner = true;
      this.ordersService.addAcknowledge(this.AckForm.value, this.ImageData, this.ID).subscribe(data => {
          if (data['status']) {
              this.getOrderDetail(this.ID);
              const dialogRef = this.dialog.open(DialogOverviewExample1Dialog, {
                width: '250px',
                data: {name: this.name, animal: this.animal}
              });
          
              dialogRef.afterClosed().subscribe(result => {
                console.log('The dialog was closed');
                this.animal = result;
              });
              this.showSpinner = false;
          }
      }, err => {
          this.snackBar.open('something went error', 'OK', {
              duration: 5000,
              panelClass: ['danger-snackbar'],
              verticalPosition: 'top'
          });
      });
  }

  onFileChange(event) {
    const reader = new FileReader();

    if (event.target.files && event.target.files.length) {
        const [file] = event.target.files;
        reader.readAsDataURL(file);
        const fileName = event.target.files[0].name;
        const lastIndex = fileName.lastIndexOf('.');
        const extension = fileName.substr(lastIndex + 1);
            reader.onload = () => {
                this.ImageData = event.target.files[0];
            };
    }
}

  

}

@Component({
  selector: 'dialog-overview-example1-dialog',
  templateUrl: './dialog-overview-example1-dialog.html',
})
export class DialogOverviewExample1Dialog {

  constructor( private router: Router,
    private location: Location,
    public dialogRef: MatDialogRef<DialogOverviewExample1Dialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {}

  ok(): void {
    this.dialogRef.close();
    // this.router.navigate(["/dashboard"]);
  }

}