import {TestBed} from '@angular/core/testing';

import {CustomerService} from "@app/features/Customer/customer.service";

describe('CustomerService', () => {
    beforeEach(() => TestBed.configureTestingModule({}));

    it('should be created', () => {
        const service: CustomerService = TestBed.get(CustomerService);
        expect(service).toBeTruthy();
    });
});
