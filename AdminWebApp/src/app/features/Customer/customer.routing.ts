import {RouterModule, Routes} from "@angular/router";
import {CustomerComponent} from "@app/features/Customer/customer.component";
import {CustomerFormComponent} from "@app/features/Customer/customer-form.component";

export const routes: Routes = [

    {
        path: '',
        redirectTo: 'list'
    },
    {
        path: 'list',
        component: CustomerComponent
    },
    {
        path: 'form/:id',
        component: CustomerFormComponent
    }
];

export const routing = RouterModule.forChild(routes);
