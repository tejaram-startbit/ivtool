import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {NotificationService} from '@app/core/services';
import {MatSnackBar} from "@angular/material";
import {CustomerService} from "@app/features/Customer/customer.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {Location} from "@angular/common";
import {Observable} from "rxjs";
import {Ng4LoadingSpinnerService} from "ng4-loading-spinner";
import {RoleService} from "@app/features/Role/role.service";
import { Options, LabelType } from 'ng5-slider';
import {CostLevelService} from "@app/features/CostLevel/costlevel.service";


@Component({
    selector: 'project-customer-form',
    templateUrl: './customer-form.component.html',
})
export class CustomerFormComponent implements OnInit {
    ID: any;
    name: any;
    States: any;
    CustomerForm: FormGroup;
    showSpinner: boolean = false;
    ImageData: any;
    showForm: boolean = false;
    validationCustomerFormDetailOptions = {};
    allRoles: any = [];
    allCostLevels: any = [];
    showRangeSlider: boolean = false;

    minValue: number = 0;
    maxValue: number = 100000;
    rangeSliderOptions: Options = {
        floor: 0,
        ceil: 1000000,
        step: 1000,
        translate: (value: number, label: LabelType): string => {
            switch (label) {
                case LabelType.Low:
                    return '<b>Min:</b>' + value;
                case LabelType.High:
                    return '<b>Max:</b>' + value;
                default:
                    return '' + value;
            }
        }
    };

    showPassword = false;

    constructor(private http: HttpClient,
                private router: Router,
                private fb: FormBuilder,
                private location: Location,
                private route: ActivatedRoute,
                private customerService: CustomerService,
                private notificationService: NotificationService,
                private snackBar: MatSnackBar,
                private spinnerService: Ng4LoadingSpinnerService,
                private costlevelServece: CostLevelService,
                private roleServece: RoleService) {

        this.CustomerForm = fb.group({
            CustomerName: ['', [Validators.required]],
            Password: ['', [Validators.required]],
            FirstName: ['', [Validators.required]],
            LastName: ['', [Validators.required]],
            Email: ['', [Validators.required, Validators.email]],
            Phone: ['', [Validators.min(1000000000), Validators.max(9999999999)]],
            Gender: ['Male', [Validators.required]],
            Avatar: [''],
            RoleID: ['0', [Validators.required]],
            Address: ['', [Validators.required]],
            CostLevel: ['0']
        });
        this.getAllRoles();
        this.getAllCostLevels();
        this.validationCustomerFormDetailOptions = {
            // Rules for form validation
            rules: {
                CustomerName: {
                    required: true
                },
                Password: {
                    required: true
                },
                FirstName: {
                    required: true
                },
                LastName: {
                    required: true
                },
                Email: {
                    required: true,
                    email: true
                },
                Phone: {
                    required: true,
                    pattern: "[0-9]+",
                    minlength: 10,
                    maxlength: 10
                },
                Address: {
                    required: true
                },
                RoleID: {
                    required: true
                },
                Gender: {
                    required: true
                }
            },

            // Messages for form validation
            messages: {
                CustomerName: {
                    required: 'Please enter your CustomerName'
                },
                Password: {
                    required: 'Please enter your Password'
                },
                FirstName: {
                    required: 'Please enter your first name'
                },
                LastName: {
                    required: 'Please enter your last name'
                },
                Email: {
                    required: 'Please enter your email address',
                    email: 'Please enter a valid email addres'
                },
                Phone: {
                    required: 'Please enter your phone number',
                    minlength: '<i class="fa fa-warning"></i>&nbsp;Please enter at least 10 digits',
                    maxlength: '<i class="fa fa-warning"></i>&nbsp;Please enter no more than 10 digits.',
                },
                Address: {
                    required: 'Please enter address'
                },
                RoleID: {
                    required: 'Please select Role'
                },
                Gender: {
                    required: 'Please select gender'
                }
            },
            submitHandler: this.onSubmit

        };
        this.ID = this.route.params['value'].id;
        if (this.ID !== '-1') {
            this.getOne(this.ID);
        }
        this.showForm = true;
    }

    onSubmit() {
        console.log('\n', 'submit handler for validated form', '\n\n')
    }

    ngOnInit() {

    }

    passwordShowHide() {
        this.showPassword = !this.showPassword;
    }

    onSubmitForm() {
        if (this.CustomerForm.invalid) {
            return 0;
        }
        if(this.CustomerForm.value.RoleID == '0'){
            this.snackBar.open('Customer Role required', 'OK', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
            return 0;
        }

        if(this.showRangeSlider){
            var ind = this.allCostLevels.findIndex(x => x._id == this.CustomerForm.value.CostLevel);
            if(ind == -1){
                this.snackBar.open('please select customer cost level', 'OK', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
                return 0;
            } else {
                this.CustomerForm.value.MinCostLevel =  this.allCostLevels[ind].MinRange;
                this.CustomerForm.value.MaxCostLevel =  this.allCostLevels[ind].MaxRange;
            }
        } else {
            this.CustomerForm.value.MinCostLevel =  0;
            this.CustomerForm.value.MaxCostLevel =  0;
        }
        if (this.ID === '-1') {
            this.createNew();
        } else {
            this.editExisting();
        }
    }

    getAllRoles(){
        this.roleServece.getAllRoles().subscribe(data => {
            if (data['status']) {
                this.allRoles = data['result'];
            }
        }, err => {
                this.snackBar.open('something went error', 'OK', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
        })
    }

    getAllCostLevels() {
        this.costlevelServece.getAllCostLevels().subscribe(data => {
            if (data['status']) {
                this.allCostLevels = data['result'];
            }
        }, err => {
            this.snackBar.open('something went error', 'OK', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        })
    }


    createNew() {
        this.showSpinner = true;
        if(!this.showRangeSlider){
            this.CustomerForm.value.CostLevel = 0;
        }
        this.customerService.addCustomer(this.CustomerForm.value, this.ImageData).subscribe(data => {
            if (data['status']) {
                this.snackBar.open('Record created successfully', 'OK', {
                    duration: 5000,
                    panelClass: ['success-snackbar'],
                    verticalPosition: 'top'
                });
                this.location.back();
            }
        }, err => {
            this.snackBar.open('something went error', 'OK', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        });

    }

    editExisting() {
        this.notificationService.smartMessageBox({
            title: "Update!",
            content: "Are you sure update this record?",
            buttons: '[No][Yes]'
        }, (ButtonPressed) => {
            if (ButtonPressed === "Yes") {
                this.showSpinner = true;
                if(!this.showRangeSlider){
                    this.CustomerForm.value.CostLevel = 0;
                }
                this.customerService.updateMyCustomer(this.ID, this.CustomerForm.value, this.ImageData).subscribe(data => {
                    if (data['status']) {
                        this.snackBar.open('Record updated successfully', 'OK', {
                            duration: 5000,
                            panelClass: ['success-snackbar'],
                            verticalPosition: 'top'
                        });
                        this.location.back();
                    }
                }, err => {
                    this.snackBar.open('something went error', 'OK', {
                        duration: 5000,
                        panelClass: ['danger-snackbar'],
                        verticalPosition: 'top'
                    });
                });
            }
            if (ButtonPressed === "No") {

            }

        });

    }

    getOne(id) {
        this.spinnerService.show();
        this.customerService.getOneCustomer(id).subscribe(data => {
            if (data['status']) {
                this.CustomerForm = this.fb.group({
                    CustomerName: [data['result'].CustomerName, [Validators.required]],
                    // Password: [data['result'].Password],
                    FirstName: [data['result'].FirstName, [Validators.required]],
                    LastName: [data['result'].LastName, [Validators.required]],
                    Email: [data['result'].Email, [Validators.required, Validators.email]],
                    Phone: [data['result'].Phone, [Validators.min(1000000000), Validators.max(9999999999)]],
                    Gender: [data['result'].Gender, [Validators.required]],
                    Avatar: [''],
                    RoleID: [data['result'].RoleID ? data['result'].RoleID._id : '0', [Validators.required]],
                    Address: [data['result'].Address, [Validators.required]],
                    CostLevel: ['0', [Validators.required]]
                });
                this.onChangeRole(data['result'].RoleID._id);
                var ind = this.allCostLevels.findIndex(x => x.MinRange == data['result'].MinCostLevel && x.MaxRange == data['result'].MaxCostLevel);
                if(ind != -1) {
                    this.CustomerForm.patchValue({
                        CostLevel: this.allCostLevels[ind]._id
                    })
                }
            }
            this.spinnerService.hide();
        }, err => {
            this.snackBar.open('something went error', 'OK', {
                duration: 5000,
                panelClass: ['danger-snackbar'],
                verticalPosition: 'top'
            });
        });
    }

    onFileChange(event) {
        const reader = new FileReader();

        if (event.target.files && event.target.files.length) {
            const [file] = event.target.files;
            reader.readAsDataURL(file);
            const fileName = event.target.files[0].name;
            const lastIndex = fileName.lastIndexOf('.');
            const extension = fileName.substr(lastIndex + 1);
            if (extension.toLowerCase() === 'jpg' || extension.toLowerCase() === 'jpeg' || extension.toLowerCase() === 'png') {
                reader.onload = () => {
                    this.ImageData = event.target.files[0];
                };
            } else {
                this.snackBar.open('Invalid selected file', 'OK', {
                    duration: 5000,
                    panelClass: ['danger-snackbar'],
                    verticalPosition: 'top'
                });
            }
        }
    }

    register() {
        this.router.navigate(['/customer/list']);
    }

    cancel() {
        this.location.back();
    }

    onChangeRole(RoleID){
        const index = this.allRoles.findIndex(function(x){return x._id == RoleID });
        if (index != -1 && (this.allRoles[index]['Name'] == 'Approver' || this.allRoles[index]['Name'] == 'Approver')) {
            this.showRangeSlider = true;
        } else {
            this.showRangeSlider = false;
        }
    }
}