import {Error404Component} from '@app/features/miscellaneous/error404/error404.component';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {CoreModule} from './core/core.module';
import {SharedModule} from '@app/shared/shared.module';
import {MaterialModuleModule} from "@app/core/common/material-module/material-module.module";


@NgModule({
    declarations: [
        AppComponent,
        Error404Component
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        SharedModule,
        CoreModule,
        MaterialModuleModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
