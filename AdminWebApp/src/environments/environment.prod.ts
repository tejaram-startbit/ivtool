export const environment = {
    API_URL:"IVT/Admin/",
  production: true,

  firebase: {
  },

  debug: false,
  log: {
    auth: false,
    store: false,
  },

  smartadmin: {
    api: null,
    db: 'smartadmin-angular'
  }

};
