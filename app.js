var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var morgan = require('morgan');
var bodyParser = require('body-parser')
const config = require('./API/config');
var mainRoute = require('./API/routes/routes');
var signin = require('./API/routes/signin');
var nodemailer = require('nodemailer');
var async = require('async');
var compression = require('compression');

var cors = require('cors');
var helmet = require("helmet");

var cron = require('node-cron');

var app = express();


app.use(cors({ credentials: true, origin: '*' }))
app.use(helmet()); // Add Helmet as a middleware




app.use(compression());
app.use(function(req, res, next) {
    // res.header("Content-Security-Policy", "default-src 'self' data: gap: http://startbitinc.com:19961 'unsafe-eval';font-src 'self' data: fonts.gstatic.com; style-src 'self' 'unsafe-inline' http://startbitinc.com:19961; media-src * blob:; img-src 'self' data: ; script-src * data: https://ssl.gstatic.com 'unsafe-eval'; connect-src 'self';");
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});


app.use(morgan('dev'));
// app.use(express.json());
// app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
var cacheTime = 86400000 * 15; // 15 days
app.use(express.static(path.join(__dirname, './public'), { maxAge: 1 }));

app.use(bodyParser.urlencoded({ limit: "100mb", extended: false, parameterLimit: 50000 }));
app.use(bodyParser.json({ limit: "100mb", extended: true })); // to support JSON-encoded bodies


app.use((req, res, next) => {
    config;
    next();
});

app.use('/', mainRoute);
app.use('/admin', signin);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    next(createError(404));
});

var orderCtrl = require('./API/controller/Order/order.controller');
cron.schedule('59 59 23 * * *', () => {
    orderCtrl.acknowledgeReminder();
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    // res.status(err.status || 500);
    // res.render('error');
    //console.log('app crashed', res.locals.message, res.locals.error);
    res.send('<div style="text-align: center;"><img src="http://134.209.146.247/assets/images/error-404.jpg" class="error-image"></div>')
});

module.exports = app;